# Wtienda ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

Wtienda is a system for shop stations

## Installation

### Requirements
* Node
* Angular
* Composer
* Laravel
* MySql

For Backend: `$ composer install`

For Frontend: `$ npm install`


## License
[MIT](https://choosealicense.com/licenses/mit/)