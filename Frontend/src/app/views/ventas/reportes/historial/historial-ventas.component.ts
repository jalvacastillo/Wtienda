import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

declare var $:any;

@Component({
  selector: 'app-historial-ventas',
  templateUrl: './historial-ventas.component.html'
})

export class HistorialVentasComponent implements OnInit {

	public ventas:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    public filtro:any = {};
    modalRef!: BsModalRef;

    constructor(
        public apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {

        if(!this.filtro.inicio) {
            this.filtro.inicio          = this.apiService.date();
            this.filtro.inicio_hora     = '00:00';
            this.filtro.fin             = this.apiService.date();
            this.filtro.fin_hora        = '23:59';
            this.filtro.nombre          = '';
        }

        this.loading = true;

        this.apiService.store('ventas/historial', this.filtro).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

    }


}
