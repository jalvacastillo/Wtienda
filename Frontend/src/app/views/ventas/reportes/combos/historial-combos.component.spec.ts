import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCombosComponent } from './historial-combos.component';

describe('HistorialCombosComponent', () => {
  let component: HistorialCombosComponent;
  let fixture: ComponentFixture<HistorialCombosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialCombosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCombosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
