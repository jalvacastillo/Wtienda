import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-entradas',
  templateUrl: './entradas.component.html'
})

export class EntradasComponent implements OnInit {

    public entradas:any = [];
	public entrada:any = {};
    public buscador:any = '';
    public loading:boolean = false;
    modalRef?: BsModalRef;
    
    constructor(private apiService: ApiService, private alertService: AlertService, private modalService: BsModalService){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('entradas').subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    openModal(template: TemplateRef<any>, entrada:any) {
        this.entrada = entrada;
        this.modalRef = this.modalService.show(template);
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('entradas/buscar/', this.buscador).subscribe(entradas => { 
                this.entradas = entradas;
            }, error => {this.alertService.error(error); });
        }
    }

    public onSubmit() {
        // Guardamos
        this.loading = true;
        
        if (!this.entrada.id) { 
            this.entrada.usuario_id = this.apiService.auth_user().id;
        }

        this.apiService.store('entrada', this.entrada).subscribe(entrada => {
            this.alertService.success("Entrada guardada");
            if (!this.entrada.id) { 
                this.entradas.data.unshift(entrada);
            }
            this.modalRef!.hide();
            this.entrada = {};
            this.loading = false;
        },error => {
            this.alertService.error(error);
            this.loading = false;
        });
    }


    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.entradas.path + '?page='+ event.page).subscribe(entradas => { 
            this.entradas = entradas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
