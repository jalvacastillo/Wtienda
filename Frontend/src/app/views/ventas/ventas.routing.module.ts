import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { LayoutComponent } from '../../layout/layout.component';

// Ventas
    import { VentasComponent } from './ventas.component';
    import { VentaComponent } from './venta/venta.component';
    import { EntradasComponent } from './entradas/entradas.component';
    import { DevolucionesVentasComponent } from './devoluciones/devoluciones-ventas.component';
    import { DevolucionVentaComponent } from './devoluciones/devolucion/devolucion-venta.component';
    import { ValesComponent } from './vales/vales.component';
    import { HistorialVentasComponent } from './reportes/historial/historial-ventas.component';
    import { DetalleVentasComponent } from './reportes/detalle/detalle-ventas.component';
    import { CategoriasVentasComponent } from './reportes/categorias/categorias-ventas.component';
    import { HistorialCombosComponent } from './reportes/combos/historial-combos.component';
    
    import { FacturacionComponent } from '../facturacion/facturacion.component';

// Clientes
    import { ClientesComponent } from './clientes/clientes.component';
    import { ClienteComponent } from './clientes/cliente/cliente.component';
    import { CuentasCobrarComponent } from './clientes/cuentas-cobrar/cuentas-cobrar.component';
    import { AnticiposComponent } from './clientes/anticipos/anticipos.component';
    import { AnticipoComponent } from './clientes/anticipos/anticipo/anticipo.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    title: 'Ventas',
    children: [
        // Facturación
            { path: 'facturacion/:id', component: FacturacionComponent},
        // Ventas
            { path: 'ventas', component: VentasComponent},
            { path: 'venta/:id', component: VentaComponent},
            { path: 'entradas', component: EntradasComponent},
            { path: 'devoluciones/ventas', component: DevolucionesVentasComponent},
            { path: 'devolucion/venta/:id', component: DevolucionVentaComponent},
            { path: 'vales', component: ValesComponent},
            
            { path: 'reporte/historial/ventas', component: HistorialVentasComponent},
            { path: 'reporte/detalle/ventas', component: DetalleVentasComponent},
            { path: 'reporte/detalle/combos', component: HistorialCombosComponent},
            { path: 'reporte/categorias/ventas', component: CategoriasVentasComponent},
        
        // Clientes
            { path: 'clientes', component: ClientesComponent},
            { path: 'cliente/:id', component: ClienteComponent},
            { path: 'cliente/:id/:estado', component: ClienteComponent},
            { path: 'cuentas-cobrar', component: CuentasCobrarComponent},
            { path: 'anticipos', component: AnticiposComponent},
            { path: 'anticipo/:id', component: AnticipoComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentasRoutingModule { }
