import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';


declare var $:any;

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html'
})

export class VentasComponent implements OnInit {

    public ventas:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];
    
    modalRef?: BsModalRef;

    constructor(
        public apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;

        // Si es adminstrador mostramos todas las ventas sino solo las del turno
        if (this.apiService.auth_user().tipo == "Administrador" || this.apiService.auth_user().tipo == "Supervisor") { 
            this.apiService.getAll('ventas').subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;this.filtrado = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        } else {
            this.apiService.getAll('ventas/corte').subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;this.filtrado = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }
    }

    public search(){
        if(this.buscador && this.buscador.length > 1) {
            this.loading = true;
            this.apiService.read('ventas/buscar/', this.buscador).subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false; this.filtrado = true;
            }, error => {this.alertService.error(error); this.loading = false;});
        }else{
            this.loadAll();
        }
    }


    public setEstado(venta:any, estado:string){
        venta.estado = estado;
        this.apiService.store('venta', venta).subscribe(venta => { 
            this.alertService.success('Actualizado');
            if((this.apiService.auth_user().tipo == 'Empleado') && venta.estado == "Anulada" && (venta.tipo_documento == 'Factura' || venta.tipo_documento == 'Credito Fiscal')){
                if (confirm('¿Desea imprimir la factura como anulada?')) {
                    window.open(this.apiService.baseUrl + '/api/reporte/anulacion?token=' + this.apiService.auth_token(), 'hola', 'width=400');
                }
            }
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('venta/', id) .subscribe(data => {
                for (let i = 0; i < this.ventas['data'].length; i++) { 
                    if (this.ventas['data'][i].id == data.id )
                        this.ventas['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
        }
    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.ventas.path + '?page='+ event.page).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


    // Filtros
    openFilter(template: TemplateRef<any>) {     

        if(!this.filtrado) {
            this.filtro.inicio = null;
            this.filtro.fin = null;
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.metodo_pago = '';
            this.filtro.tipo_documento = '';
        }
        if(!this.usuarios.data){
            this.apiService.getAll('usuarios/filtrar/tipo/Empleado').subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('ventas/filtrar', this.filtro).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false; this.filtrado = true;
            this.modalRef!.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    reemprimir(venta:any){
        if(venta.tipo_documento == 'Factura' || venta.tipo_documento == 'Credito Fiscal' || venta.tipo_documento == 'Ticket'){
            window.open(this.apiService.baseUrl + '/api/reporte/facturacion/' + venta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }
    }

}
