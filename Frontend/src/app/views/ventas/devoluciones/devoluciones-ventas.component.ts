import { Component, OnInit, TemplateRef } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-devoluciones-ventas',
  templateUrl: './devoluciones-ventas.component.html'
})

export class DevolucionesVentasComponent implements OnInit {

    public ventas:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    constructor(public apiService: ApiService, private alertService: AlertService){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;

        // Si es adminstrador mostramos todas las ventas sino solo las del turno
        if (this.apiService.auth_user().tipo == "Administrador" || this.apiService.auth_user().tipo == "Supervisor") { 
            this.apiService.getAll('devoluciones-ventas').subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        } else {
            this.apiService.getAll('devoluciones-ventas/corte').subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }
    }

    public search(){
        this.loading = true;
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('devolucion-ventas/buscar/', this.buscador).subscribe(ventas => { 
                this.ventas = ventas;
                this.loading = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }else{
            this.loadAll();
        }
    }

    public filtrar(filtro:any, txt:any){
        this.loading = true;
        this.apiService.read('devolucion-ventas/' + filtro + '/', txt).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); });

    }

    public print(venta:any){
        window.open(this.apiService.baseUrl + 'reporte/devolucion/' + venta.id + '?token=' + this.apiService.auth_token(), 'Venta', 'width:400');
    }

    reemprimir(venta:any){
        if(venta.tipo_documento == 'Factura' || venta.tipo_documento == 'Credito Fiscal' || venta.tipo_documento == 'Ticket'){
            window.open(this.apiService.baseUrl + '/api/reporte/devolucion/' + venta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }
    }

    public setEstado(venta:any, estado:string){
        venta.estado = estado;
        this.apiService.store('venta', venta).subscribe(venta => { 
            this.alertService.success('Actualizado');
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('devolucion-venta/', id) .subscribe(data => {

                for (let i = 0; i < this.ventas['data'].length; i++) { 
                    if (this.ventas['data'][i].id == data.id )
                        this.ventas['data'].splice(i, 1);
                }

            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.ventas.path + '?page='+ event.page).subscribe(ventas => { 
            this.ventas = ventas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
