import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'
})
export class ClienteComponent implements OnInit {

	public cliente: any = {};
    public loading = false;
    public filtro:any = {}; //Para activar el tab que se necesite

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    const id = +this.route.snapshot.paramMap.get('id')!;
        this.filtro.estado = "";
        if(this.route.snapshot.paramMap.get('estado'))
            this.filtro.estado = this.route.snapshot.paramMap.get('estado');
	        
        if(isNaN(id)){
            this.cliente = {};
            this.cliente.usuario_id = this.apiService.auth_user().id;
        }
        else{
            // Optenemos el cliente
            this.loading = true; 
            this.apiService.read('cliente/', id).subscribe(cliente => {
               this.cliente = cliente;
               this.loading = false; 
            },error => {this.alertService.error(error); this.loading = false; });
        }
	}

	public onSubmit() {
	    this.loading = true;
	    // Guardamos al cliente
	    this.apiService.store('cliente', this.cliente).subscribe(cliente => {
	        this.cliente = cliente;
	        this.loading = false;
	        this.router.navigate(['/cliente/'+ cliente.id]);
	    },error => {
	        this.alertService.error(error);
	        this.loading = false;
	    });
	}

}
