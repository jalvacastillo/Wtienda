import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-anticipo',
  templateUrl: './anticipo.component.html'
})
export class AnticipoComponent implements OnInit {

	public anticipo: any = {};
	public clientes:any = [];
	public cliente: any = {};
    public searching = false;
    public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    const id = +this.route.snapshot.paramMap.get('id')!;

        if(isNaN(id)){
            this.anticipo = {};
            this.cliente = {};
            this.anticipo.fecha = this.apiService.datetime();
            this.anticipo.usuario_id = this.apiService.auth_user().id;
        }
        else{
            // Optenemos el anticipo
            this.apiService.read('anticipo/', id).subscribe(anticipo => {
               this.anticipo = anticipo;
               this.cliente = anticipo.cliente;
            });
        }

	}

    searchCliente(){
    	if(this.cliente.nombre && this.cliente.nombre.length > 1) {
    		this.searching = true;
    		this.apiService.read('clientes/buscar/', this.cliente.nombre).subscribe(clientes => {
    		   this.clientes = clientes;
    			this.searching = false;
    		}, error => {this.alertService.error(error._body); this.searching = false;});
    	}else if (!this.cliente.nombre  || this.cliente.nombre.length < 1){ this.searching = false; this.cliente = {}; this.clientes.total = 0;}
    }

    selectCliente(cliente:any){
    	this.cliente = cliente;
    	this.anticipo.cliente_id = cliente.id;
    	if(this.clientes) {this.clientes.total = 0; }
    }


	public onSubmit() {
	    this.loading = true;

	    this.anticipo.tipo = 'Abono'

	    // Guardamos al anticipo
	    this.apiService.store('anticipo', this.anticipo).subscribe(anticipo => {
	        this.anticipo = anticipo;
            this.router.navigate(['/anticipos']);
	    },error => {this.alertService.error(error); this.loading = false; });
	}

}
