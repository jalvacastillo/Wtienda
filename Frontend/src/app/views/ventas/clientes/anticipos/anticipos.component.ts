import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-anticipos',
  templateUrl: './anticipos.component.html',
})
export class AnticiposComponent implements OnInit {

	public anticipos:any = [];
    public buscador:any = '';

    public filtro:any = {};
    public filtrado:boolean = false;
    public loading:boolean = false;

    modalRef!: BsModalRef;

    constructor(private apiService: ApiService, private alertService: AlertService,
                private modalService: BsModalService
        ){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('anticipos').subscribe(anticipos => { 
            this.anticipos = anticipos;
            this.loading = false;this.filtrado = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
    	if(this.buscador && this.buscador.length > 2) {
	    	this.apiService.read('anticipos/buscar/', this.buscador).subscribe(anticipos => { 
	    	    this.anticipos = anticipos;
                this.loading = false;this.filtrado = false;
	    	}, error => {this.alertService.error(error); this.loading = false;this.filtrado = false; });
    	}
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('pago-anticipado/', id) .subscribe(data => {
                for (let i = 0; i < this.anticipos['data'].length; i++) { 
                    if (this.anticipos['data'][i].id == data.id )
                        this.anticipos['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    // Filtros
    openFilter(template: TemplateRef<any>) {
        if(!this.filtro.fecha_ini) {
            this.filtro.fecha_ini = this.apiService.date();
            this.filtro.fecha_fin = this.apiService.date();
            this.filtro.estado = '';
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('anticipos/filtrar', this.filtro).subscribe(anticipos => { 
            this.anticipos = anticipos;
            this.loading = false; this.filtrado = true;
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.anticipos.path + '?page='+ event.page).subscribe(anticipos => { 
            this.anticipos = anticipos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
