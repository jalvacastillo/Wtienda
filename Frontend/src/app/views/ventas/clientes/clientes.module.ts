import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FocusModule } from 'angular2-focus';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask'
import { PipesModule } from '../../../pipes/pipes.module';
import { SharedModule } from '../../../shared/shared.module';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { ClientesComponent } from './clientes.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteVentasComponent } from './cliente/ventas/cliente-ventas.component';
import { ClienteAnticiposComponent } from './cliente/anticipos/cliente-anticipos.component';

import { AnticiposComponent } from './anticipos/anticipos.component';
import { AnticipoComponent } from './anticipos/anticipo/anticipo.component';

import { CuentasCobrarComponent } from './cuentas-cobrar/cuentas-cobrar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    NgxMaskDirective,
    PopoverModule.forRoot(),
    FocusModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
  	ClientesComponent,
    ClienteComponent,
    ClienteVentasComponent,
    ClienteAnticiposComponent,
    AnticiposComponent,
    AnticipoComponent,
    CuentasCobrarComponent
  ],
  exports: [
  	ClientesComponent,
    ClienteComponent,
    ClienteVentasComponent,
    ClienteAnticiposComponent,
    AnticiposComponent,
    AnticipoComponent,
    CuentasCobrarComponent
  ]
})
export class ClientesModule { }
