import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { FocusModule } from 'angular2-focus';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

import { VentasRoutingModule } from './ventas.routing.module';

import { VentasComponent } from './ventas.component';
import { VentaComponent } from './venta/venta.component';
import { EntradasComponent } from './entradas/entradas.component';
import { ValesComponent } from './vales/vales.component';

import { DevolucionesVentasComponent } from './devoluciones/devoluciones-ventas.component';
import { DevolucionVentaComponent } from './devoluciones/devolucion/devolucion-venta.component';

import { HistorialVentasComponent } from './reportes/historial/historial-ventas.component';
import { DetalleVentasComponent } from './reportes/detalle/detalle-ventas.component';
import { CategoriasVentasComponent } from './reportes/categorias/categorias-ventas.component';
import { HistorialCombosComponent } from './reportes/combos/historial-combos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    VentasRoutingModule,
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    ModalModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
  	VentasComponent,
    VentaComponent,
    ValesComponent,
    EntradasComponent,
    DevolucionesVentasComponent,
    DevolucionVentaComponent,
    HistorialVentasComponent,
    DetalleVentasComponent,
    CategoriasVentasComponent,
    HistorialCombosComponent
  ],
  exports: [
  	VentasComponent,
    VentaComponent,
    ValesComponent,
    EntradasComponent,
    DevolucionesVentasComponent,
    DevolucionVentaComponent,
    HistorialVentasComponent,
    DetalleVentasComponent,
    CategoriasVentasComponent,
    HistorialCombosComponent
  ]
})
export class VentasModule { }
