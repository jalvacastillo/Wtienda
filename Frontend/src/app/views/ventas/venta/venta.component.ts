import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SumPipe }     from '../../../pipes/sum.pipe';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  providers: [ SumPipe ]
})

export class VentaComponent implements OnInit {

    public venta: any= {};
    public loading = false;

    modalRef?: BsModalRef;
    
    constructor( 
        private apiService: ApiService, private alertService: AlertService, private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService, private sumPipe:SumPipe
    ) {}

    ngOnInit() {
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };

        const id = +this.route.snapshot.paramMap.get('id')!;

        if(isNaN(id)){
            this.router.navigate(['/ventas']);
        }
        else{
            this.venta.id = id;
            this.loading = true;
            this.apiService.read('venta/', id).subscribe(venta => {
                this.venta = venta;
                this.loading = false;
            }, error => {this.alertService.error(error);  this.loading = false;});
        }

    }


}
