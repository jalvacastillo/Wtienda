import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

declare var $:any;

@Component({
  selector: 'app-vales',
  templateUrl: './vales.component.html'
})

export class ValesComponent implements OnInit {

	public vales:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];
    modalRef!: BsModalRef;

    constructor(public apiService: ApiService, private alertService: AlertService,
                private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        // Si es adminstrador mostramos todas los vales sino solo las del turno
        if (this.apiService.auth_user().tipo == "Administrador" || this.apiService.auth_user().tipo == "Supervisor") { 
            this.apiService.getAll('vales').subscribe(vales => { 
                this.vales = vales;
                this.loading = false; this.filtrado = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        } else {
            this.apiService.getAll('vales/corte').subscribe(vales => { 
                this.vales = vales;
                this.loading = false; this.filtrado = false;
            }, error => {this.alertService.error(error); this.loading = false;});
        }

    }

    public search(){
        this.loading = true;
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('vales/buscar/', this.buscador).subscribe(vales => { 
                this.vales = vales;
                this.loading = false;
            }, error => {this.alertService.error(error); });
        }else{
            this.loadAll();
        }
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('venta/', id) .subscribe(data => {

                for (let i = 0; i < this.vales['data'].length; i++) { 
                    if (this.vales['data'][i].id == data.id )
                        this.vales['data'].splice(i, 1);
                }

            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.vales.path + '?page='+ event.page).subscribe(vales => { 
            this.vales = vales;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


    // Filtros
    openFilter(template: TemplateRef<any>) {
        
        if(!this.filtrado) {
            this.filtro.inicio = null;
            this.filtro.fin = null;
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
        }
        if(!this.usuarios.data){
            this.apiService.getAll('usuarios/filtrar/tipo/Empleado').subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('vales/filtrar', this.filtro).subscribe(vales => { 
            this.vales = vales;
            this.loading = false; this.filtrado = true;
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

}
