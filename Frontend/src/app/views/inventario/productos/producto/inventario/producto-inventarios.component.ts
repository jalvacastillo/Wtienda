import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-producto-inventarios',
  templateUrl: './producto-inventarios.component.html'
})
export class ProductoInventariosComponent implements OnInit {

    @Input() producto: any = {};
    public bodegas: any = [];
    public bodega: any = {};
    public ajuste:any = {};
    public buscador:string = '';
    public loading:boolean = false;

    modalRef!: BsModalRef;

    constructor(private apiService: ApiService, private alertService: AlertService,  
        private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
    ){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll(){
        this.loading = true;
        this.apiService.getAll('bodegas').subscribe(bodegas => {
            this.bodegas = bodegas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false; });
    }

    public setAjuste(event:any){
        this.bodega.stock = event.stock_final;
    }

    openModalInventario(template: TemplateRef<any>, bodega:any) {
        this.bodega = bodega;
        if (!this.bodega.id){
            this.bodega.stock = 0;
            this.bodega.bodega_id = 1;
        }
        this.bodega.producto_id = this.producto.id;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

    public agregarInventario() {
        this.loading = true;
        this.apiService.store('bodega-producto', this.bodega).subscribe(bodega => {
            if(!this.bodega.id)
                this.producto.bodegas.push(bodega);
            this.bodega = {};
            this.loading = false;
            this.modalRef.hide();
        },error => {this.alertService.error(error); this.loading = false; });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('bodega-producto/', id) .subscribe(data => {
                for (let i = 0; i < this.producto.bodegas.length; i++) { 
                    if (this.producto.bodegas[i].id == data.id )
                        this.producto.bodegas.splice(i, 1);
                }
                this.alertService.success("Registro eliminado");
            }, error => {this.alertService.error(error); });
                   
        }

    }

}
