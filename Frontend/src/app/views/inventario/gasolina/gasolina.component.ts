import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-gasolina',
  templateUrl: './gasolina.component.html'
})
export class GasolinaComponent implements OnInit {

	public gasolinas:any = [];
	public tanque:any ={};
    public gasolina:any ={};
    public loading = false;
    modalRef!: BsModalRef;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
		private modalService: BsModalService
	) { }

	ngOnInit() {
        this.loadAll();
    }

    openModal(template: TemplateRef<any>, tanque:any) {
        this.tanque = tanque;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});

    }

    openModalGas(template: TemplateRef<any>, gasolina:any) {
        this.gasolina = gasolina;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});

    }

    public loadAll() {
        this.apiService.getAll('productos/gasolina').subscribe(gasolinas => { 
            this.gasolinas = gasolinas;
        }, error => {this.alertService.error(error); });
    }

    public onSubmit() {
        this.loading = true;
        // Guardamos la caja
        this.apiService.store('tanque', this.tanque).subscribe(tanque => {
            this.tanque = {};
            this.alertService.success("Datos guardados");
            this.loading = false;
            this.modalRef.hide();
        },error => {this.alertService.error(error); this.loading = false;
        });
    }

    public onSubmitGas() {
        this.loading = true;
        // Guardamos la caja
        this.apiService.store('producto', this.gasolina).subscribe(gasolina => {
            this.gasolina = {};
            this.alertService.success("Datos guardados");
            this.loading = false;
            this.modalRef.hide();
        },error => {this.alertService.error(error); this.loading = false;
        });
    }


}
