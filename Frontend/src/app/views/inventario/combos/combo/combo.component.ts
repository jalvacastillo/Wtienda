import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

declare var $:any;

@Component({
  selector: 'app-combo',
  templateUrl: './combo.component.html'
})
export class ComboComponent implements OnInit {

	public combo: any = {};
	public detalles: any[] = [];
	public opciones: any[] = [];
	public opcion: any = {};
	public detalle: any = {};

	public categorias: any[] = [];
	public productos:any = [];
	public producto: any = {};

    public loading = false;
    modalRef!: BsModalRef;
    @ViewChild('medit')
    public wizardRef!: TemplateRef<any>;  

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router, private modalService: BsModalService
	) { }

	ngOnInit() {

        setTimeout(()=> {
            $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
            $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
        },200);
	    
	    this.route.params.subscribe(params => {
	        
	        if(isNaN(params['id'])){
	            this.combo = {};
	        }
	        else{
	        	this.loading = true;
	            // Optenemos el combo
	            this.apiService.read('combos-detalle/', params['id']).subscribe(combo => {
	                this.combo = combo;
	                this.detalles = combo.detalles;
	        	    this.loading = false;
	            });
	        }

	    });


	}

	openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

	searchProducto(){
    	if(this.producto.nombre && this.producto.nombre.length > 0) {
    		this.apiService.read('productos/buscar/', this.producto.nombre).subscribe(productos => {
    		   this.productos = productos;
    		});
    	}else{
    		this.productos.data = [];
    		this.producto = {};
    	}
    }


    productoSelect(event:any):any{
    	this.detalle = event.producto;
    	this.detalle.id = null;
        this.detalle.combo_id = this.combo.id;
    	this.editDetalle(this.wizardRef, this.detalle);

	}

    editDetalle(template: TemplateRef<any>, detalle:any){
    	this.detalle = detalle;
    	console.log(detalle);
    	this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

	updateDetalle(){
    	this.apiService.store('combo-detalle', this.detalle).subscribe(detalle => {
        	 this.apiService.read('combos-detalle/', this.combo.id).subscribe(combo => {
               	this.combo = combo;
               	this.detalles = combo.detalles;
        		this.loading = false;
        		this.modalRef.hide();
            });
		}, error => {this.alertService.error(error); this.loading = false; });
	}


	public deleteDetalle(detalle:any){
		if (confirm('¿Desea eliminar el Registro?')) {
    		this.apiService.delete('combo-detalle/', detalle.id).subscribe(detalle => {
		        this.apiService.read('combos-detalle/', this.combo.id).subscribe(combo => {
	               this.combo = combo;
	               this.detalles = combo.detalles;
	        		this.loading = false;
	            });
		    }, error => {this.alertService.error(error); });
		}
	}
	

	public onSubmit() {

		this.loading = true;
    	this.apiService.store('combo', this.combo).subscribe(combo => {
    		this.combo = combo;
        	this.loading = false;
        	this.alertService.success("Guardado");
			this.router.navigate(['/combo/' + combo.id]);
		}, error => {this.alertService.error(error); this.loading = false; });


	}

	openModalO(template: TemplateRef<any>, detalle:any) {
		this.apiService.read('combos-detalle-opciones/', detalle.id).subscribe(detalle => {
		   this.detalle = detalle;
		   this.opciones = detalle.opciones;
		});
        this.modalRef = this.modalService.show(template, {class: 'modal-md'});
    }


    agregarOpcion(producto:any){
    	this.productos.data = [];
    	this.opcion.producto_id = producto.id;
        this.opcion.combo_detalle_id = this.detalle.id;
    	this.apiService.store('combo-detalle-opcion', this.opcion).subscribe(opcion => {
			this.opciones.push(opcion);
			this.producto = {};
			this.opcion = {};
		}, error => {this.alertService.error(error); this.loading = false; });
    }

	public deleteOpcion(opcion:any){
		if (confirm('¿Desea eliminar el Registro?')) {
    		this.apiService.delete('combo-detalle-opcion/', opcion.id).subscribe(opcion => {
		        for (let i = 0; i < this.opciones.length; i++) { 
		            if (this.opciones[i].id == opcion.id )
		                this.opciones.splice(i, 1);
		        }
		    }, error => {this.alertService.error(error); });
		}
	}

}
