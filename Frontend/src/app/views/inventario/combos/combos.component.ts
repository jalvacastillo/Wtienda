import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-combos',
  templateUrl: './combos.component.html',
})
export class CombosComponent implements OnInit {

    public combos:any = [];

    public buscador:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService
    ){ }

    ngOnInit() {
        this.loadAll();
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('combos/buscar/', this.buscador).subscribe(combos => { 
                this.combos = combos;
            }, error => {this.alertService.error(error); });
        }
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('combos').subscribe(combos => { 
            this.combos = combos;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }


    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('combo/', id) .subscribe(data => {
                for (let i = 0; i < this.combos['data'].length; i++) { 
                    if (this.combos['data'][i].id == data.id )
                        this.combos['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.combos.path + '?page='+ event.page).subscribe(combos => { 
            this.combos = combos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
