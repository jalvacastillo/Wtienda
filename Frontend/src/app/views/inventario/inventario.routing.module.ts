import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { LayoutComponent } from '../../layout/layout.component';

// Inventario

    import { ProductosComponent } from './productos/productos.component';
    import { ProductoComponent } from './productos/producto/producto.component';
    import { GasolinaComponent } from './gasolina/gasolina.component';
    import { MateriasPrimaComponent } from './materias-prima/materias-prima.component';
    import { MateriaPrimaComponent } from './materias-prima/materia-prima/materia-prima.component';
    import { KardexComponent } from './kardex/kardex.component';
    import { BodegaComponent } from './bodega/bodega.component';
    import { SalaVentaComponent } from './sala-venta/sala-venta.component';
    import { TrasladosComponent } from './traslados/traslados.component';
    import { TrasladoComponent } from './traslados/traslado/traslado.component';
    import { AjustesComponent } from './ajustes/ajustes.component';
    import { AjusteComponent } from './ajustes/ajuste/ajuste.component';
    import { TelefoniaComponent } from './telefonia/telefonia.component';
    import { CombosComponent } from './combos/combos.component';
    import { ComboComponent } from './combos/combo/combo.component';

    import { AnalisisProductosComponent } from './analisis/analisis-productos.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    title: 'Dashboard',
    children: [
        { path: 'productos', component: ProductosComponent},
        { path: 'producto/:id', component: ProductoComponent},
        { path: 'gasolinas', component: GasolinaComponent},
        { path: 'materias-primas', component: MateriasPrimaComponent},
        { path: 'materia-prima/:id', component: MateriaPrimaComponent},
        { path: 'producto/kardex/:id', component: KardexComponent},
        { path: 'inventario/bodega', component: BodegaComponent},
        { path: 'inventario/sala-venta', component: SalaVentaComponent},
        { path: 'traslados', component: TrasladosComponent},
        { path: 'traslado/:id', component: TrasladoComponent},
        { path: 'ajustes', component: AjustesComponent},
        { path: 'ajuste/:id', component: AjusteComponent},
        { path: 'telefonia', component: TelefoniaComponent},
        { path: 'combos', component: CombosComponent},
        { path: 'combo/:id', component: ComboComponent},
        { path: 'analisis/productos', component: AnalisisProductosComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventarioRoutingModule { }
