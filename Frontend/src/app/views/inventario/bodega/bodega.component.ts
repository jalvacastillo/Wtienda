import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-bodega',
  templateUrl: './bodega.component.html',
})
export class BodegaComponent implements OnInit {

    public bodegaP:any = [];
    public producto:any = {};
    public ajuste:any = {};

    public filtro:any = {};
    public filtrado:boolean = false;
    public categorias:any =[];
    public buscador:any = '';
    public loading:boolean = false;

    modalRef!: BsModalRef;

    constructor(public apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('bodega-productos/1').subscribe(bodegaP => { 
            this.bodegaP = bodegaP;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    openModal(template: TemplateRef<any>, producto:any) {
        this.producto = producto;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('bodega-productos/bodega/buscar/', this.buscador).subscribe(bodegaP => { 
                this.bodegaP = bodegaP;
            }, error => {this.alertService.error(error); });
        }
    }

    public onSubmit() {
        this.loading = true;
        this.apiService.store('bodega-producto', this.producto).subscribe(producto => {
            this.producto = {};
            this.alertService.success("Datos guardados");
            this.loading = false;
            this.modalRef.hide();
        },error => {this.alertService.error(error._body); this.loading = false; });
    }

    // Ajustes

        public openModalAjuste(template: TemplateRef<any>, bodega:any) {
            this.producto = bodega;
            this.ajuste.producto_id = bodega.producto_id;
            this.ajuste.bodega_id = bodega.bodega_id;
            this.ajuste.stock_inicial = bodega.stock;
            this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
        }
        
        public onSubmitAjuste() {

            this.loading = true;
            this.ajuste.usuario_id = this.apiService.auth_user().id;
            this.apiService.store('ajuste', this.ajuste).subscribe(ajuste => {
                this.ajuste = {};
                this.producto.stock = ajuste.stock_final;
                this.loading = false;
                this.alertService.success("Guardado");
                this.modalRef.hide();
            }, error => {this.alertService.error(error._body); this.loading = false; });


        }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.bodegaP.path + '?page='+ event.page).subscribe(bodegaP => { 
            this.bodegaP = bodegaP;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    // Filtros
        public openFilter(template: TemplateRef<any>) {
            if(!this.filtro.categorias_id) {
                this.filtro.categorias_id = [];
            }
            if(!this.categorias.length){
                this.apiService.getAll('categorias').subscribe(categorias => { 
                    this.categorias = categorias;
                }, error => {this.alertService.error(error); });
            }
            this.modalRef = this.modalService.show(template);
        }

        public onFiltrar(){
            this.loading = true;
            if (this.filtro.categorias_id[0] == '') {
                this.filtro.categorias_id = null;
            }
            this.apiService.store('bodega-productos/filtrar-bodega', this.filtro).subscribe(bodegaP => { 
                this.bodegaP = bodegaP;
                this.loading = false; this.filtrado = true;
                this.modalRef.hide();
            }, error => {this.alertService.error(error); this.loading = false;});

        }

}
