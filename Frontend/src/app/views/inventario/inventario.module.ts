import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FocusModule } from 'angular2-focus';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask'
import { NgChartsModule } from 'ng2-charts';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

import { InventarioRoutingModule } from './inventario.routing.module';

import { ProductosComponent } from './productos/productos.component';
import { ProductoComponent } from './productos/producto/producto.component';
import { ProductoInventariosComponent } from './productos/producto/inventario/producto-inventarios.component';
import { ProductoComposicionComponent } from './productos/producto/composicion/producto-composicion.component';
import { ProductoPromocionesComponent } from './productos/producto/promociones/producto-promociones.component';
import { ProductoPreciosComponent } from './productos/producto/precios/producto-precios.component';
import { ProductoComprasComponent } from './productos/producto/compras/producto-compras.component';

import { BodegaComponent } from './bodega/bodega.component';
import { SalaVentaComponent } from './sala-venta/sala-venta.component';
import { KardexComponent } from './kardex/kardex.component';
import { GasolinaComponent } from './gasolina/gasolina.component';
import { TrasladosComponent } from './traslados/traslados.component';
import { TrasladoComponent } from './traslados/traslado/traslado.component';
import { AjustesComponent } from './ajustes/ajustes.component';
import { AjusteComponent } from './ajustes/ajuste/ajuste.component';

import { MateriasPrimaComponent } from './materias-prima/materias-prima.component';
import { MateriaPrimaComponent } from './materias-prima/materia-prima/materia-prima.component';
import { MateriaPrimaInformacionComponent } from './materias-prima/materia-prima/informacion/materia-prima-informacion.component';

import { TelefoniaComponent } from './telefonia/telefonia.component';
import { CombosComponent } from './combos/combos.component';
import { ComboComponent } from './combos/combo/combo.component';

import { AnalisisProductosComponent } from './analisis/analisis-productos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    PipesModule,
    NgChartsModule,
    NgxMaskDirective,
    InventarioRoutingModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
  	ProductosComponent,
    BodegaComponent,
    SalaVentaComponent,
    KardexComponent,
    ProductoComponent,
    ProductoInventariosComponent,
    ProductoComposicionComponent,
    ProductoPromocionesComponent,
    ProductoPreciosComponent,
    ProductoComprasComponent,
    GasolinaComponent,
    TrasladosComponent,
    TrasladoComponent,
    AjustesComponent,
    AjusteComponent,
    MateriasPrimaComponent,
    MateriaPrimaComponent,
    MateriaPrimaInformacionComponent,
    TelefoniaComponent,
    CombosComponent,
    ComboComponent,
    AnalisisProductosComponent
  ],
  exports: [
  	ProductosComponent,
    BodegaComponent,
    SalaVentaComponent,
    KardexComponent,
    ProductoComponent,
    ProductoInventariosComponent,
    ProductoComposicionComponent,
    ProductoPromocionesComponent,
    ProductoPreciosComponent,
    ProductoComprasComponent,
    GasolinaComponent,
    TrasladosComponent,
    TrasladoComponent,
    AjustesComponent,
    AjusteComponent,
    MateriasPrimaComponent,
    MateriaPrimaComponent,
    MateriaPrimaInformacionComponent,
    TelefoniaComponent,
    CombosComponent,
    ComboComponent,
    AnalisisProductosComponent
  ]
})
export class InventarioModule { }
