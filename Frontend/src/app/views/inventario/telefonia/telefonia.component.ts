import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';


@Component({
  selector: 'app-telefonia',
  templateUrl: './telefonia.component.html'
})
export class TelefoniaComponent implements OnInit {

    public telefonias:any = [];
	public datos:any = [];
	public bodegas:any = {};
    public loading = false;

    public usuarios:any[] = [];
    public filtro:any = {};

    modalRef!: BsModalRef;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
		private modalService: BsModalService
	) { }

	ngOnInit() {
        this.loadAll();
    }

    openModal(template: TemplateRef<any>, bodegas:any) {
        this.bodegas = bodegas;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});

    }

    public loadAll() {
        this.loading = true;
        this.filtro.inicio = this.apiService.date();
        this.filtro.fin = this.apiService.date();
        this.filtro.usuario_id = this.apiService.auth_user().id;
        this.filtro.empleado_id = 0;

        this.apiService.getAll('productos/telefonia').subscribe(telefonias => { 
            this.telefonias = telefonias.data;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

        this.apiService.store('telefonia-datos', this.filtro).subscribe(datos => { 
            this.datos = datos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

        this.apiService.getAll('usuarios/filtrar/tipo/Empleado').subscribe(usuarios => { 
            this.usuarios = usuarios.data;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});


    }

    public onSubmit() {
        this.loading = true;
        // Guardamos la caja
        this.apiService.store('bodegas', this.bodegas).subscribe(bodegas => {
            this.bodegas = {};
            this.alertService.success("Datos guardados");
            this.loading = false;
            this.modalRef.hide();
        },error => {this.alertService.error(error._body); this.loading = false;
        });
    }

    public filtrar(){
        this.loading = true;
        this.apiService.store('telefonia-datos', this.filtro).subscribe(datos => { 
            this.datos = datos;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('producto/', id) .subscribe(data => {
                for (let i = 0; i < this.telefonias.length; i++) { 
                    if (this.telefonias[i].id == data.id )
                        this.telefonias.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }


}
