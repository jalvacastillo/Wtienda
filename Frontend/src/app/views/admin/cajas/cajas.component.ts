import { Component, OnInit, TemplateRef } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-cajas',
  templateUrl: './cajas.component.html'
})
export class CajasComponent implements OnInit {

    public cajas: any[] = [];
    public loading = false;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService
  	) { }

  	ngOnInit() {
  	    
       this.loadAll();
  	}

    public printX(corte:any){
        window.open(this.apiService.baseUrl + '/api/corte/reporte/' + corte.id + '?token=' + this.apiService.auth_token(), 'Corte #' + corte.id, "top=50,left=300,width=600,height=500");
    }

    public printZ(caja:any){
        window.open(this.apiService.baseUrl + '/api/caja/reporte-dia/' + caja.id + '?token=' + this.apiService.auth_token(), 'Corte #' + caja.id, "top=50,left=300,width=600,height=500");
    }

    public loadAll(){
        this.loading = true;
        this.apiService.getAll('cajas').subscribe(cajas => {
            this.cajas = cajas;
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false;});
    }

}
