import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-caja-documentos',
  templateUrl: './caja-documentos.component.html'
})

export class CajaDocumentosComponent implements OnInit {

    @Input() caja:any = {};
    public documento:any = {};
    public loading:boolean = false;

    modalRef!: BsModalRef;

    constructor(private apiService: ApiService, private alertService: AlertService, 
        private modalService: BsModalService, private route: ActivatedRoute, private router: Router)
    { }

    ngOnInit() {
    }

    openModal(template: TemplateRef<any>, documento:any) {
        this.documento = documento;
        this.modalRef = this.modalService.show(template);
    }

    public onSubmit() {
        this.loading = true;
        this.apiService.store('documento', this.documento).subscribe(documento => {
            this.documento = documento;
            this.modalRef.hide();
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false; });
    }

}
