import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-bodegas',
  templateUrl: './bodegas.component.html'
})
export class BodegasComponent implements OnInit {

    public bodegas: any[] = [];
    public bodega: any = {};
    public loading = false;

    modalRef!: BsModalRef;

  	constructor( 
  	    public apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {
  	    
      this.loadAll();

    }
    
    public loadAll(){
        this.loading = true;
        this.apiService.getAll('bodegas').subscribe(bodegas => {
            this.bodegas = bodegas;
            for (var i = 0; i < this.bodegas.length; ++i) {
              this.apiService.read('bodega/', this.bodegas[i].id).subscribe(data => {
                  for (var j = 0; j < this.bodegas.length; ++j) {
                    if(this.bodegas[j].id == data.id) {
                      this.bodegas[j] = data;
                    }
                  }
              },error => {this.alertService.error(error); this.loading = false;});
            }
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false;});
    }

    openModal(template: TemplateRef<any>, bodega:any) {
        this.bodega = bodega;
        this.modalRef = this.modalService.show(template);
    }

    openModalData(template: TemplateRef<any>, bodega:any) {
        this.loading = true;
        this.apiService.read('bodega/', bodega.id).subscribe(data => {
            this.bodega = data;
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false;});
        this.modalRef = this.modalService.show(template);
    }

  	public onSubmit() {
  	    this.loading = true;
  	    this.apiService.store('bodega', this.bodega).subscribe(bodega => {
  	        this.bodega = bodega;
            this.loading = false;
            this.alertService.success("Datos guardados");
            this.modalRef.hide();
  	    },error => {this.alertService.error(error); this.loading = false; });
  	}

}
