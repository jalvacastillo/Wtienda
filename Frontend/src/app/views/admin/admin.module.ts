import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { FocusModule } from 'angular2-focus';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgChartsModule } from 'ng2-charts';

import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

import { AdminRoutingModule } from './admin.routing.module';

import { EmpresaComponent } from './empresa/empresa.component';
import { PerfilComponent } from './perfil/perfil.component';
import { CajasComponent } from './cajas/cajas.component';
import { CajaComponent } from './cajas/caja/caja.component';
import { CajaCortesComponent } from './cajas/cortes/caja-cortes.component';
import { CajaDocumentosComponent } from './cajas/caja/documentos/caja-documentos.component';
import { CajaEstadisticasComponent } from './cajas/estadisticas/caja-estadisticas.component';
import { TanquesComponent } from './tanques/tanques.component';
import { BombasComponent } from './bombas/bombas.component';
import { BodegasComponent } from './bodegas/bodegas.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioComponent } from './usuarios/usuario/usuario.component';
import { ReportesComponent } from './reportes/reportes.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    PopoverModule,
    SharedModule,
    NgChartsModule,
    AdminRoutingModule,
    TooltipModule.forRoot(),
    FocusModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot()
  ],
  declarations: [
    PerfilComponent,
    EmpresaComponent,
    CajasComponent,
    CajaComponent,
    CajaCortesComponent,
    CajaDocumentosComponent,
    CajaEstadisticasComponent,
    TanquesComponent,
    BombasComponent,
    BodegasComponent,
    CategoriasComponent,
    UsuariosComponent,
    UsuarioComponent,
    ReportesComponent
  ],
  exports: [
    PerfilComponent,
    EmpresaComponent,
    CajasComponent,
    CajaComponent,
    CajaCortesComponent,
    CajaDocumentosComponent,
    CajaEstadisticasComponent,
    TanquesComponent,
    BombasComponent,
    BodegasComponent,
    CategoriasComponent,
    UsuariosComponent,
    UsuarioComponent
  ]
})
export class AdminModule { }
