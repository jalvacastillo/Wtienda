import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

	public usuario: any = {};
	public cajas: any = [];
  public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    const id = +this.route.snapshot.paramMap.get('id')!;
	    if(id) {
	    	this.loading = true;
	        this.apiService.read('usuario/', id).subscribe(usuario => {
	           this.usuario = usuario;
	           this.loading = false;
	           
	        },error => {this.alertService.error(error); this.loading = false; });
	    }else{
	        this.usuario = {};
	        this.usuario.tipo = 'Empleado';
	        this.usuario.caja_id = 1;
	    }
	    this.apiService.getAll('cajas').subscribe(cajas => {
	        this.cajas = cajas;
	    }, error => {this.alertService.error(error);});
	}


	public onSubmit() {
	    this.loading = true;
	    if(this.usuario.tipo == 1) {
	    	this.usuario.caja_id == null;
	    }
	    // Guardamos al usuario
	    this.apiService.store('usuario', this.usuario).subscribe(usuario => {
	        this.usuario = usuario;
	        this.alertService.success("Usuario guardado");
	        this.loading = false;
	        this.router.navigate(['/admin/usuarios']);
	    },error => {this.alertService.error(error); this.loading = false; });

	}

}
