import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})

export class UsuariosComponent implements OnInit {

		public usuarios:any = [];
	    public paginacion = [];
	    public loading:boolean = false;
	    public buscador:any = '';

	    constructor(public apiService: ApiService, private alertService: AlertService){ }

		ngOnInit() {
	        this.loadAll();
	    }

	    public loadAll() {
	    	this.loading = true;
	        this.apiService.getAll('usuarios').subscribe(usuarios => { 
	            this.usuarios = usuarios;
	    		this.loading = false;
	        }, error => {this.alertService.error(error); });
	    }

	    public search(){
	    	if(this.buscador && this.buscador.length > 1) {
		    	this.apiService.read('usuarios/buscar/', this.buscador).subscribe(usuarios => { 
		    	    this.usuarios = usuarios;
		    	}, error => {this.alertService.error(error); });
	    	}
	    }

	    public setEstado(usuario:any, activo:number){
	        usuario.activo = activo;
	        this.apiService.store('usuario', usuario).subscribe(usuario => { 
	            this.alertService.success('Actualizado');
	        }, error => {this.alertService.error(error); });
	    }

	    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('usuario/', id) .subscribe(data => {
                for (let i = 0; i < this.usuarios['data'].length; i++) { 
                    if (this.usuarios['data'][i].id == data.id )
                        this.usuarios['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.usuarios.path + '?page='+ event.page).subscribe(usuarios => { 
            this.usuarios = usuarios;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}

