import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BombasComponent } from './bombas.component';

describe('BombasComponent', () => {
  let component: BombasComponent;
  let fixture: ComponentFixture<BombasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BombasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BombasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
