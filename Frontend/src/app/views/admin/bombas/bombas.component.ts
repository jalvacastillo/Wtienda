import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-bombas',
  templateUrl: './bombas.component.html'
})
export class BombasComponent implements OnInit {

    public bombas: any[] = [];
    public bomba: any = {};
    public loading = false;

    modalRef!: BsModalRef;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {
  	    
        this.loadAll();

  	}

    public loadAll(){
        this.loading = true;
        this.apiService.getAll('bombas').subscribe(bombas => {
            this.bombas = bombas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false; });
    }

    openModal(template: TemplateRef<any>, bomba:any) {
        this.bomba = bomba;
        this.modalRef = this.modalService.show(template);
    }
    

  	public onSubmit() {
  	    this.loading = true;
  	    // Guardamos la bomba
  	    this.apiService.store('bomba', this.bomba).subscribe(bomba => {
  	        this.bomba = bomba;
  	        this.alertService.success("Datos guardados");
  	        this.loading = false;
            this.modalRef.hide();
  	    },error => {this.alertService.error(error); this.loading = false; });
  	}

}
