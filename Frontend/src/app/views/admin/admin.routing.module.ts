import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { LayoutComponent } from '../../layout/layout.component';

import { EmpresaComponent }     from '../../views/admin/empresa/empresa.component';
import { CategoriasComponent } from '../../views/admin/categorias/categorias.component';

import { TanquesComponent } from './tanques/tanques.component';
import { BodegasComponent } from './bodegas/bodegas.component';
import { BombasComponent } from './bombas/bombas.component';
import { CajasComponent } from './cajas/cajas.component';
import { CajaComponent } from './cajas/caja/caja.component';

import { PerfilComponent } from './perfil/perfil.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioComponent } from './usuarios/usuario/usuario.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [
  {
    path: 'admin',
    component: LayoutComponent,
    children: [
        { path: 'tanques', component: TanquesComponent},
        { path: 'bodegas', component: BodegasComponent},
        { path: 'bombas', component: BombasComponent},
        { path: 'categorias', component: CategoriasComponent},
        { path: 'cajas', component: CajasComponent},
        { path: 'caja/:id', component: CajaComponent},

        { path: 'empresa', component: EmpresaComponent},
        { path: 'perfil', component: PerfilComponent},
        { path: 'reportes', component: ReportesComponent},
        { path: 'usuarios', component: UsuariosComponent},
        { path: 'usuario/nuevo', component: UsuarioComponent},
        { path: 'usuario/:id', component: PerfilComponent},
        { path: 'resportes', component: ReportesComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
