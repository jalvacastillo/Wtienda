import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-tanques',
  templateUrl: './tanques.component.html'
})
export class TanquesComponent implements OnInit {

    public tanques: any[] = [];
    public tanque: any = {};
    public ajuste: any = {};
    public loading = false;

    modalRef!: BsModalRef;

    public options:any = {
        maintainAspectRatio: false,
        scales: {yAxes: [{stacked: true }],
        xAxes: [{gridLines: {display: false } }] },
        legend:{display: false, position: 'top', text: 'Ventas'},
        };

    public type:string = 'line';
    public colors = [{borderColor:['#428bca', '#d9534f', '#5cb85c', '#5bc0de', '#f0ad4e']}];
    public datasets:any = [];
    public labels:any = [];

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {
  	    
        this.loadAll();

  	}

    loadAll(){
      this.loading = true;
      this.apiService.getAll('tanques').subscribe(tanques => {
          this.tanques = tanques;
          this.loading = false;
      }, error => {this.alertService.error(error); this.loading = false; });
    }

    openModal(template: TemplateRef<any>, tanque:any) {
        this.tanque = tanque;
        this.modalRef = this.modalService.show(template);
    }

    openModalAjuste(template: TemplateRef<any>, tanque:any) {
          this.tanque = tanque;
          this.ajuste.producto_id = tanque.producto_id;
          this.ajuste.bodega_id = tanque.id;
          this.ajuste.stock_inicial = tanque.stock;
          this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

    openModalAjustes(template: TemplateRef<any>, tanque:any) {
          this.tanque = tanque;
          this.loading = true;
          this.apiService.read('tanque/ajustes/', this.tanque.id).subscribe(tanque => {
              this.datasets     = [{
                  label: 'Precio de venta',
                  data: tanque.ajustes_data
              }];
              this.labels = tanque.ajustes_fechas;
              this.loading = false;
          }, error => {this.alertService.error(error); this.loading = false; });

          this.modalRef = this.modalService.show(template, {class: 'modal-md'});
    }
    
    public onSubmitAjuste() {

        this.loading = true;
        this.ajuste.usuario_id = this.apiService.auth_user().id;
        if(this.ajuste.stock_final > this.tanque.stock_max){
            this.alertService.info("Cantidad máxima excedida.");
            this.loading = false;
        }else{
        this.apiService.store('ajuste', this.ajuste).subscribe(ajuste => {
            this.ajuste = {};
            this.tanque.stock = ajuste.stock_final;
            this.loading = false;
            this.alertService.success("Guardado");
            this.modalRef.hide();
        }, error => {this.alertService.error(error); this.loading = false; });
      }
    }

  	public onSubmit() {
  	    this.loading = true;
  	    // Guardamos la tanque
  	    this.apiService.store('tanque', this.tanque).subscribe(tanque => {
  	        this.tanque = tanque;
  	        this.alertService.success("Datos guardados");
  	        this.loading = false;
  	    },error => {this.alertService.error(error); this.loading = false;
  	    });
  	}

}
