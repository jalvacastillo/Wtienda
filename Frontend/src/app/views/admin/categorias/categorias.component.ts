import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html'
})

export class CategoriasComponent implements OnInit {

    public categorias:any = [];
    public categoria:any = {};
    public buscador:any = '';
    public loading:boolean = false;
    public cambio:any = {};

    modalRef?: BsModalRef;

    @ViewChild('mcategorias')
    public categoriasTemplate!: TemplateRef<any>;

    constructor(public apiService: ApiService, private alertService: AlertService,
                private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('categorias').subscribe(categorias => { 
            this.categorias = categorias;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }


    openModal(template: TemplateRef<any>, categoria:any) {
        this.categoria = categoria;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm', backdrop: 'static'});
    }


    slug(){
        this.categoria.slug = this.apiService.slug(this.categoria.nombre);
    }

    onSubmit():void{
        this.loading = true;
        this.apiService.store('categoria', this.categoria).subscribe(categoria => {
            if(!this.categoria.id){
                categoria.subcategoria = [];
                this.categorias.push(categoria);
            }
            this.loadAll();
            this.loading = false;
            this.alertService.success("Guardado");
            this.modalRef?.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    onNameChange(categoria:any, name:string):void{
        this.categoria = categoria;
        this.categoria.nombre = name;
        this.onSubmit();
    }

    openModalCategorias(categoria:any) {
        this.categoria = categoria;
        if(!this.categorias.lenght){
            this.apiService.getAll('categorias').subscribe(categorias => { 
                this.categorias = categorias;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(this.categoriasTemplate);

    }


    delete(categoria:any) {

        if (confirm('¿Desea aun eliminar la categoria?')) {
            this.apiService.delete('categoria/', categoria.id) .subscribe(data => {
                for (let i = 0; i < this.categorias.length; i++) { 
                    if (this.categorias[i].id == data.id )
                        this.categorias.splice(i, 1);
                }
                this.alertService.success("Categoria eliminada");
            }, error => {
                if (error.status == 422){
                    alert('Hay productos asignados, primero cambie los productos a otra categoria.');
                    this.openModalCategorias(categoria);
                }else{
                    this.alertService.error(error); 
                }
            });
                   
        }

    }


    onChangeCategoria(){
        this.cambio.categoria_anterior = this.categoria.id;
        this.loading = true;
        this.apiService.store('categoria/cambio', this.cambio).subscribe(categoria => {
            this.alertService.success("Se hizo el cambio de categoria");
            setTimeout(()=>{
                this.delete(categoria)
            },1000);
            this.loading = false;
            this.modalRef?.hide();
        }, error => {this.alertService.error(error); this.loading = false;});
    }


}
