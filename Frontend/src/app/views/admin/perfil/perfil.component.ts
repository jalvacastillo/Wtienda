import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html'
})
export class PerfilComponent implements OnInit {

    public usuario: any = {};
    public cajas: any = [];
    public loading = false;
    public id:any;
    public metas: any = {};
    public meta: any = {};
    public meses: any = {};
    public ano:any;


    public options:any = {
        maintainAspectRatio: false,
        scales: {yAxes: [{stacked: true }],
        xAxes: [{gridLines: {display: false } }] },
        legend:{display: false, position: 'top', text: 'Ventas'},
        tooltips: {
            callbacks: {
                label: function(tooltipItem:any, data:any) {
                    return "$" + Number(tooltipItem.yLabel).toFixed(2).replace(/./g, function(c, i, a) {
                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                    });
                }
            }
        } 
        };

    public type:string = 'line';
    public colors = [{borderColor:['#428bca', '#d9534f', '#5cb85c', '#5bc0de', '#f0ad4e']}];
    public datasets:any = [];
    public labels:any = [];

    constructor( 
        public apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {

        this.meses = [{mes:'01', active:true, nombre:'Enero'}, {mes:'02', active:true, nombre:'Febrero'}, {mes:'03', active:true, nombre:'Marzo'}, {mes:'04', active:true, nombre:'Abril'},
                      {mes:'05', active:true, nombre:'Mayo'}, {mes:'06', active:true, nombre:'Junio'}, {mes:'07', active:true, nombre:'Julio'}, {mes:'08', active:true, nombre:'Agosto'}, 
                      {mes:'09', active:true, nombre:'Septiembre'}, {mes:'10', active:true, nombre:'Octubre'}, {mes:'11', active:true, nombre:'Noviembre'}, {mes:'12', active:true, nombre:'Diciembre'}];

        this.id = +this.route.snapshot.paramMap.get('id')!;
        this.loadAll();

        this.apiService.getAll('cajas').subscribe(cajas => {
            this.cajas = cajas;
        }, error => {this.alertService.error(error);});

    }

    public loadAll(){
        this.loading = true;
        if(this.id) {
            this.apiService.read('usuario/', this.id).subscribe(usuario => {
               this.usuario = usuario;
               this.loading = false;
                this.metas = usuario.metas;
                this.validarMetas();
               this.cargarGrafico();
               
            },error => {this.alertService.error(error); this.loading = false; });
        }else{
            this.apiService.read('usuario/', this.apiService.auth_user().id).subscribe(usuario => {
               this.usuario = usuario;
               this.metas = usuario.metas;
               this.validarMetas();
               this.loading = false;
               this.cargarGrafico();

            },error => {this.alertService.error(error); this.loading = false; });
        }
    }
    validarMetas(){
        let today = new Date();
        this.ano = today.getFullYear();

        for (var i = 0; i < this.meses.length; ++i) {

            for (var j = 0; j < this.metas.length; ++j) {
                if (this.metas[j].mes == this.meses[i].mes && this.metas[j].ano == this.ano) { 
                    this.meses[i].id = this.metas[j].id;
                    this.meses[i].active = false;
                    this.meses[i].meta = this.metas[j].meta;
                    this.meses[i].venta = this.metas[j].venta;
                }
            }
        }
    }


    public cargarGrafico(){
        this.datasets = this.usuario.data;
        this.labels = this.usuario.labels;
    }

    public onSubmit() {
        console.log(this.usuario);
        // corregir para no tener error al guardar
            this.usuario.labels    = [];
            this.usuario.data    = [];
            this.usuario.cortes    = [];
        this.loading = true;
        this.apiService.store('usuario', this.usuario).subscribe(usuario => {
            this.loading = false;
            if((this.usuario.id == this.apiService.auth_user().id) && this.usuario.password && this.usuario.password_confirmation) {
                this.router.navigate(['/login']);
                this.alertService.success("Por el cambio de contraseña, se debe iniciar sesión.");
            }else{
                this.alertService.success("Usuario guardado");
                // this.usuario = usuario;
            }
        },error => {this.alertService.error(error); this.loading = false; });
    }

    public onSubmitMeta(meta:any) {
        meta.usuario_id =  this.usuario.id;
        meta.ano        =  this.ano;
        console.log(meta);
        // Guardamos la meta
        this.apiService.store('usuario/meta', meta).subscribe(meta => {
            meta = {};
            this.alertService.success("Meta guardada");
            // this.apiService.read('usuario/metas/', this.usuario.id).subscribe(usuario => {
            //     this.usuario = usuario;
            //     this.metas = usuario.metas;
            //     this.validarMetas();
            // });
        },error => {this.alertService.error(error);});
    }


}
