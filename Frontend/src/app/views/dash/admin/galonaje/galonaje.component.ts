import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-galonaje',
  templateUrl: './galonaje.component.html'
})
export class GalonajeComponent implements OnInit {

    public datos:any = {};
    public tanques:any[] = [];
    public loading = false;
    public filtro:any = {};

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {
        this.filtro.inicio = this.apiService.date();
        this.filtro.fin = this.apiService.date();
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.store('galonajes', this.filtro).subscribe(datos => { 
            this.datos = datos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
