import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgChartsModule } from 'ng2-charts';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html'
})
export class EstadisticaComponent implements OnInit {

    @Input() filtro:any = {};

    public chartOptions:any = {legend:{position:'left'}};
    public chartType:string = 'doughnut';
    public chartLegend:boolean = true;
    public chartColors = [{backgroundColor:['#428bca', '#d9534f', '#5cb85c', '#5bc0de', '#f0ad4e']}];
    public chartLabels:string[] = [];
    public chartData:any[] = [{data: [], label: 'Ventas'} ];
    public CchartLabels:string[] = [];
    public CchartData:any[] = [{data: [], label: 'Ventas'} ];


	constructor( private alertService:AlertService, private apiService:ApiService
	) { }

	ngOnInit() {

    if (this.apiService.auth_user().tipo == 'Administrador') { 
        this.filtro.fecha_ini = this.apiService.date();
        this.filtro.hora_ini = '00:00';
        this.filtro.hora_fin = '23:59';
        this.filtro.fecha_fin = this.apiService.date();
    }

      this.apiService.store('estadistica', this.filtro).subscribe(datos => { 
          this.chartData[0].data = datos.totales;
          this.chartLabels       = datos.productos;

          this.CchartData[0].data = datos.ctotales;
          this.CchartLabels       = datos.cproductos;
      }, error => {this.alertService.error(error); });
  }

}
