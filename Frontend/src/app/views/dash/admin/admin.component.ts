import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from '@angular/router';
  
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})


export class AdminComponent implements OnInit {

	 public datos:any = {};
    public usuario:any;
    public filtro:any = {};
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService
    ) { }

    ngOnInit() {
        this.usuario = this.apiService.auth_user();
        this.filtro.inicio = this.apiService.date();
        this.filtro.fin = this.apiService.date();
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.store('admin', this.filtro).subscribe(datos => { 
            this.datos = datos;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
