import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-niveles',
  templateUrl: './niveles.component.html'
})
export class NivelesComponent implements OnInit {

    public tanques:any[] = [];
    public loading = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.apiService.getAll('tanques').subscribe(tanques => { 
            this.tanques = tanques;
            for (var i = 0; i < this.tanques.length; ++i) {
                if (this.tanques[i].producto_id == 1) { //Super
                    this.tanques[i].tipo = 'danger';
                }
                if (this.tanques[i].producto_id == 2) { //Regular
                    this.tanques[i].tipo = 'warning';
                }
                if (this.tanques[i].producto_id == 3) { //Diesel
                    this.tanques[i].tipo = 'success';
                }
            }
        }, error => {this.alertService.error(error); });

    }

}
