import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgChartsModule } from 'ng2-charts';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { PipesModule } from '../../pipes/pipes.module';
import { FocusModule } from 'angular2-focus';
import { SharedModule } from '../../shared/shared.module';

import { DashRoutingModule } from './dash.routing.module';

import { DashComponent } from './dash.component';
import { DatosComponent } from './admin/datos/datos.component';
import { EstadisticaComponent } from './admin/estadistica/estadistica.component';
import { DashCajasComponent } from './admin/cajas/dash-cajas.component';
import { CuentasComponent } from './admin/cuentas/cuentas.component';
import { GasolinaComponent } from './admin/gasolina/gasolina.component';
import { NivelesComponent } from './admin/niveles/niveles.component';
import { GalonajeComponent } from './admin/galonaje/galonaje.component';

import { AdminComponent } from './admin/admin.component';
import { CajaComponent } from './caja/caja.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgChartsModule,
    PipesModule,
    SharedModule,
    DashRoutingModule,
    TooltipModule.forRoot(),
    FocusModule.forRoot(),
    ProgressbarModule.forRoot()
  ],
  declarations: [
  	DashComponent,
    DatosComponent,
    EstadisticaComponent,
    DashCajasComponent,
    GasolinaComponent,
    NivelesComponent,
    GalonajeComponent,
    CuentasComponent,
    CajaComponent,
    AdminComponent
  ],
  exports: [
  	DashComponent,
    DatosComponent,
    EstadisticaComponent,
    DashCajasComponent,
    GasolinaComponent,
    NivelesComponent,
    GalonajeComponent,
    CuentasComponent,
    CajaComponent,
    AdminComponent
  ]
})
export class DashModule { }
