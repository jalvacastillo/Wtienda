import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaClienteTiendaComponent } from './venta-cliente-tienda.component';

describe('VentaClienteTiendaComponent', () => {
  let component: VentaClienteTiendaComponent;
  let fixture: ComponentFixture<VentaClienteTiendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaClienteTiendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaClienteTiendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
