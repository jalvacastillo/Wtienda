import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SumPipe }     from '../../../pipes/sum.pipe';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-facturacion-tienda',
  templateUrl: './facturacion-tienda.component.html',
  providers: [ SumPipe ]
})

export class FacturacionTiendaComponent implements OnInit {

    public venta: any= {};
    public detalle: any = {};
    public documentos:any = [];
    public metodospago:any = [];
    public supervisor:any = {};
    public loading = false;
    public imprimir:boolean = true;
    
    modalRef!: BsModalRef;

    @ViewChild('msupervisor')
    public supervisorTemplate!: TemplateRef<any>;

    
	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService, private sumPipe:SumPipe,
        private route: ActivatedRoute, private router: Router,
	) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

	ngOnInit() {

        const id = +this.route.snapshot.paramMap.get('id')!;

        if(isNaN(id)){
            this.cargarDatosIniciales();
        }
        else{
            this.loading = true;
            this.cargarDocumentos();
            this.apiService.read('venta/', id).subscribe(venta => {
                this.venta = venta;
                if(this.venta.cliente.nombre == '')
                    this.venta.cliente.nombre = this.venta.nombre;
                this.sumTotal();
                this.loading = false;
            }, error => {this.alertService.error(error);this.loading = false;});
        }

        this.apiService.getAll('formas-pagos').subscribe(metodospago => {
            this.metodospago = metodospago;
        }, error => {this.alertService.error(error);});

    }

    cargarDocumentos(){
        this.apiService.getAll('documentos').subscribe(documentos => {
            this.documentos = documentos;
        }, error => {this.alertService.error(error);});
    }

    cargarDatosIniciales(){
        this.cargarDocumentos();
        this.venta = {};
        this.venta.fecha = JSON.parse(sessionStorage.getItem('wgas_corte')!).fecha;
        this.venta.tipo = 'Interna';
        this.venta.caja_id = JSON.parse(sessionStorage.getItem('wgas_corte')!).caja_id;
        this.venta.corte_id = JSON.parse(sessionStorage.getItem('wgas_corte')!).id;
        this.venta.cliente = {};
        this.venta.detalles = [];
        this.detalle = {};
        this.sumTotal();
        this.venta.usuario_id = this.apiService.auth_user().id;
        this.imprimir = true;
    }

    public sumTotal() {
        this.venta.iva = this.sumPipe.transform(this.venta.detalles, 'iva');
        this.venta.cotrans = this.sumPipe.transform(this.venta.detalles, 'cotrans');
        this.venta.fovial = this.sumPipe.transform(this.venta.detalles, 'fovial');
        this.venta.subcosto = this.sumPipe.transform(this.venta.detalles, 'subcosto');
        this.venta.subtotal = this.sumPipe.transform(this.venta.detalles, 'subtotal');
        if(this.venta.cliente.tipo == "Grande") {
            this.venta.iva_retenido = this.venta.subtotal * 0.1;
        }else{
            this.venta.iva_retenido = 0;
        }
        this.venta.total = this.sumPipe.transform(this.venta.detalles, 'total') - this.venta.iva_retenido;
    }

	// Seleccionar Cliente
        clienteSelect(event:any):void{
            this.venta.cliente = event.cliente;
            if(this.venta.tipo_documento == 'Devolucion') {
                document.getElementById("referencia")!.focus();
            }else{
                document.getElementById("metodo_pago")!.focus();
            }
        }

    // Agregar detalle
        productoSelect(producto:any):void{
            this.detalle = Object.assign({}, producto);
            this.detalle.id = null;
            
            // Verifica si el producto ya fue ingresado
            let detalle = this.venta.detalles.find((x:any) => x.producto_id == this.detalle.producto_id);
            
            if(detalle) {
                this.detalle = detalle;
                this.detalle.cantidad += 1;
            }

            // Impuestos
            if(this.detalle.tipo_impuesto == "Gravada") {
                this.detalle.iva     = (((this.detalle.precio - this.detalle.descuento) / 1.13) * this.detalle.cantidad) * 0.13;
            }
            
            this.detalle.total = ((this.detalle.precio - this.detalle.descuento) * this.detalle.cantidad);
            this.detalle.subcosto = (this.detalle.costo * this.detalle.cantidad);
            this.detalle.subtotal = (this.detalle.total - this.detalle.iva);
    		
            if(!detalle)
                this.venta.detalles.push(this.detalle);

            // Mantener el scroll hasta abajo en la lista de productos
            setTimeout(function(){
                var objDiv = document.getElementById("detalles")!;
                objDiv.scrollTop = objDiv.scrollHeight;
            },300);

    		this.sumTotal();
    		
    		this.detalle = {};
    		if (this.modalRef) { this.modalRef.hide() }

    	}

    openModalOrden(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }
    
    updateVenta(venta:any) {
        this.venta = venta;
        this.sumTotal();
    }
	

    // Facturar

    	openModalFacturar(template: TemplateRef<any>) {
            this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    		let documento = this.documentos.find((x:any) => x.nombre == 'Ticket');
            this.venta.tipo_documento = documento.nombre;
            this.venta.correlativo = documento.actual;
            this.venta.metodo_pago = 'Efectivo';
            this.venta.estado = 'Cobrada';
        }

        openModalDevolucion(template: TemplateRef<any>) {
            this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
            let documento = this.documentos.find((x:any) => x.nombre == 'Ticket');
            this.venta.tipo_documento = 'Devolucion';
            this.venta.metodo_pago = 'Efectivo';
            this.venta.estado = 'Cobrada';
        }

        public onSelectTipo(event:any){
            if(event == 'Devolucion') {
                this.venta.tipo_documento = 'Devolucion';
                this.venta.correlativo = null;
            }else{
                let documento = this.documentos.find((x:any) => x.nombre == event);
                this.venta.tipo_documento = documento.nombre;
                this.venta.correlativo = documento.actual;
            }
            
            setTimeout(()=>{
                if (this.venta.tipo_documento == 'Ticket'){
                    this.venta.tipo = 'Interna';
                    document.getElementById('metodo_pago')!.focus();
                }
                else if (this.venta.tipo_documento == 'Factura' || this.venta.tipo_documento == 'Credito Fiscal'){
                    this.venta.tipo = 'Interna';
                    document.getElementById('cliente')!.focus();
                }
            },100);
        }

        public onSelectForma(event:any){

            // event = event.substr(2);// Para quitar la letra y espacio inicial que se usa para seleccionar con teclado
            this.venta.metodo_pago = event;            

            if (this.venta.metodo_pago == 'Vale' || this.venta.metodo_pago == 'Credito')
                this.venta.estado = 'Pendiente';
            
            setTimeout(()=>{
                if (this.venta.metodo_pago == 'Efectivo' || this.venta.metodo_pago == 'Credito')
                    document.getElementById('submit')!.focus();
                else
                    document.getElementById('referencia')!.focus();
            },100);

        }

        onFacturar(){
            if (confirm('¿Confirma la venta?')) {
                if(!this.venta.recibido)
                    this.venta.recibido = this.venta.total;
                this.onSubmit();
            }
        }

        onDevolucion(){
            if (confirm('¿Confirma la devolución?')) {
                if(!this.venta.recibido)
                    this.venta.recibido = this.venta.total;
                this.onSubmit();
            }
        }


        onPosponer(){
            if (confirm('¿Confirma que la venta se marque como pendiente?')) {
                this.venta.estado = 'Pendiente';
                this.venta.metodo_pago = 'Efectivo';
                this.venta.tipo_documento = 'Ticket';
                this.onSubmit();
            }
        }	



    // Guardar venta
        public onSubmit() {

            this.loading = true;
            if(!this.venta.cliente.id && !this.venta.cliente.nombre) {
                this.venta.cliente.id = 1;
            }
            
            // Para ventas pendientes
            if(this.venta.id) {
                this.apiService.store('facturacion', this.venta).subscribe(venta => {
                    if(this.venta.estado == "Cobrada") {
                        if(this.venta.tipo_documento == 'Factura' || this.venta.tipo_documento == 'Credito Fiscal' || this.venta.tipo_documento == 'Ticket'){
                            this.imprimirVenta(venta);
                        }
                    }
                    if (this.modalRef) { this.modalRef.hide() }
                    this.loading = false;
                    this.cargarDatosIniciales();
                    this.router.navigate(['/facturacion/nueva']);
                    this.alertService.success("Guardado");
                },error => {this.alertService.error(error); this.loading = false; });

            }else{

                if(this.venta.tipo_documento == 'Devolucion') {
                    this.venta.tipo_documento = 'Ticket';
                    this.apiService.store('devolucion/venta', this.venta).subscribe(venta => {
                        if(this.venta.tipo_documento == 'Devolucion' || this.venta.tipo_documento == 'Credito Fiscal' || this.venta.tipo_documento == 'Ticket'){
                            this.imprimirDevolucion(venta);
                        }
                        if (this.modalRef) { this.modalRef.hide() }
                        this.loading = false;
                        this.cargarDatosIniciales();
                        this.router.navigate(['/facturacion/nueva']);
                        this.alertService.success("Guardado");
                    },error => {this.alertService.error(error); this.loading = false; });
                
                }
                else{

                    this.apiService.store('facturacion', this.venta).subscribe(venta => {
                        if(this.venta.estado == "Cobrada") {
                            if(this.venta.tipo_documento == 'Factura' || this.venta.tipo_documento == 'Credito Fiscal' || this.venta.tipo_documento == 'Ticket'){
                                this.imprimirVenta(venta);
                            }
                        }
                        if (this.modalRef) { this.modalRef.hide() }
                        this.loading = false;
                        this.cargarDatosIniciales();
                        this.router.navigate(['/facturacion/nueva']);
                        this.alertService.success("Guardado");
                    },error => {this.alertService.error(error); this.loading = false; });

                }
            }

        }

    public limpiar(){
        this.modalRef = this.modalService.show(this.supervisorTemplate, {class: 'modal-xs'});

    }

    public imprimirVenta(venta:any){
        setTimeout(()=>{
            window.open(this.apiService.baseUrl + '/api/reporte/facturacion/' + venta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }, 1000);

    }

    public imprimirDevolucion(venta:any){
        setTimeout(()=>{
            window.open(this.apiService.baseUrl + '/api/reporte/devolucion/' + venta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }, 1000);

    }

    public supervisorCheck(){
        this.loading = true;
        this.apiService.store('usuario-validar', this.supervisor).subscribe(supervisor => {
            this.modalRef.hide();
            this.cargarDatosIniciales();
            this.loading = false;
            this.supervisor = {};
        },error => {this.alertService.error(error); this.loading = false; });
    }


}
