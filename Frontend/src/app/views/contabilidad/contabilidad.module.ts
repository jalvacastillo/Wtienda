import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FocusModule } from 'angular2-focus';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

import { ContabilidadRoutingModule } from './contabilidad.routing.module';

import { LibroIvaComponent } from './libro-iva/libro-iva.component';
import { LibroComprasComponent } from './libro-compras/libro-compras.component';
import { GalonajeComponent } from './galonaje/galonaje.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    ContabilidadRoutingModule,
    FocusModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    LibroIvaComponent,
  	LibroComprasComponent,
    GalonajeComponent
  ],
  exports: [
    LibroIvaComponent,
  	LibroComprasComponent,
    GalonajeComponent
  ]
})
export class ContabilidadModule { }
