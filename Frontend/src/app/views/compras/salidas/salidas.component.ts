import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-salidas',
  templateUrl: './salidas.component.html'
})

export class SalidasComponent implements OnInit {

    public salidas:any = [];
	public salida:any = {};
    public buscador:any = '';
    public loading:boolean = false;
    modalRef!: BsModalRef;
    
    constructor(private apiService: ApiService, private alertService: AlertService, private modalService: BsModalService){ }

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('salidas').subscribe(salidas => { 
            this.salidas = salidas;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    openModal(template: TemplateRef<any>, salida:any) {
        this.salida = salida;
        this.modalRef = this.modalService.show(template);
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.apiService.read('salidas/buscar/', this.buscador).subscribe(salidas => { 
                this.salidas = salidas;
            }, error => {this.alertService.error(error); });
        }
    }

    public onSubmit() {
        // Guardamos
        if (!this.salida.id) { 
            this.salida.usuario_id = this.apiService.auth_user().id;
        }
        this.loading = true;
        this.apiService.store('salida', this.salida).subscribe(salida => {
            this.alertService.success("Salida guardada");
            if (!this.salida.id) { 
                this.salidas.data.unshift(salida);
            }
            this.salida = {};
            this.modalRef.hide();
            this.loading = false;
        },error => {
            this.alertService.error(error);
            this.loading = false;
        });
    }


    public setPaginacion(page:number) {
        this.apiService.paginate(this.salidas.path + '?page='+ page).subscribe(salidas => { this.salidas = salidas; });
    }

}
