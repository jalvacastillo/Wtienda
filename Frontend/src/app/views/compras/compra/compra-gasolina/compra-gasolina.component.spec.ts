import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompraGasolinaComponent } from './compra-gasolina.component';

describe('CompraGasolinaComponent', () => {
  let component: CompraGasolinaComponent;
  let fixture: ComponentFixture<CompraGasolinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompraGasolinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompraGasolinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
