import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-compra-gasolina',
  templateUrl: './compra-gasolina.component.html'
})
export class CompraGasolinaComponent implements OnInit {

	@Output() productoSelect = new EventEmitter();
	modalRef!: BsModalRef;

	public detalle: any = {};
    public gasolinas:any = [];

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService
	) { }

	ngOnInit() {
        this.apiService.getAll('productos/gasolina').subscribe(gasolinas => {
            this.gasolinas = gasolinas;
        }, error => {this.alertService.error(error);});
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    setGasolina(producto:any, template: TemplateRef<any>){
        this.detalle = {};

        this.detalle.producto_id = producto.id;
        this.detalle.categoria_id = producto.categoria_id;
        this.detalle.producto = producto.nombre;
        this.detalle.precio = producto.precio;
        this.detalle.medida = producto.medida;
        this.detalle.tanques = producto.tanques;

        if (this.detalle.producto_id == 1 ) { 
            this.detalle.tanque_id = 1;
        }
        if (this.detalle.producto_id == 2 ) { 
            this.detalle.tanque_id = 2;
        }
        if (this.detalle.producto_id == 3 ) { 
            this.detalle.tanque_id = 3;
        }
        
        this.detalle.tipo = 'Gravada';
        this.detalle.descuento = 0;
        // this.detalle.otros = 0;

        this.modalRef = this.modalService.show(template, {class: 'modal-lg'})
    }

    calcular(){

        if(this.detalle.costo) {
            // this.detalle.costo_iva  = (parseFloat(this.detalle.costo) + (this.detalle.costo * 0.13)).toFixed(2);
            // this.detalle.precio_nuevo    = (parseFloat(this.detalle.costo_iva) + (this.detalle.costo_iva * (this.detalle.margen / 100))).toFixed(2);
            if(!this.detalle.subtotal){
                this.detalle.subtotal = ((this.detalle.cantidad * this.detalle.costo) - this.detalle.descuento).toFixed(2);
            }
        }
        
        
        // if(this.detalle.subtotal) {
        this.detalle.iva = 0;
        if(this.detalle.tipo == 'Gravada'){
            this.detalle.iva = (this.detalle.subtotal * 0.13).toFixed(2);
        }
        // }
        this.detalle.fovial = this.detalle.cantidad * 0.20;
        this.detalle.cotrans = this.detalle.cantidad * 0.10;
        
        this.detalle.total = (parseFloat(this.detalle.subtotal) + parseFloat(this.detalle.iva) + parseFloat(this.detalle.fovial) + parseFloat(this.detalle.cotrans)).toFixed(2);
        
        console.log(this.detalle);
    }
    calcularCosto(){
        this.detalle.costo = (this.detalle.subtotal / this.detalle.cantidad).toFixed(4);
        this.calcular();
    }

    agregarDetalle(){
		this.productoSelect.emit({detalle: this.detalle});
		this.modalRef.hide();
	}

}
