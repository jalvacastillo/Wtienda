import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SumPipe }     from '../../../../pipes/sum.pipe';
import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-devolucion-compra',
  templateUrl: './devolucion-compra.component.html',
  providers: [ SumPipe ]
})

export class DevolucionCompraComponent implements OnInit {

	public compra: any= {};
	public detalle: any = {};
	public detalleModificado: any = {};
    public loading = false;
    modalRef!: BsModalRef;
    
	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
	    private modalService: BsModalService, private sumPipe:SumPipe
	) { }

	ngOnInit() {

	    
	    const id = +this.route.snapshot.paramMap.get('id')!;
	        
        if(isNaN(id)){
            this.cargarDatosIniciales();
        }
        else{
            this.loading = true;
            this.cargarDatosIniciales();
            // Optenemos el compra
            this.apiService.read('compra/', id).subscribe(compra => {
               	this.compra = compra;
        		this.loading = false;
            });
        }

	}

	cargarDatosIniciales(){
		this.compra = {};
		this.compra.proveedor = {};
		this.compra.detalles = [];
		this.detalle = {};
		this.compra.fecha = this.apiService.date();
		this.compra.tipo = 'Interna';
		this.sumTotal();
		// this.compra.interno = 0;
		this.compra.usuario_id = this.apiService.auth_user().id;
	}



    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

	public sumTotal() {
        this.compra.subtotal = this.sumPipe.transform(this.compra.detalles, 'subtotal');
        if(this.compra.retencion) { 
            this.compra.iva_retenido = this.compra.subtotal * 0.01;
        }else{
            this.compra.iva_retenido = 0;            
        }
        this.compra.descuento = this.sumPipe.transform(this.compra.detalles, 'descuento');
        this.compra.total = this.sumPipe.transform(this.compra.detalles, 'total') + this.compra.iva_retenido;
        this.compra.iva = this.sumPipe.transform(this.compra.detalles, 'iva');
        this.compra.cotrans = this.sumPipe.transform(this.compra.detalles, 'cotrans');
	    this.compra.fovial = this.sumPipe.transform(this.compra.detalles, 'fovial');
	}

	// Proveedor
	proveedorSelect(event:any):void{
        this.compra.proveedor = event.proveedor;
        if(this.compra.proveedor.tipo == "Grande") {
        	this.compra.retencion = 1;
        	this.sumTotal();
        }
    }

    // Producto o Gasolina
    selectProducto(producto:any){
        this.detalle.producto_id = producto.id;
        this.detalle.precio = producto.precio;
        this.detalle.costo = producto.costo;
        this.detalle.medida = producto.medida;
        this.detalle.producto_nombre = producto.nombre;
        this.detalle.medida = producto.medida;
        this.detalle.tipo_impuesto = producto.tipo_impuesto;  
        this.detalle.bodega_id = 1;  
        
        document.getElementById('cantidad')!.focus();
    }

    public agregarDetalle(){
        // Impuestos
        if(this.detalle.tipo_impuesto == "Gravada") {
            this.detalle.iva     = ((this.detalle.costo / 1.13) * this.detalle.cantidad) * 0.13;
        }
        this.detalle.cotrans = 0;
        this.detalle.fovial = 0;
        
        this.detalle.total = (this.detalle.costo * this.detalle.cantidad);
        this.detalle.subtotal = (this.detalle.total - this.detalle.iva);

        console.log(this.detalle);
        this.compra.detalles.push(this.detalle);
        this.detalle = {};
        
        this.sumTotal();
        this.modalRef.hide();
    }

	public onSubmit() {

		this.loading = true;
		
	    this.apiService.store('devolucion/compra', this.compra).subscribe(compra => {
	        if (this.modalRef) { this.modalRef.hide() }
	        this.loading = false;
	        this.cargarDatosIniciales()
	        this.alertService.success("Guardado");
	    },error => {this.alertService.error(error); this.loading = false; });


	}

	public eliminarDetalle(detalle:any){
		if (confirm('¿Desea eliminar el Registro?')) {
			if(detalle.id) {
				this.apiService.delete('compra/detalle/', detalle.id).subscribe(detalle => {
					for (var i = 0; i < this.compra.detalles.length; ++i) {
						if (this.compra.detalles[i].id === detalle.id ){
							this.compra.detalles.splice(i, 1);
						}
					}
	        	}, error => {this.alertService.error(error._body); });
			}else{
				for (var i = 0; i < this.compra.detalles.length; ++i) {
					if (this.compra.detalles[i].producto_id === detalle.producto_id ){
						this.compra.detalles.splice(i, 1);
					}
				}
			}
			this.sumTotal();
		}
	}

	//Para Editar detalle

	public editarDetalle(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.detalleModificado.tipo = this.detalle.tipo;
        this.detalleModificado.precio = this.detalle.precio;
        this.detalleModificado.precio_nuevo = this.detalle.precio_nuevo;
		this.modalRef = this.modalService.show(template, {class: 'modal-lg', backdrop: 'static'});
    }


}
