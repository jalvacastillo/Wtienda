import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-devoluciones-compras',
  templateUrl: './devoluciones-compras.component.html'
})

export class DevolucionesComprasComponent implements OnInit {

	public compras:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    constructor(private apiService: ApiService, private alertService: AlertService){ }

	ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('devoluciones-compras').subscribe(compras => { 
            this.compras = compras;
            this.loading = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
    	if(this.buscador && this.buscador.length > 2) {
	    	this.apiService.read('devoluciones-compras/buscar/', this.buscador).subscribe(compras => { 
	    	    this.compras = compras;
	    	}, error => {this.alertService.error(error); });
    	}
    }

    public setEstado(compra:any, estado:string){
        compra.estado = estado;
        this.apiService.store('devolucion-compra', compra).subscribe(compra => { 
            this.alertService.success('Actualizado');
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('devolucion-compra/', id) .subscribe(data => {
                for (let i = 0; i < this.compras['data'].length; i++) { 
                    if (this.compras['data'][i].id == data.id )
                        this.compras['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public filtrar(filtro:any, txt:any){
        this.loading = true;
        this.apiService.read('devoluciones-compras/' + filtro + '/', txt).subscribe(compras => { 
            this.compras = compras;
            this.loading = false;
        }, error => {this.alertService.error(error); });

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.compras.path + '?page='+ event.page).subscribe(compras => { 
            this.compras = compras;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

}
