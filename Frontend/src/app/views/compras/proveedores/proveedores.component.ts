import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
})
export class ProveedoresComponent implements OnInit {

    public proveedores:any = [];
    public buscador:any = '';
    public loading:boolean = false;

    public filtro:any = {};
    public filtrado:boolean = false;
    modalRef?: BsModalRef;
    
    constructor(public apiService: ApiService, private alertService: AlertService,
                private modalService: BsModalService
    ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('proveedores').subscribe(proveedores => { 
            this.proveedores = proveedores;
            this.loading = false; this.filtrado = false;
        }, error => {this.alertService.error(error); });
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.loading = true;
            this.apiService.read('proveedores/buscar/', this.buscador).subscribe(proveedores => { 
                this.proveedores = proveedores;
                this.loading = false; this.filtrado = true;
            }, error => {this.alertService.error(error); this.loading = false;});
        }
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('proveedor/', id) .subscribe(data => {
                for (let i = 0; i < this.proveedores['data'].length; i++) { 
                    if (this.proveedores['data'][i].id == data.id )
                        this.proveedores['data'].splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.proveedores.path + '?page='+ event.page).subscribe(proveedores => { 
            this.proveedores = proveedores;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    // Filtros
    openFilter(template: TemplateRef<any>) {
        if(!this.filtro.fecha_ini) {
            this.filtro.fecha_ini = this.apiService.date();
            this.filtro.hora_ini = '00:00';
            this.filtro.hora_fin = '23:59';
            this.filtro.fecha_fin = this.apiService.date();
            this.filtro.estado = '';
            this.filtro.forma_de_pago = '';
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('proveedores/filtrar', this.filtro).subscribe(proveedores => { 
            this.proveedores = proveedores;
            this.loading = false; this.filtrado = true;
            this.modalRef!.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

}
