import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html'
})
export class ProveedorComponent implements OnInit {

	public proveedor: any = {};
    public loading = false;
    public filtro:any = {};

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    
	    this.loadAll();

	}

	public loadAll(){
	    const id = +this.route.snapshot.paramMap.get('id')!;
        this.filtro.estado = "";
        if(this.route.snapshot.paramMap.get('estado'))
            this.filtro.estado = this.route.snapshot.paramMap.get('estado');
	        
        if(isNaN(id)){
            this.proveedor = {};
        }
        else{
            this.loading = true;
            this.apiService.read('proveedor/', id).subscribe(proveedor => {
                this.proveedor = proveedor;
            	this.loading = false;
            },error => {this.alertService.error(error); this.loading = false; });
        }
	}

	public onSubmit() {
	    this.loading = true;
	    // Guardamos al proveedor
	    this.apiService.store('proveedor', this.proveedor).subscribe(proveedor => {
            this.proveedor = proveedor;
	        this.loading = false;
	        this.router.navigate(['/proveedor/'+ proveedor.id]);
	    },error => {this.alertService.error(error); this.loading = false; });
	}

}
