import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { LayoutComponent } from '../../layout/layout.component';


// Compras
    import { ComprasComponent } from './compras.component';
    import { CompraComponent } from './compra/compra.component';
    import { SalidasComponent } from './salidas/salidas.component';
    import { DevolucionesComprasComponent } from './devoluciones/devoluciones-compras.component';
    import { DevolucionCompraComponent } from './devoluciones/devolucion/devolucion-compra.component';
    import { RequisicionesComprasComponent } from './requisiciones/requisiciones-compras.component';

    import { HistorialComprasComponent } from './reportes/historial/historial-compras.component';
    import { DetalleComprasComponent } from './reportes/detalle/detalle-compras.component';
    import { CategoriasComprasComponent } from './reportes/categorias/categorias-compras.component';

// Proveedores

    import { ProveedoresComponent } from './proveedores/proveedores.component';
    import { ProveedorComponent } from './proveedores/proveedor/proveedor.component';
    import { CuentasPagarComponent } from './proveedores/cuentas-pagar/cuentas-pagar.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    title: 'Compras',
    children: [
        // Compras
            { path: 'compras', component: ComprasComponent},
            { path: 'compra/:id', component: CompraComponent},
            { path: 'salidas', component: SalidasComponent},
            { path: 'devoluciones/compras', component: DevolucionesComprasComponent},
            { path: 'devolucion/compra/:id', component: DevolucionCompraComponent},
            { path: 'requisicion/compras', component: RequisicionesComprasComponent},

            { path: 'reporte/historial/compras', component: HistorialComprasComponent},
            { path: 'reporte/detalle/compras', component: DetalleComprasComponent},
            { path: 'reporte/categorias/compras', component: CategoriasComprasComponent},


         // Proveedores
             
             { path: 'proveedores', component: ProveedoresComponent},
             { path: 'proveedor/:id', component: ProveedorComponent},
             { path: 'proveedor/:id/:estado', component: ProveedorComponent},
             { path: 'cuentas-pagar', component: CuentasPagarComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComprasRoutingModule { }
