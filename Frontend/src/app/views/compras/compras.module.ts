import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { FocusModule } from 'angular2-focus';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask'

import { ComprasRoutingModule } from './compras.routing.module';

import { ComprasComponent } from './compras.component';
import { CompraComponent } from './compra/compra.component';
import { CompraProveedorComponent } from './compra/compra-proveedor/compra-proveedor.component';
import { CompraProductoComponent } from './compra/compra-producto/compra-producto.component';
import { CompraGasolinaComponent } from './compra/compra-gasolina/compra-gasolina.component';

import { SalidasComponent } from './salidas/salidas.component';

import { DevolucionesComprasComponent } from './devoluciones/devoluciones-compras.component';
import { DevolucionCompraComponent } from './devoluciones/devolucion/devolucion-compra.component';
import { RequisicionesComprasComponent } from './requisiciones/requisiciones-compras.component';

import { HistorialComprasComponent } from './reportes/historial/historial-compras.component';
import { DetalleComprasComponent } from './reportes/detalle/detalle-compras.component';
import { CategoriasComprasComponent } from './reportes/categorias/categorias-compras.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    NgxMaskDirective,
    ComprasRoutingModule,
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
  	ComprasComponent,
    CompraComponent,
    CompraProveedorComponent,
    CompraProductoComponent,
    CompraGasolinaComponent,
    SalidasComponent,
    DevolucionesComprasComponent,
    DevolucionCompraComponent,
    RequisicionesComprasComponent,
    HistorialComprasComponent,
    DetalleComprasComponent,
    CategoriasComprasComponent
  ],
  exports: [
  	ComprasComponent,
    CompraComponent,
    CompraProveedorComponent,
    CompraProductoComponent,
    CompraGasolinaComponent,
    SalidasComponent,
    DevolucionesComprasComponent,
    DevolucionCompraComponent,
    RequisicionesComprasComponent,
    HistorialComprasComponent,
    DetalleComprasComponent,
    CategoriasComprasComponent
  ]
})
export class ComprasModule { }
