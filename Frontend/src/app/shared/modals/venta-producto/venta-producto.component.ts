import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../services/api.service';
import { AlertService } from '../../../services/alert.service';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

@Component({
  selector: 'app-venta-producto',
  templateUrl: './venta-producto.component.html'
})
export class VentaProductoComponent implements OnInit {

	@Output() productoSelect = new EventEmitter();
	modalRef!: BsModalRef;
    public searching = false;
    public productos: any = [];
	public producto: any = {};

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService
	) { }

	ngOnInit() {
        
	}

	openModal(template: TemplateRef<any>) {
        this.producto = {};

        if(this.producto.id) {
            this.searching = false;
        }
        this.modalRef = this.modalService.show(template);

        const input = document.getElementById('example')!;
        const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
        const debouncedInput = example.pipe(debounceTime(500));
        const subscribe = debouncedInput.subscribe(val => { this.searchProducto(); });
    }

    searchProducto(){
        if(this.producto.nombre && this.producto.nombre.length > 1) {
            this.searching = true;
            this.apiService.read('productos/buscar/', this.producto.nombre).subscribe(productos => {
               this.productos = productos;
               this.searching = false;
            }, error => {this.alertService.error(error);this.searching = false;});
        }else if (!this.producto.nombre  || this.producto.nombre.length < 1 ){ this.searching = false; this.producto = {}; this.productos.total = 0; }
    }

    public selectProducto(item:any){
        this.productos = [];
        this.producto = Object.assign({}, item);;
        document.getElementById('cantidad')!.focus();
        this.producto.cantidad = 1;
        // Descuento promoción si esta en fecha
        if (this.producto.promocion) {
            this.producto.descuento = this.producto.precio - this.producto.promocion.precio;
        }else{
            this.producto.descuento = 0;
        }
    }

    agregarDetalle(){
    	this.producto.producto_id = this.producto.id;
        this.producto.producto_nombre = this.producto.nombre;
        this.producto.escombo = false;

        // Impuestos
        this.producto.iva = 0;
        if(this.producto.tipo_impuesto == 'Gravada'){
            this.producto.iva = (((this.producto.precio - this.producto.descuento) / 1.13) * this.producto.cantidad * 0.13);
        }
        this.producto.fovial = 0;
        this.producto.cotrans = 0;

		this.productoSelect.emit({producto: this.producto});
        this.producto = {};
		this.modalRef.hide();
	}

}
