import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

import { ApiService } from '../../../services/api.service';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'app-venta-cliente',
  templateUrl: './venta-cliente.component.html'
})
export class VentaClienteComponent implements OnInit {

	@Input() cliente: any = {};
	@Output() clienteSelect = new EventEmitter();
    public clientes: any = [];
    public searching = false;
	modalRef!: BsModalRef;

	public pagoAnticipado:boolean = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService
	) { }

    ngOnInit() {
    }

	openModal(template: TemplateRef<any>) {
        if(this.cliente.id) {
            this.searching = false;
        }
        this.modalRef = this.modalService.show(template);

        const input = document.getElementById('example')!;
        const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
        const debouncedInput = example.pipe(debounceTime(500));
        const subscribe = debouncedInput.subscribe(val => { this.searchCliente(); });
        
    }


    searchCliente(){
        if(this.cliente.registro && this.cliente.registro.length > 1) {
            this.searching = true;
            this.apiService.read('clientes/buscar/', this.cliente.registro).subscribe(clientes => {
               this.clientes = clientes;
               if(clientes.total) {
                   if(clientes.total == 1 && ((clientes.data[0].registro.replace('-', '')) == this.cliente.registro)) {
                       this.selectCliente(clientes.data[0]);
                   }
               }
               this.searching = false;
            }, error => {this.alertService.error(error);this.searching = false;});
        }else if (!this.cliente.registro  || this.cliente.registro.length < 1 ){ this.searching = false; this.cliente = {}; this.clientes.total = 0; }
    }

	public selectCliente(cliente:any){
        this.clientes = [];
        this.cliente = cliente;
        if(this.cliente.pago_anticipado > 0)
            this.cliente.pagoAnticipado = true;
        else
    	    this.cliente.pagoAnticipado = false;
	    this.clienteSelect.emit({cliente: this.cliente});
	    this.modalRef.hide()
	}

    clear(){
        if(this.clientes.data && this.clientes.data.length == 0) { this.clientes = []; }
    }

}
