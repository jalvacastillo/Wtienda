import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FocusModule } from 'angular2-focus';
import { PipesModule } from '../pipes/pipes.module';
import { NgxMaskDirective, NgxMaskPipe } from 'ngx-mask'

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { PaginationComponent } from './parts/pagination/pagination.component';
import { NotFoundComponent } from './404/not-found.component';

import { BuscadorProductosComponent } from './parts/buscador-productos/buscador-productos.component';
import { BuscadorClientesComponent } from './parts/buscador-clientes/buscador-clientes.component';
import { BuscadorProveedoresComponent } from './parts/buscador-proveedores/buscador-proveedores.component';
import { BuscadorMateriasPrimasComponent } from './parts/buscador-materias-primas/buscador-materias-primas.component';

import { DescargarExcelComponent } from './parts/descargar-excel/descargar-excel.component';
import { ImportarExcelComponent } from './parts/importar-excel/importar-excel.component';

import { VentaClienteComponent } from './modals/venta-cliente/venta-cliente.component';
import { VentaProductoComponent } from './modals/venta-producto/venta-producto.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    NgxMaskDirective,
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
    PaginationComponent,
    NotFoundComponent,
    DescargarExcelComponent,
    ImportarExcelComponent,
    VentaClienteComponent,
    VentaProductoComponent,
    BuscadorProductosComponent,
    BuscadorClientesComponent,
    BuscadorProveedoresComponent,
    BuscadorMateriasPrimasComponent

  ],
  exports: [
    PaginationComponent,
    NotFoundComponent,
    DescargarExcelComponent,
    ImportarExcelComponent,
    VentaClienteComponent,
    VentaProductoComponent,
    BuscadorProductosComponent,
    BuscadorClientesComponent,
    BuscadorProveedoresComponent,
    BuscadorMateriasPrimasComponent

  ]
})
export class SharedModule { }
