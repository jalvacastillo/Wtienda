import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})

export class SidebarComponent implements OnInit {

    public usuario: any = {};
    public admin:boolean = true;
    public web:boolean = true;
    public blog:boolean = true;

    constructor(private apiService: ApiService) { }

    ngOnInit() {
        this.usuario = this.apiService.auth_user();

    }

}
