import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FocusModule } from 'angular2-focus';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { HeaderComponent } from './header/header.component';
import { AdminHeaderComponent } from './header/admin/admin-header.component';
import { CajaHeaderComponent } from './header/caja/caja-header.component';
import { VendedorHeaderComponent } from './header/vendedor/vendedor-header.component';

import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout.component';

import { PerfilComponent } from './header/perfil/perfil.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    TooltipModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    AdminHeaderComponent,
    VendedorHeaderComponent,
    CajaHeaderComponent,
    SidebarComponent,
    LayoutComponent,
    PerfilComponent,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    AdminHeaderComponent,
    VendedorHeaderComponent,
    CajaHeaderComponent,
    SidebarComponent,
    LayoutComponent,
    PerfilComponent,
  ]
})
export class LayoutModule { }
