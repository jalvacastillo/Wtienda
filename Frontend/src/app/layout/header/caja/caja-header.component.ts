import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-caja-header',
  templateUrl: './caja-header.component.html'
})
export class CajaHeaderComponent implements OnInit {

    public usuario: any = {};

    constructor(private apiService: ApiService) { }

    ngOnInit() {
        this.usuario = this.apiService.auth_user();
    }

}
