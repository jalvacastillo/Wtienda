import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

    public usuario: any = {};
    public mensajes: any[] = [];
    public mensaje: any = {};

    constructor(private apiService: ApiService, private alertService:AlertService) { }

    ngOnInit() {
        this.usuario = this.apiService.auth_user();
        this.loadAll();
    }

    public loadAll(){
        this.apiService.getAll('mensajes/filtrar/estado/pendiente').subscribe(mensajes => { 
            this.mensajes = mensajes;
        }, error => {this.alertService.error(error); });
    }

    mensajeUpdate(mensaje:any){
        // if(confirm("Desea marcarlo como hecho?")){
            mensaje.estado = 'Finalizado';
            mensaje.usuario_id = this.apiService.auth_user().id;
            this.apiService.store('mensaje', mensaje).subscribe(mensaje => { 
                this.loadAll();
            }, error => {this.alertService.error(error); });
        // }
    }

    submit(){
        this.mensaje.estado = 'Pendiente';
        this.mensaje.usuario_id = this.apiService.auth_user().id;
        this.apiService.store('mensaje', this.mensaje).subscribe(mensaje => {
            this.mensaje = {};
            this.loadAll();
        }, error => {this.alertService.error(error); });
    }

}
