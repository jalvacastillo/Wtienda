import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html'
})
export class AdminHeaderComponent {

  public usuario: any = {};

    constructor(public apiService: ApiService) { }

    ngOnInit() {
        this.usuario = this.apiService.auth_user();
    }

}
