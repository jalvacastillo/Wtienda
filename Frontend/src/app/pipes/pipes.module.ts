import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SumPipe } from './sum.pipe';
import { TruncatePipe } from './truncate.pipe';
import { FilterPipe } from './filter.pipe';
import { AvgPipe } from './avg.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  	SumPipe,
    TruncatePipe,
    FilterPipe,
    AvgPipe
  ],
  exports: [
  	SumPipe,
    TruncatePipe,
    FilterPipe,
    AvgPipe
  ]
})
export class PipesModule { }
