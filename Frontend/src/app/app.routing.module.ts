import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { PerfilComponent } from './views/admin/perfil/perfil.component';

import { DashComponent } from './views/dash/dash.component';
import { NotFoundComponent }    from './shared/404/not-found.component';

import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [
    
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: RegisterComponent },
    
    // Dash
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/dash/dash.module').then(m => m.DashModule),
    },
    // Ventas
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/ventas/ventas.module').then(m => m.VentasModule),
    },
    // Compras
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/compras/compras.module').then(m => m.ComprasModule),
    },
    // Inventario
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/inventario/inventario.module').then(m => m.InventarioModule),
    },
    // Contabilidad
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/contabilidad/contabilidad.module').then(m => m.ContabilidadModule),
    },
    // Admin
    {
      path: '', canActivate: [AuthGuard],
      loadChildren: () => import('./views/admin/admin.module').then(m => m.AdminModule),
    },

    // Not Found
    {
      path: '**',
      component: NotFoundComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      // { enableTracing: true }
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
