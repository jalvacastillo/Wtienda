<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaDocumentosTable extends Migration {

    public function up()
    {
        Schema::create('caja_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->integer('actual');
            $table->integer('inicial');
            $table->integer('final');
            $table->string('rangos')->nullable();
            $table->string('numero_autorizacion')->nullable();
            $table->string('resolucion')->nullable();
            $table->date('fecha')->nullable();
            $table->text('nota')->nullable();
            $table->integer('caja_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('caja_documentos');
    }

}
