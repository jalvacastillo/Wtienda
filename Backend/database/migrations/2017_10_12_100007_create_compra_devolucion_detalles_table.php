<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraDevolucionDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('compra_devolucion_detalles', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('producto_id');
			$table->decimal('cantidad', 9,2);
			$table->decimal('costo', 9,2);
			$table->string('tipo')->default('Gravada');
			$table->decimal('iva', 9,2)->default(0);
			$table->decimal('fovial', 9,2)->default(0);
			$table->decimal('cotrans', 9,2)->default(0);
			$table->decimal('subtotal', 9,2)->default(0);
			$table->decimal('total', 9,2)->default(0);
			$table->integer('compra_id');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('compra_devolucion_detalles');
	}

}
