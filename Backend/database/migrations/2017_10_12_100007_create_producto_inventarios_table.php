<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoInventariosTable extends Migration {

	public function up()
	{
		Schema::create('producto_inventarios', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('producto_id');
			$table->string('bodega_id');
			$table->string('ubicacion')->nullable();
			$table->decimal('stock', 9, 2);
			$table->decimal('stock_min', 9, 2);
			$table->decimal('stock_max', 9, 2);

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_inventarios');
	}

}
