    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration {

    public function up()
    {
        Schema::create('empresa', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('nit')->nullable();
            $table->string('ncr')->nullable();
            $table->string('giro')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('municipio')->nullable();
            $table->string('departamento')->nullable();
            $table->string('logo')->default('default.png');
            $table->integer('meta_galones')->default(0);
            $table->string('valor_inventario')->default('Promedio');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }

}
