<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoMetasTable extends Migration
{

    public function up()
    {
        Schema::create('empleado_metas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('ano');
            $table->string('mes');
            $table->decimal('meta', 8,2);
            $table->integer('usuario_id');
            
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleado_metas');
    }
}
