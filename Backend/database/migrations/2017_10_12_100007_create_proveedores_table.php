<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration {

	public function up()
	{
		Schema::create('proveedores', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre',100);
			$table->string('dui',50)->nullable();
			$table->string('nit',50)->nullable();
			$table->string('registro',50)->nullable()->unique();
			$table->string('giro')->nullable();
			$table->string('descripcion',50)->nullable();
			$table->string('direccion',100)->nullable();
			$table->string('municipio',50)->nullable();
			$table->string('departamento',50)->nullable();
			$table->string('telefono',50)->nullable();
			$table->string('correo')->nullable();
			$table->string('etiquetas')->nullable();
			$table->string('nota')->nullable();
			$table->string('tipo_contribuyente')->default('Pequeño');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('proveedores');
	}

}
