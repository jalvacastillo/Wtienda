<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoComboDetallesTable extends Migration {

	public function up()
	{
		Schema::create('producto_combo_detalles', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('producto_id');
			$table->integer('cantidad');
			$table->decimal('costo', 8,2);
			$table->decimal('precio', 8,2);
			$table->integer('combo_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_combo_detalles');
	}

}
