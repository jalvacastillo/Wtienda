<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoCategoriasTable extends Migration {

	public function up()
	{
		Schema::create('producto_categorias', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre', 50);
			$table->text('descripcion')->nullable();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_categorias');
	}

}
