<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoTrasladoDetallesTable extends Migration {

	public function up()
	{
		Schema::create('producto_traslado_detalles', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('producto_id');
			$table->integer('cantidad');
			$table->integer('traslado_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_traslado_detalles');
	}

}
