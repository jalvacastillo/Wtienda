<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasDevolucionesTable extends Migration {

	public function up()
	{
		Schema::create('compras_devoluciones', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('estado');
			$table->text('nota')->nullable();
			$table->text('recibio')->nullable();
			$table->string('referencia')->nullable();
			$table->integer('proveedor_id')->nullable();
			$table->decimal('iva', 9,2);
			$table->decimal('fovial', 9,2);
			$table->decimal('cotrans', 9,2);
			$table->decimal('subtotal', 9,2);
			$table->decimal('total', 9,2);
			$table->integer('usuario_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('compras_devoluciones');
	}

}
