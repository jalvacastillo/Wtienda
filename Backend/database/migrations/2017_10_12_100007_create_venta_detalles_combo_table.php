<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentaDetallesComboTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venta_detalles_combo', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('producto_id');
            $table->decimal('cantidad', 9,2);
            $table->decimal('precio', 9,2);
            $table->decimal('costo', 9,2);
            $table->integer('detalle_id');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venta_detalles_combo');
    }

}
