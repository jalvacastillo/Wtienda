<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration {

	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->text('descripcion')->nullable();
			$table->string('codigo')->nullable();
			$table->string('medida')->default('Unidad');
			$table->decimal('precio', 9,2)->default(0);
			$table->decimal('costo', 9,2)->default(0);
			$table->decimal('costo_anterior', 9,2)->default(0);
			$table->integer('categoria_id')->nullable();
			$table->string('tipo')->default('Producto');
			$table->string('tipo_impuesto')->default('Gravada');
			$table->decimal('impuesto')->default(13);
			$table->boolean('inventario')->default(0);
			$table->boolean('compuesto')->default(0);
			$table->integer('bodega_venta_id')->default(2);
			$table->boolean('activo')->default(1);

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('productos');
	}

}
