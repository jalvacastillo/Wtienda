<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoTrasladosTable extends Migration {

	public function up()
	{
		Schema::create('producto_traslados', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->text('nota')->nullable();
			$table->string('estado');
			$table->integer('origen_id');
			$table->integer('destino_id');
			$table->integer('usuario_id');
			$table->integer('autorizo_id')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_traslados');
	}

}
