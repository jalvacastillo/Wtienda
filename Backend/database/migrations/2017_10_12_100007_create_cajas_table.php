<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajasTable extends Migration {

	public function up()
	{
		Schema::create('cajas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->string('tipo');
			$table->text('descripcion')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cajas');
	}

}
