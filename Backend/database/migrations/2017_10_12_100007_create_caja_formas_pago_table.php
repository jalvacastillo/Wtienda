<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaFormasPagoTable extends Migration {

    public function up()
    {
        Schema::create('caja_formas_pago', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->integer('caja_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('caja_formas_pago');
    }

}
