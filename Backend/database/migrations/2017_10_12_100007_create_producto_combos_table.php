<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoCombosTable extends Migration {

	public function up()
	{
		Schema::create('producto_combos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre')->nullable();
			$table->datetime('inicio')->nullable();
			$table->datetime('fin')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_combos');
	}

}
