<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanquesTable extends Migration {

	public function up()
	{
		Schema::create('tanques', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->integer('producto_id');
			$table->decimal('stock', 9, 2);
			$table->decimal('stock_min', 9, 2);
			$table->decimal('stock_max', 9, 2);

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('tanques');
	}

}
