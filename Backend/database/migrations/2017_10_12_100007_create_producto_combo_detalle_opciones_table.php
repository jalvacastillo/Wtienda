<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoComboDetalleOpcionesTable extends Migration {

	public function up()
	{
		Schema::create('producto_combo_detalle_opciones', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('combo_detalle_id');
			$table->integer('producto_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('producto_combo_detalle_opciones');
	}

}
