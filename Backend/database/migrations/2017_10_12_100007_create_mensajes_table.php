<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTable extends Migration {

    public function up()
    {
        Schema::create('mensajes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->text('descripcion');
            $table->string('estado');
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('mensajes');
    }

}
