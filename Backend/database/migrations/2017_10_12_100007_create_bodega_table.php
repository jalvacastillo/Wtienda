<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodegaTable extends Migration {

	public function up()
	{
		Schema::create('bodegas', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('nombre');
			$table->string('descripcion')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('bodegas');
	}

}
