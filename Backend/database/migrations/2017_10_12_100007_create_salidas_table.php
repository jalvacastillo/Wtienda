<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalidasTable extends Migration {

	public function up()
	{
		Schema::create('salidas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('descripcion');
			$table->decimal('total', 9, 2);
			$table->integer('usuario_id');
			$table->timestamps();

		});
	}

	public function down()
	{
		Schema::drop('salidas');
	}

}
