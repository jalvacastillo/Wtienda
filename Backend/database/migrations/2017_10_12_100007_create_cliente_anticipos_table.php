<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteAnticiposTable extends Migration {

	public function up()
	{
		Schema::create('cliente_anticipos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('tipo');
			$table->decimal('total', 9,2);
			$table->string('referencia')->nullable();
			$table->integer('cliente_id');
			$table->integer('usuario_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cliente_anticipos');
	}

}
