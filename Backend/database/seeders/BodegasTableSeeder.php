<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Bodega;
     
class BodegasTableSeeder extends Seeder {
     
    public function run()
    {

        Bodega::create([ 
            'nombre' => 'Bodega',
            'descripcion' => 'Para productos que estan en bodega.'
        ]);

        Bodega::create([ 
            'nombre' => 'Sala de Venta',
            'descripcion' => 'Para productos que estan a la vista del cliente.'
        ]);
        

    }
     
}