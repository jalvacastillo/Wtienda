<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmpresaTableSeeder::class,
            UsersTableSeeder::class,
            CajasTableSeeder::class,
            BodegasTableSeeder::class,
            // CategoriasTableSeeder::class,
            // ClientesTableSeeder::class,
            // ProveedoresTableSeeder::class,

            // TanquesTableSeeder::class,

            // CombosTableSeeder::class,
            // CombosDetalleTableSeeder::class,
            
            // ProductosTableSeeder::class,
            
            // PagosAdelantadosTableSeeder::class,
            // VentasTableSeeder::class,
            // ComprasTableSeeder::class,
        ]);
    }
}
        