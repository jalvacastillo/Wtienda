<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registros\Anticipo;
     
class PagosAdelantadosTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 1; $i <= 150 ; $i++)
        {
            $table = new Anticipo;

            $table->referencia = $faker->numberBetween(1,60);
            $table->total = $faker->numberBetween(10,60);
            $table->tipo = $faker->numberBetween(1,2);
            $table->cliente_id = $faker->numberBetween(1,200);
            $table->usuario_id = $faker->numberBetween(1,2);
            $table->created_at = $faker->dateTimeBetween('-5 years', 'now', $timezone = null);

            $table->save();
            
        }
    }
     
}