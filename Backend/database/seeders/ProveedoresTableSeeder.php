<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registros\Proveedor;
     
class ProveedoresTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        Proveedor::create([ 'nombre' => 'DELIPAN', 'telefono' => '2228-4171', 'direccion' => '9A. C. OTE. NO 10-A, COL STA. MONICA. SANTA TECLA', 'nit' => '9384-260148-001-1', 'registro' =>'35598-4']);
        Proveedor::create([ 'nombre' => 'DINANT DE EL SALVADOR, S.A DE C.V.', 'telefono' => '2510-8300', 'direccion' => 'BOULEVARD DEL EJERCITO NACIONAL ENTRADA ZONA FRANCA, ILOPANGO, SAN SALVADOR', 'nit' => '0614-010704-103-4', 'registro' =>'157769-7']);
        Proveedor::create([ 'nombre' => 'DISTRIBUIDORA SALVADOREÑA, S.A DE C.V.', 'telefono' => '2319-2710', 'direccion' => 'URBANIZACION INDUSTRIAL, HACIENDA NUEVA KM 27, CARRETERA SONSONATE', 'nit' => '0614-250107-104-9', 'registro' =>'177411-0']);
        Proveedor::create([ 'nombre' => 'EME. S.A. DE C.V.', 'telefono' => '2248-7500', 'direccion' => 'PLAN DE LA LAGUNA, C. CIRCUNVALACION, POLIG. "A" #17 ANT. CUSCATLAN.', 'nit' => '0614-240381-001-2', 'registro' =>'431-6']);
        Proveedor::create([ 'nombre' => 'ESCARRSA, DE C.V.', 'telefono' => '2660-1289', 'direccion' => 'FINAL AV. PERALTA Y 38 AV. NORTE, BO. LOURDES, SAN SALVADOR', 'nit' => '0614-300801-101-7', 'registro' =>'133843-2']);
        Proveedor::create([ 'nombre' => 'HELADOS SARITA S.A DE C.V', 'telefono' => '2293-8000', 'direccion' => 'FINAL AV. PERALTA CONTIGUO A FABRICA EL DORADO, SAN SALVADOR', 'nit' => '9483-240499-101-2', 'registro' =>'112334-3']);
        Proveedor::create([ 'nombre' => 'INALTA S.A DE C.V', 'telefono' => '2222-6483', 'direccion' => 'CALLE GERARDO BARRIOS, ENTRE 19 Y 21 AV. SUR, S.S.', 'nit' => '0614-300687-001-5', 'registro' =>'8829-3']);
        Proveedor::create([ 'nombre' => 'INDUSTRIAS LA CONSTANCIA S.A DE C.V', 'telefono' => '800-8080', 'direccion' => 'AV. INDEPENDENCIA NO. 525, SAN SALVADOR, EL SALVADOR', 'nit' => '0614-251002-101-1', 'registro' =>'145663-4']);
        Proveedor::create([ 'nombre' => 'PRODUCTOS CARNICOS, SA. DE C.V.', 'telefono' => '2319-6555', 'direccion' => 'CARRETERA CA. 1 KM 19.7 FINCA LA ESTACION, CENTRO LOGISTICO NEJAPA', 'nit' => '0614-121083-001-4', 'registro' =>'222-4']);
        Proveedor::create([ 'nombre' => 'RAYOVAC EL SALVADOR, S.A . DE C.V.', 'telefono' => '2278-9466', 'direccion' => 'EDIFICIO RAYOVAC, BLVD, MERLIOT, CIUDAD MERLIOT, SANTA TECLA', 'nit' => '0614-110371-001-3', 'registro' =>'197-0']);
        Proveedor::create([ 'nombre' => 'STEINER S.A. DE C.V.', 'telefono' => '2536-7600', 'direccion' => 'CARRETERA PANAMERICANA, SANTA TECLA , LIBERTAD.', 'nit' => '0614-040967-001-9', 'registro' =>'84-1']);
        Proveedor::create([ 'nombre' => 'TELEFONICA MOVILES EL SALVADOR, S. A DE C.V.', 'telefono' => '2244-0144', 'direccion' => '63 AV. SUR Y ALAMEDA ROOSEVELT, CENTRO FINANCIERO GIGANTE, TORRE B', 'nit' => '0614-210297-103-6', 'registro' =>'97894-8']);
        Proveedor::create([ 'nombre' => 'COMERCIALIZADORA INTERAMERICANA, SA. DE C.V.', 'telefono' => '2289-7377', 'direccion' => 'BLVD. SANTA ELENA, C. CERRO VERDE PTE. URB SANTA ELENA NO.53', 'nit' => '0614-200801-103-7', 'registro' =>'134055-9']);
        Proveedor::create([ 'nombre' => 'CRIO INVERSIONES, S.A DE C.V', 'telefono' => '2271-0812', 'direccion' => 'FINAL 12 CALLE PONIENTE Y 21  AVENIDA SUR N0 1146, SAN SALVADOR', 'nit' => '0614-140303-101-6', 'registro' =>'148102-4']);
        Proveedor::create([ 'nombre' => 'DAE KI KIM', 'telefono' => '2563-3809', 'direccion' => 'RESIDENCIAL ALTOS DE MONTEBELLO, CALLE PARACUTIN GRUPO 2 NO. 8', 'nit' => '9405-230167-101-4', 'registro' =>'112230-4']);
        Proveedor::create([ 'nombre' => 'DILASAL, S.A DE C.V.', 'telefono' => '2222-5418', 'direccion' => '13 AV SUR BOULEVARD VENEZUELA #911, BARRIO SANTA ANITA, SS.', 'nit' => '0614-271000-102-3', 'registro' =>'126445-0']);
        Proveedor::create([ 'nombre' => 'DISMO S.A DE C.V.', 'telefono' => '2201-7707', 'direccion' => 'CARRETERA TRONCAL DEL NORTE, KM 11 1/2, BARRIO SAN SEBASTIAN.', 'nit' => '0614-271010-103-6', 'registro' =>'2051122-5']);
        Proveedor::create([ 'nombre' => 'DISZASA DE C.V.', 'telefono' => '2525-8113', 'direccion' => '17 AV. SUR, 14C. ORIENTE, STA. TECLA', 'nit' => '0511-111269-001-3', 'registro' =>'7-8']);
        Proveedor::create([ 'nombre' => 'EXIT DE EL SALVADOR, S.A. DE C.V.', 'telefono' => '2262-1542', 'direccion' => 'PROLONG. ALAM. JUAN PABLO II, EDIF. NO.7 COL. ESCALON, SAN SALVADOR', 'nit' => '0614-150394-102-8', 'registro' =>'78037-5']);
        Proveedor::create([ 'nombre' => 'INDUSTRIAS ALIMENTICIAS MONTECO, S.A DE C.V.', 'telefono' => '2202-0600', 'direccion' => '3A. AC. NTE. ENTRE 27 Y 29 CALLE PTE. EDIFICIO MONTECO, SAN SALVADOR', 'nit' => '0614-230610-108-1', 'registro' =>'203564-6']);
        Proveedor::create([ 'nombre' => 'INVERSIONES VIDA, S.A. DE C.V.', 'telefono' => '2213-2000', 'direccion' => 'CALLE A SAN MARCOS #2000 COL. AMERICA, S.S.', 'nit' => '0614-141292-102-4', 'registro' =>'72295-2']);
        Proveedor::create([ 'nombre' => 'LACTOSA S.A DE C.V.', 'telefono' => '2248-6600', 'direccion' => 'CALLE SIEMENS 1, PARQUE INDUSTRIAL SANTA ELENA,  ANTIGUO CUSCATLAN.', 'nit' => '0614-090284-002-4', 'registro' =>'161-9']);
        Proveedor::create([ 'nombre' => 'PHILIP MORRIS EL SALVADOR, S.A DE C.V', 'telefono' => '2233-5200', 'direccion' => 'AVENIDA EL BOQUERON, LOTES 2 Y 3 BLOCK B, URBANIZACION SANTA ELENA', 'nit' => '0614-310576-001-7', 'registro' =>'556-8']);
        Proveedor::create([ 'nombre' => 'PRICESMART', 'telefono' => '2246-7400', 'direccion' => '', 'nit' => '0614-160399-103-0', 'registro' =>'111508-1']);
        Proveedor::create([ 'nombre' => 'PRODUCTOS ALIMENTICIOS DIANA, S.A. DE C.V.', 'telefono' => '2277-1233', 'direccion' => '12 AV. SUR N0. 111, SOYAPANGO, SAN SALVADOR', 'nit' => '0614-311269-001-3', 'registro' =>'573-8']);
        Proveedor::create([ 'nombre' => 'PRODUCTOS ALIMENTICIOS QUEENIES, S.A DE C.V.', 'telefono' => '2274-4724', 'direccion' => 'CALLE A SAN ANTONIO ABAD, NO. 2425, SAN SALVADOR', 'nit' => '0614-091099-101-7', 'registro' =>'116661-3']);
        Proveedor::create([ 'nombre' => 'SABRITAS Y CIA., S. EN C. DE C.V.', 'telefono' => '2201-7900', 'direccion' => 'CALLE EL PROGRESO FINCA, SANTA ELENA LOTES 13, 14 Y 15, ANTIGUO CUSCATLAN', 'nit' => '0614-090694-105-6', 'registro' =>'79894-0']);

        
    }
     
}
            
