<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle;
     
class VentasTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 1; $i <= 600 ; $i++)
        {
            $table = new Venta;

            $table->correlativo     = $i;
            $table->estado          = 'Cobrada';
            $table->metodo_pago     = 'Efectivo';
            $table->tipo_documento  = $faker->randomElement(['Factura', 'Crédito Fiscal', 'Ticket']);
            $table->referencia      = $faker->numberBetween(10000,50000);
            $table->fecha           = $faker->dateTimeBetween('-5 years', 'now', $timezone = null);
            $table->iva_retenido    =  0;  
            $table->iva           = $faker->numberBetween(1,50);  
            $table->fovial        = $faker->numberBetween(1,50);  
            $table->cotrans       = $faker->numberBetween(1,50);  
            $table->subcosto      = $faker->numberBetween(50,150);  
            $table->subtotal      = $faker->numberBetween(50,150);  
            $table->total         = $faker->numberBetween(100,250);  
            $table->cliente_id    = $faker->numberBetween(1,50);  
            $table->caja_id       = $faker->numberBetween(1,2); 
            $table->corte_id      = $faker->numberBetween(1,2); 
            $table->usuario_id    =  $faker->numberBetween(1,2); 
            $table->save();

            for($j = 1; $j <= $faker->numberBetween(1,5) ; $j++)
            {
                $table = new Detalle;

                $table->producto_id = $faker->numberBetween(1,20);
                $table->cantidad    = $faker->numberBetween(1,20);
                $table->precio      = $faker->numberBetween(1,20);
                $table->costo      = $faker->numberBetween(1,20);
                $table->descuento   = 0;
                $table->tipo_impuesto =  'Gravada';  
                $table->subcosto      = $faker->numberBetween(50,150);  
                $table->subtotal      = $faker->numberBetween(50,150);  
                $table->total         = $faker->numberBetween(100,250);  

                if ($table->producto_id > 3) {
                    $table->fovial = $table->cantidad * 0.20;
                    $table->cotrans = $table->cantidad * 0.10;
                }else{
                    $table->fovial = 0;
                    $table->cotrans = 0;
                }
                
                $table->iva         = $table->precio * 0.13;
                
                $table->venta_id    = $i;
                $table->escombo    = false;
                $table->save();
                
            }
            
        }
    }
     
}