<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registros\Cliente;
     
class ClientesTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        Cliente::create([ 'nombre' => 'PRICESMART', 'registro' => '111508-1']);
        Cliente::create([ 'nombre' => 'DAE KI KIM', 'registro' => '112230-4']);
        Cliente::create([ 'nombre' => 'HELADOS SARITA S.A DE C.V', 'registro' => '112334-3']);
        Cliente::create([ 'nombre' => 'PRODUCTOS ALIMENTICIOS QUEENIES, S.A DE C.V.', 'registro' => '116661-3']);
        Cliente::create([ 'nombre' => 'DILASAL, S.A DE C.V.', 'registro' => '126445-0']);
        Cliente::create([ 'nombre' => 'ESCARRSA, DE C.V.', 'registro' => '133843-2']);
        Cliente::create([ 'nombre' => 'COMERCIALIZADORA INTERAMERICANA, SA. DE C.V.', 'registro' => '134055-9']);
        Cliente::create([ 'nombre' => 'INDUSTRIAS LA CONSTANCIA S.A DE C.V', 'registro' => '145663-4']);
        Cliente::create([ 'nombre' => 'DINANT DE EL SALVADOR, S.A DE C.V.', 'registro' => '157769-7']);
        Cliente::create([ 'nombre' => 'LACTOSA S.A DE C.V.', 'registro' => '161-9']);
        Cliente::create([ 'nombre' => 'DISTRIBUIDORA SALVADOREÑA, S.A DE C.V.', 'registro' => '177411-0']);
        Cliente::create([ 'nombre' => 'RAYOVAC EL SALVADOR, S.A . DE C.V.', 'registro' => '197-0']);
        Cliente::create([ 'nombre' => 'INDUSTRIAS ALIMENTICIAS MONTECO, S.A DE C.V.', 'registro' => '203564-6']);
        Cliente::create([ 'nombre' => 'DISMO S.A DE C.V.', 'registro' => '2051122-5           ']);
        Cliente::create([ 'nombre' => 'PRODUCTOS CARNICOS, SA. DE C.V.', 'registro' => '222-4']);
        Cliente::create([ 'nombre' => 'DELIPAN', 'registro' => '35598-4']);
        Cliente::create([ 'nombre' => 'EME. S.A. DE C.V.', 'registro' => '431-6']);
        Cliente::create([ 'nombre' => 'PHILIP MORRIS EL SALVADOR, S.A DE C.V', 'registro' => '556-8']);
        Cliente::create([ 'nombre' => 'PRODUCTOS ALIMENTICIOS DIANA, S.A. DE C.V.', 'registro' => '573-8']);
        Cliente::create([ 'nombre' => 'INVERSIONES VIDA, S.A. DE C.V.', 'registro' => '72295-2']);
        Cliente::create([ 'nombre' => 'EXIT DE EL SALVADOR, S.A. DE C.V.', 'registro' => '78037-5']);
        Cliente::create([ 'nombre' => 'SABRITAS Y CIA., S. EN C. DE C.V.', 'registro' => '79894-0']);
        Cliente::create([ 'nombre' => 'STEINER S.A. DE C.V.', 'registro' => '84-1']);
        Cliente::create([ 'nombre' => 'INALTA S.A DE C.V', 'registro' => '8829-3']);
        Cliente::create([ 'nombre' => 'TELEFONICA MOVILES EL SALVADOR, S. A DE C.V.', 'registro' => '97894-8']);

    }
     
}