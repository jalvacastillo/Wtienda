<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inventario\Categoria;
     
class CategoriasTableSeeder extends Seeder {
     
    public function run()
    {

        Categoria::create([ 'nombre' => 'Jugos']);
        Categoria::create([ 'nombre' => 'Sodas']);
        Categoria::create([ 'nombre' => 'Bebidas']);
        Categoria::create([ 'nombre' => 'Cervezas']);
        Categoria::create([ 'nombre' => 'Agua']);
        Categoria::create([ 'nombre' => 'Salud']);
        Categoria::create([ 'nombre' => 'Cigarros']);
        Categoria::create([ 'nombre' => 'Sopas']);
        Categoria::create([ 'nombre' => 'Chicles']);
        Categoria::create([ 'nombre' => 'Chocolates']);
        Categoria::create([ 'nombre' => 'Galletas']);
        Categoria::create([ 'nombre' => 'Yumis']);
        Categoria::create([ 'nombre' => 'Papas']);
        Categoria::create([ 'nombre' => 'Diana']);
        Categoria::create([ 'nombre' => 'Varios']);
        Categoria::create([ 'nombre' => 'Comida']);
        Categoria::create([ 'nombre' => 'Baterias']);
        Categoria::create([ 'nombre' => 'Paletas']);
        Categoria::create([ 'nombre' => 'Preservativos']);
        Categoria::create([ 'nombre' => '1/2 licores']);
        Categoria::create([ 'nombre' => 'Miniaturas']);
        Categoria::create([ 'nombre' => 'Vino']);
        Categoria::create([ 'nombre' => 'Licores']);
        Categoria::create([ 'nombre' => 'Medicina']);
        Categoria::create([ 'nombre' => 'Pan bimbo']);
        Categoria::create([ 'nombre' => 'Accesorios de vehiculo']);
        Categoria::create([ 'nombre' => 'Dulces']);
        Categoria::create([ 'nombre' => 'Telefonia']);
        Categoria::create([ 'nombre' => 'Pan sinai']);
        Categoria::create([ 'nombre' => 'Te helados']);
        Categoria::create([ 'nombre' => 'Energizantes']);
        Categoria::create([ 'nombre' => 'Lacteos']);
        Categoria::create([ 'nombre' => 'Cafes']);
        Categoria::create([ 'nombre' => 'Boquitas ideal zambos y pro']);
        Categoria::create([ 'nombre' => 'Pizza']);
        Categoria::create([ 'nombre' => 'Gomitas y marshmallows']);
        Categoria::create([ 'nombre' => 'Mentas']);
        Categoria::create([ 'nombre' => 'Sandwich']);
        Categoria::create([ 'nombre' => 'Materia prima']);
        Categoria::create([ 'nombre' => 'Esmalte de uñas']);
        Categoria::create([ 'nombre' => 'Pan san antonio']);
        Categoria::create([ 'nombre' => 'Pan dulce']);

    }
     
}