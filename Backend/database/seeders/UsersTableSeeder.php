<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $faker = \Faker\Factory::create();

            $user = new User;
            $user->name = 'Usuario Admin';
            $user->username = 'admin';
            $user->password = Hash::make('admin');
            $user->tipo = 'Administrador';
            $user->save();

            $user = new User;
            $user->name = 'Usuario Tienda';
            $user->username = 'tienda';
            $user->password = Hash::make('emple');
            $user->tipo = 'Empleado';
            $user->caja_id = 1;
            $user->save();


            $user = new User;
            $user->name = 'Usuario Gasolinera';
            $user->username = 'gas';
            $user->password = Hash::make('emple');
            $user->tipo = 'Empleado';
            $user->caja_id = 1;
            $user->save();

            $user = new User;
            $user->name = 'Supervisor';
            $user->username = 'supervisor';
            $user->password = Hash::make('emple');
            $user->codigo = '1234';
            $user->tipo = 'Supervisor';
            $user->save();
            
    }
}
