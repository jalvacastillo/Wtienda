<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Empresa;
use App\Models\Admin\Meta;
use App\Models\Registros\Cliente;

class EmpresaTableSeeder extends Seeder {
    
    public function run(){

        $table = new Empresa;

        $table->nombre        = 'SERVICENTRO RIVERA S.A. DE C.V.';
        $table->direccion     = 'Panamericana KM. 33 1/2, Barrio Concepción, Cojutepeque.';
        $table->telefono      = '0000-0000';
        $table->nit           = '0614-310591-102-2';
        $table->ncr           = '7608-2';
        $table->giro           = 'Gasolinera, Compra y venta de Rep. para Vehiculo';
        $table->correo        = 'tu@correo.com';
        $table->municipio     = 'Ilobasco';
        $table->departamento  = 'Cabañas';

        $table->save();

        // Default
        $table = new Cliente;
        $table->nombre = 'CLIENTE GENERAL';
        $table->save();


    }

}
