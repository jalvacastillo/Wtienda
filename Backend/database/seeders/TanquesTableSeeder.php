<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Tanque;
     
class TanquesTableSeeder extends Seeder {
     
    public function run()
    {

        Tanque::create([ 
            'nombre' => 'Especial',
            'producto_id' => 1,
            'stock' => 1000,
            'stock_min' => 150,
            'stock_max' => 5000
        ]);
        Tanque::create([ 
            'nombre' => 'Regular',
            'producto_id' => 2,
            'stock' => 1000,
            'stock_min' => 150,
            'stock_max' => 5000
        ]);
        Tanque::create([ 
            'nombre' => 'Diesel',
            'producto_id' => 3,
            'stock' => 1000,
            'stock_min' => 150,
            'stock_max' => 5000
        ]);
        // Tanque::create([ 
        //     'nombre' => 'Diesel B',
        //     'producto_id' => 3,
        //     'stock' => 2000,
        //     'stock_min' => 100,
        //     'stock_max' => 3000
        // ]);

        

    }
     
}