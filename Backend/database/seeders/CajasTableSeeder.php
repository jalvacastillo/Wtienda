<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Caja;
use App\Models\Admin\Documento;
use App\Models\Admin\FormaPago;
     
class CajasTableSeeder extends Seeder {
     
    public function run()
    {


        Caja::create(['nombre'=> 'Principal', 'tipo'=> 'Tienda', 'descripcion'   => 'En pista', ]);
            Documento::create(['nombre'=> 'Credito Fiscal', 'inicial' => 1, 'actual'=> 1, 'final'=> 100, 'caja_id' => 1, ]);
            Documento::create(['nombre'=> 'Factura', 'inicial' => 1, 'actual'=> 1, 'final'=> 100, 'caja_id' => 1, ]);
            Documento::create(['nombre'=> 'Ticket', 'actual'=> 1, 'inicial' => 1, 'final'=> 100, 'caja_id' => 1, ]);
            Documento::create(['nombre'=> 'Nota Credito', 'actual'=> 1, 'inicial' => 1, 'final'=> 100, 'caja_id' => 1, ]);
            FormaPago::create(['nombre'=> 'Efectivo', 'caja_id' => 1, ]);
            FormaPago::create(['nombre'=> 'Tarjeta', 'caja_id' => 1, ]);
            FormaPago::create(['nombre'=> 'Vale', 'caja_id' => 1, ]);
            FormaPago::create(['nombre'=> 'Credito', 'caja_id' => 1, ]);
            FormaPago::create(['nombre'=> 'Chivo Wallet', 'caja_id' => 1, ]);

    }
     
}
