<?php

namespace App\Http\Controllers\Api\Compras;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Compras\Salida;

class SalidasController extends Controller
{
    

    public function index() {
       
        $usuario = JWTAuth::parseToken()->authenticate();

        $mes = date('m');
        $ano = date('Y');
        $dia = date('d');
        
        if ($usuario->tipo == 'Administrador') {
            $salidas = Salida::orderBy('id', 'desc')->paginate(10);
        } else {
            $salidas = Salida::where('usuario_id', $usuario->id)
                                        ->whereYear('created_at', $ano)
                                        ->whereMonth('created_at', $mes)
                                        ->whereDay('created_at', $dia)
                                        ->orderBy('id','desc')->paginate(10);
        }

        return Response()->json($salidas, 200);

    }


    public function read($id) {
    	
        $entrada = Salida::findOrFail($id);
        return Response()->json($entrada, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'         => 'required',
            'total'          => 'required',
            'usuario_id'     => 'required',
        ]);

        if($request->id)
            $salida = Salida::findOrFail($request->id);
        else
            $salida = new Salida;
        
        $salida->fill($request->all());
        $salida->save();

        return Response()->json($salida, 200);

    }


}
