<?php

namespace App\Http\Controllers\Api\Compras;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Compras\DevolucionCompra;
use App\Models\Compras\DevolucionDetalle;
use App\Models\Registros\Proveedor;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Inventario;
use App\Models\Admin\Tanque;
use App\Models\Inventario\Kardex;
use Illuminate\Support\Facades\DB;

class DevolucionComprasController extends Controller
{
    

    public function index() {
       
        $compras = DevolucionCompra::orderBy('id','desc')->paginate(10);
        return Response()->json($compras, 200);
           
    }

    public function read($id) {

        $compra = DevolucionCompra::where('id', $id)->with('detalles', 'proveedor')->first();
        return Response()->json($compra, 200);
 
    }

    public function search($txt) {

        $compras = DevolucionCompra::whereHas('proveedor', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');
                    })->paginate(10);

        return Response()->json($compras, 200);

    }

    public function filter(Request $request) {

        $compras = DevolucionCompra::when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->when($request->proveedor_id, function($query) use ($request){
                                return $query->whereHas('proveedor', function($query) use ($request)
                                {
                                    $query->where('proveedor_id', $request->proveedor_id);

                                });
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($compras, 200);

    }



    public function store(Request $request)
    {

        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'proveedor_id'      => 'required',
            'usuario_id'        => 'required',
        ]);

        if($request->id)
            $compra = DevolucionCompra::findOrFail($request->id);
        else
            $compra = new DevolucionCompra;
        
        $compra->fill($request->all());
        $compra->save();

        return Response()->json($compra, 200);

    }

    public function delete($id)
    {
        $compra = DevolucionCompra::findOrFail($id);
        foreach ($compra->detalles as $detalle) {
            // Actualizar inventario
                $producto = Producto::findOrFail($detalle->producto_id);
                if ($producto->categoria == 1) {
                    Tanque::where('id', $detalle->tanque_id)
                                ->increment('stock', $detalle->cantidad);
                } else {
                    Inventario::where('producto_id', $detalle->producto_id)
                        ->where('bodega_id', 1)
                        ->increment('stock', $detalle->cantidad);
                } 

            $detalle->delete();
        }
        $compra->delete();

        return Response()->json($compra, 201);

    }


    public function facturacion(Request $request){

        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'proveedor'         => 'required',
            'detalles'          => 'required',
            'usuario_id'        => 'required',
        ]);

        DB::beginTransaction();
         
        try {

        // Guardamos el proveedor

            if(isset($request->proveedor['id']))
                $proveedor = Proveedor::findOrFail($request->proveedor['id']);
            else
                $proveedor = new Proveedor;

            $proveedor->fill($request->proveedor);
            $proveedor->save();

        // DevolucionCompra
            if($request->id)
                $compra = DevolucionCompra::findOrFail($request->id);
            else
                $compra = new DevolucionCompra;
            $request['proveedor_id'] = $proveedor->id;
            $compra->fill($request->all());
            $compra->save();


        // Detalles

            foreach ($request->detalles as $det) {
                if(isset($det['id']))
                    $detalle = DevolucionDetalle::findOrFail($det['id']);
                else
                    $detalle = new DevolucionDetalle;

                $det['compra_id'] = $compra->id;
                
                $detalle->fill($det);
                $detalle->save();

                // Actualizar inventario
                $producto = Producto::findOrFail($det['producto_id']);
                if ($producto->inventario) {
                    // Si es gasolina aumentar tanque
                    if (strtoupper($producto->categoria) == 'COMBUSTIBLE') {
                        if (isset($det['tanque_id'])) {
                            $inventario = Tanque::where('id', $det['tanque_id'])->first();
                        }
                    // Si es producto aumentar bodega
                    } else {
                        $inventario = Inventario::where('producto_id', $producto->id)->where('bodega_id', $det['bodega_id'])->first();
                    }


                    // Kardex
                    if ($inventario) {
                        $inventario->stock -= $det['cantidad'];
                        $inventario->save();
                        $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                        $entradaCantidad =  null;
                        $salidaCantidad = $det['cantidad'];
                        Kardex::create([
                            'fecha'             => date('Y-m-d'),
                            'producto_id'       => $producto->id,
                            'bodega_id'         => $inventario->bodega_id,
                            'detalle'           => 'Devolución de Compra',
                            'referencia'        => $compra->id,
                            'valor_unitario'    => $valor,
                            'entrada_cantidad'  => $entradaCantidad,
                            'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                            'salida_cantidad'   => $salidaCantidad,
                            'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                            'total_cantidad'    => $inventario->stock,
                            'total_valor'       => $inventario->stock * $valor,
                            'usuario_id'        => $request->usuario_id,
                        ]);
                    }

                }

            }

        
        DB::commit();
        return Response()->json($compra, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }
        
        return Response()->json($compra, 200);

    }



}
