<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Ventas\Entrada;

class EntradasController extends Controller
{
    

    public function index() {
       
        $usuario = JWTAuth::parseToken()->authenticate();

        $mes = date('m');
        $ano = date('Y');
        $dia = date('d');
        
        if ($usuario->tipo == 'Administrador') {
            $entradas = Entrada::orderBy('id', 'dsc')->paginate(10);
        } else {
            $entradas = Entrada::where('usuario_id', $usuario->id)
                                        ->whereYear('created_at', $ano)
                                        ->whereMonth('created_at', $mes)
                                        ->whereDay('created_at', $dia)
                                        ->orderBy('id','dsc')->paginate(10);
        }

        return Response()->json($entradas, 200);

    }


    public function read($id) {
    	
        $entrada = Entrada::findOrFail($id);
        return Response()->json($entrada, 200);

    }

     public function store(Request $request)
    {
        if($request->id){
            $entrada = Entrada::findOrFail($request->id);
        }
        else{
            $entrada = new Entrada;
        }
        
        $entrada->fill($request->all());
        $entrada->save();

        return Response()->json($entrada, 200);

    }


}
