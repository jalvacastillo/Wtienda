<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\DetalleCombo;
use App\Models\Admin\Empresa;

use App\Models\Admin\Caja;
use App\Models\Registros\Cliente;
use App\Models\Ventas\DevolucionDetalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Combos\Combo;
use App\Models\Admin\Tanque;
use App\Models\Inventario\Inventario;
use App\Models\Inventario\Kardex;

class DevolucionVentasController extends Controller
{
    

    public function index() {
       
        $ventas = DevolucionVenta::orderBy('id','desc')->paginate(10);
       
        return Response()->json($ventas, 200);

    }



    public function read($id) {

        $venta = DevolucionVenta::where('id', $id)->with('detalles', 'cliente')->first();
        return Response()->json($venta, 200);

    }

    public function search($txt) {

        $ventas = DevolucionVenta::whereHas('cliente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('forma_de_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($ventas, 200);

    }

    public function filter(Request $request) {


        $ventas = DevolucionVenta::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->forma_de_pago, function($query) use ($request){
                            return $query->where('forma_de_pago', $request->forma_de_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($ventas, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'cliente_id'        => 'required',
            'usuario_id'        => 'required',
        ]);

        if($request->id)
            $venta = DevolucionVenta::findOrFail($request->id);
        else
            $venta = new Venta;
        
        $venta->fill($request->all());
        $venta->save();        

        return Response()->json($venta, 200);

    }

    public function delete($id)
    {
        $venta = DevolucionVenta::findOrFail($id);

        foreach ($venta->detalles as $detalle) {
            // Actualizar inventario
                $producto = Producto::findOrFail($detalle->producto_id);
                if ($producto->tipo == 'Compuesto') {
                        
                    foreach ($producto->composiciones as $comp) {
                        $bodega = Inventario::where('bodega_id', 2)->where('producto_id', $comp->compuesto_id)->decrement('stock', ($request->cantidad * $comp->cantidad));
                    }
                        
                }else{
                    if ($producto->inventario) {
                        // Gasolina
                        if (strtoupper($producto->categoria) == 'COMBUSTIBLE') {
                            if (isset($det['tanque_id'])) {
                                Tanque::where('id', $detalle->tanque_id)->decrement('stock', $detalle->cantidad);
                            }
                        }
                        // Telefonia
                        elseif (strtoupper($producto->categoria) == 'TELEFONIA') {
                            $bodega = Inventario::where('producto_id', $producto->id)->where('bodega_id', 2)->decrement('stock', $detalle->precio);                                    
                        }else{
                        // Productos
                            $bodega = Inventario::where('producto_id', $producto->id)->where('bodega_id', 2)->decrement('stock', $detalle->cantidad);
                        }
                    }
                }

            $detalle->delete();
        }
        $venta->delete();

        return Response()->json($venta, 201);

    }

    public function corte() {

        $usuario = JWTAuth::parseToken()->authenticate();
       
        $caja   = Caja::where('id', $usuario->caja_id)->with('corte')->firstOrFail();
        $corte  = $caja->corte;
        $ventas = $corte->devoluciones()->orderBy('id', 'desc')
                            ->paginate(30);

        return Response()->json($ventas, 200);

    }


    public function facturacion(Request $request){

        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'cliente'           => 'required',
            'detalles'          => 'required',
            'usuario_id'        => 'required',
            'caja_id'           => 'required',
            'corte_id'          => 'required',
        ]);

        // Guardamos el cliente

            if(isset($request->cliente['id']))
                $cliente = Cliente::findOrFail($request->cliente['id']);
            else
                $cliente = new Cliente;

            $cliente->fill($request->cliente);
            $cliente->save();

        // Guardamos la venta
            $venta = new DevolucionVenta;
            $request['cliente_id'] = $cliente->id;
            $venta->fill($request->all());
            $venta->save();

        // Incrementar el correlarivo
            // Documento::where('caja_id', JWTAuth::parseToken()->authenticate()->caja_id)
            //                     ->where('nombre', $request->tipo_documento)
            //                     ->increment('actual');

        // Guardamos los detalles

            foreach ($request->detalles as $det) {
                if(isset($det['id']))
                    $detalle = DevolucionDetalle::findOrFail($det['id']);
                else
                    $detalle = new DevolucionDetalle;
                $det['venta_id'] = $venta->id;
                

                $detalle->fill($det);
                $detalle->save();

                // Actualizar inventario
                if ($request->estado == 'Cobrada') {
                    
                    if ($det['escombo']) {
                        if ($det['detalles']) {
                            foreach ($det['detalles'] as $item) {
                                Inventario::where('producto_id', $item['producto_id'])->where('bodega_id', 2)->increment('stock', $item['cantidad']);

                                $cd = new DetalleCombo;
                                $cd->producto_id = $item['producto_id'];
                                $cd->cantidad   = $item['cantidad'];
                                $cd->precio     = $item['precio'];
                                $cd->costo      = $item['costo'];
                                $cd->detalle_id = $detalle->id;
                                $cd->save();
                            }
                        }
                    } else {
                        $producto = Producto::findOrFail($det['producto_id']);

                        if ($producto->tipo == 'Compuesto') {
                            
                            foreach ($producto->composiciones as $comp) {
                                if ($comp->inventario) {
                                    $bodega = Inventario::where('bodega_id', 2)->where('producto_id', $comp->compuesto_id)->increment('stock', ($det['cantidad'] * $comp->cantidad));
                                }
                            }
                            if ($producto->inventario) {
                                $bodega = Inventario::where('producto_id', $producto->id)->where('bodega_id', 2)->increment('stock', $det['cantidad']);
                            }
                        }else{
                            if ($producto->inventario) {
                                // Si es gasolina aumentar tanque
                                if (strtoupper($producto->categoria) == 'COMBUSTIBLE') {
                                    if (isset($det['tanque_id'])) {
                                        $inventario = Tanque::where('id', $det['tanque_id'])->first();
                                    }
                                // Si es producto aumentar bodega
                                } else {
                                    $inventario = Inventario::where('producto_id', $producto->id)->where('bodega_id', 2)->first();
                                }

                                // Kardex
                                if ($inventario) {
                                    $inventario->stock += $det['cantidad'];
                                    $inventario->save();
                                    $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                                    $entradaCantidad =  $det['cantidad'];
                                    $salidaCantidad = null;
                                    Kardex::create([
                                        'fecha'             => date('Y-m-d'),
                                        'producto_id'       => $producto->id,
                                        'bodega_id'         => $inventario->bodega_id,
                                        'detalle'           => 'Devolución de Venta',
                                        'referencia'        => $venta->id,
                                        'valor_unitario'    => $valor,
                                        'entrada_cantidad'  => $entradaCantidad,
                                        'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                        'salida_cantidad'   => $salidaCantidad,
                                        'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                        'total_cantidad'    => $inventario->stock,
                                        'total_valor'       => $inventario->stock * $valor,
                                        'usuario_id'        => $request->usuario_id,
                                    ]);
                                }

                            }
                        }
                    }
                }
            }

        
        return Response()->json($venta, 200);

    }

    public function generarDoc($id){
        $venta = DevolucionVenta::where('id', $id)->with('detalles', 'cliente', 'documento')->firstOrFail();
        $empresa = Empresa::find(1);
        $venta->empresa = $empresa;

        $partes = explode('.', strval( number_format($venta->total, 2) ));

        $venta->total_letras = \NumeroALetras::convertir($partes[0], 'Dolares con ') . $partes[1].'/100';

        if ($venta->tipo_documento == 'Factura') {

            return view('reportes.factura', compact('venta', 'empresa'));
        }
        elseif ($venta->tipo_documento == 'Credito Fiscal') {

            return view('reportes.credito', compact('venta', 'empresa'));

        }elseif ($venta->tipo_documento == 'Ticket') {

            return view('reportes.ticket-devolucion', compact('venta', 'empresa'));
        }
        else{
            return "Venta sin tipo";
        }

    }


}
