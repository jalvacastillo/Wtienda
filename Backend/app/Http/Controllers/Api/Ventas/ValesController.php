<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

use App\User;
use App\Models\Admin\Caja;
use App\Models\Admin\Correlativo;
use App\Models\Admin\Corte;
use App\Models\Ventas\Detalle as VentaDetalle;
use App\Models\Ventas\Venta;
use App\Models\Compras\Compra;
use App\Models\Compras\DevolucionCompra;

class ValesController extends Controller
{

    public function vales() {
       
        $vales = Venta::where('metodo_pago', 'Vale')->orderBy('fecha','desc')->paginate(10);

        return Response()->json($vales, 200);

    }

    public function valesSearch($txt) {
       
        $vales = Venta::where('metodo_pago', 'Vale')->where('referencia', 'like' ,'%' . $txt . '%')->orderBy('fecha','desc')->paginate(10);

        return Response()->json($vales, 200);

    }

    public function valesFilter(Request $request) {


        $ventas = Venta::where('metodo_pago', 'Vale')
                            ->when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->usuario_id, function($query) use ($request){
                                return $query->where('usuario_id', $request->usuario_id);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($ventas, 200);

    }

    public function corte() {

        $usuario = JWTAuth::parseToken()->authenticate();
        
        $corte  = Corte::where('caja_id', $usuario->caja_id)->where('estado', 'Abierta')->orderBy('id', 'desc')->first();
        
        $vales = Venta::where('metodo_pago', 'Vale')->where('usuario_id',  $usuario->id)
                            ->where('fecha', '>=', $corte->inicio)
                            ->orderBy('id','desc')->paginate(5000);
       
        return Response()->json($vales, 200);

    }

}