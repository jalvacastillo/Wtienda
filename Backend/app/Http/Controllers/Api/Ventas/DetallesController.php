<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Ventas\Detalle;
use App\Models\Ventas\DetalleCombo;
use App\Models\Inventario\Producto;
use App\Models\Admin\Bomba;
use App\Models\Admin\Tanque;
use App\Models\Inventario\Inventario;

class DetallesController extends Controller
{
    

    public function index() {

        $detalles = Detalle::orderBy('created_at','desc')->paginate(10);

        return Response()->json($detalles, 200);

    }


    public function read($id) {

        $detalle = Detalle::findOrFail($id);
        return Response()->json($detalle, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'producto_id'    => 'required',
            'cantidad'    => 'required',
            'precio'    => 'required',
            'costo'    => 'required',
            'venta_id'    => 'required'
        ]);
        
        if($request->id){
            $detalle = Detalle::findOrFail($request->id);
        }
        else{
            $detalle = new Detalle;

        // Actualizar inventario
            $producto = Producto::findOrFail($request->producto_id);
            if ($producto->inventario) {
                // Si es gasolina disminuir tanque
                if ($producto->categoria_id == 1) {
                    $bomba = Bomba::findOrFail($request->bomba_id);
                    
                    $tanque = Tanque::findOrFail($bomba->tanque_id);
                    $tanque->stock -= $request->cantidad;
                    $bomba->lectura += $request->cantidad;
                    $bomba->save();
                    $tanque->save();
                // Si es producto disminuir bodega
                } else {
                    $bodega = Inventario::where('bodega_id', 2)->where('producto_id',$producto->id)->first();
                    $bodega->stock -= $request->cantidad;
                    $bodega->save();
                }            
            }
        }
        
        $detalle->fill($request->all());
        $detalle->save();

        return Response()->json($detalle, 200);

    }

    public function delete($id)
    {
        $detalle = Detalle::findOrFail($id);
        // Actualizar inventario
            $producto = Producto::findOrFail($detalle->producto_id);
            if ($producto->inventario) {
                if ($producto->categoria == 1) {
                    $tanque = Tanque::findOrFail($detalle->tanque_id);
                    $tanque->stock += $detalle->cantidad;
                    $tanque->save();
                } else {
                    $bodega = Inventario::where('bodega_id', 2)->where('producto_id', $detalle->producto_id)->first();
                    $bodega->stock += $detalle->cantidad;
                    $bodega->save();
                }
            }
        $detalle->delete();

        return Response()->json($detalle, 201);

    }

    public function historial(Request $request) {

        $request->inicio = $request->inicio . ' ' . $request->inicio_hora . ':00';
        $request->fin = $request->fin . ' ' . $request->fin_hora . ':59';


        $ventas = Detalle::whereHas('venta', function($query) use ($request){
                            $query->where('estado', 'Cobrada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->nombre, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('nombre', 'like' ,'%' . $request->nombre . '%');
                            });
                        })
                        ->when($request->categoria_id, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('categoria_id', $request->categoria_id );
                            });
                        })
                        ->get()
                        ->groupBy('producto_id');

        $ventas_combos = DetalleCombo::whereHas('detalle.venta', function($query) use ($request){
                            $query->where('estado', 'Cobrada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->nombre, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('nombre', 'like' ,'%' . $request->nombre . '%');
                            });
                        })
                        ->when($request->categoria_id, function($query) use ($request){
                            return $query->whereHas('producto', function($q) use ($request){
                                $q->where('categoria_id', $request->categoria_id );
                            });
                        })
                        ->get()
                        ->groupBy('producto_id');
        
        $movimientos = collect();

        foreach ($ventas as $venta) {
            $movimientos->push([
                'fecha'         => $venta[0]->venta->fecha,
                'producto'      => $venta[0]->producto_nombre,
                'medida'        => $venta[0]->medida,
                'cantidad'      => $venta->sum('cantidad'),
                'total'         => $venta->sum('total'),
                'costo'         => $venta->sum('subcosto'),
                'utilidad'      => $venta->sum('total') - $venta->sum('subcosto'),
                'margen'        => $venta->sum('total') > 0 ? round((($venta->sum('total') - ($venta->sum('subcosto'))) / $venta->sum('total') * 100), 2) : null
            ]);
        }

        foreach ($ventas_combos as $venta) {
            $ids = '';
            foreach ($venta as $item) {
                $ids .= $item->detalle->venta->id . ' '; 
            }
            $total = $venta[0]->precio * $venta->sum('detalle_cantidad');
            $movimientos->push([
                'ids'           => $ids,
                'fecha'         => $venta[0]->created_at->toDateString(),
                'producto'      => $venta[0]->producto,
                'medida'        => $venta[0]->detalle()->first()->producto()->first()->medida,
                'cantidad'      => $venta->sum('detalle_cantidad'),
                'total'         => $total,
                'costo'         => $venta->sum('costo'),
                'utilidad'      => $total - $venta->sum('costo'),
                'margen'        => $total > 0 ? round((($total - ($venta->sum('costo'))) / $total * 100), 2) : null
            ]);
        }

        return Response()->json($movimientos, 200);

    }


}
