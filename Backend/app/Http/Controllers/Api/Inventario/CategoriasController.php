<?php

namespace App\Http\Controllers\Api\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Categoria;
use App\Models\Ventas\Detalle as DetalleVenta;
use App\Models\Ventas\DetalleCombo;
use App\Models\Compras\Detalle as DetalleCompra;

class CategoriasController extends Controller
{
    

    public function index() {
       
        $categorias = Categoria::orderBy('id','asc')->get();

        return Response()->json($categorias, 200);

    }


    public function read($id) {

        $categoria = Categoria::findOrFail($request->id);
        return Response()->json($categoria, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre'    => 'required|max:255',
        ]);

        if($request->id)
            $categoria = Categoria::findOrFail($request->id);
        else
            $categoria = new Categoria;
        
        $categoria->fill($request->all());
        $categoria->save();

        return Response()->json($categoria, 200);

    }

    public function delete($id)
    {
        $categoria = Categoria::findOrFail($id);

        if ($categoria->productos()->count() > 0)
            return Response()->json(['error' => ['La categoria tiene productos asignados'], 'code' => 422], 422);

        $categoria->delete();

        return Response()->json($categoria, 201);

    }

    public function historialVentas(Request $request) {

        $request->inicio = $request->inicio . ' ' . $request->inicio_hora . ':00';
        $request->fin = $request->fin . ' ' . $request->fin_hora . ':59';


        $ventas = DetalleVenta::with('producto.categoria')
                        ->whereHas('venta', function($query) use ($request){
                            $query->where('estado', 'Cobrada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->get()
                        ->groupBy(function($detalle) {
                            return $detalle->producto()->first()->categoria;
                        });
        
        $ventas_combos = DetalleCombo::with('producto.categoria')
                        ->whereHas('detalle.venta', function($query) use ($request){
                            $query->where('estado', 'Cobrada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->get()
                        ->groupBy(function($detalle) {
                            return $detalle->producto()->first()->categoria;
                        });
        

        $movimientos = collect();


        foreach ($ventas as $venta) {

            $cantidad = $venta->sum('cantidad');
            $total = $venta->sum('total');
            $costo = $venta->sum('subcosto');
            
            foreach ($ventas_combos as $combo) {
                if ($combo[0]->producto()->first()->categoria()->first()->nombre == $venta[0]->producto()->first()->categoria) {
                    $cantidad += $combo->sum('detalle_cantidad');
                    $total += $combo[0]->precio * $combo->sum('detalle_cantidad');
                    $costo += $costo + $combo->sum('costo');
                }
            }   

            $movimientos->push([
                'categoria'     => $venta[0]->producto()->first()->categoria,
                'cantidad'      => $cantidad,
                'total'         => $total,
                'costo'         => $costo,
                'utilidad'      => $total - $costo,
                'margen'        => $total > 0 ? round((($total - $costo) / $total * 100), 2) : null
            ]);
        }


        return Response()->json($movimientos, 200);

    }

    public function historialCompras(Request $request) {

        $ventas = DetalleCompra::with('producto.categoria')
                        ->whereHas('compra', function($query) use ($request){
                            $query->where('estado', 'Pagada')
                            ->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->get()
                        ->groupBy(function($detalle) {
                            return $detalle->producto()->first()->categoria;
                        });
        
        // return Response()->json($ventas, 200);
        $movimientos = collect();

        foreach ($ventas as $venta) {
            $movimientos->push([
                'categoria'     => $venta[0]->producto()->first()->categoria,
                'cantidad'      => $venta->count(),
                'subtotal'      => $venta->sum('subtotal'),
                'iva'         => $venta->sum('iva'),
                'total'         => $venta->sum('total')
            ]);
        }

        return Response()->json($movimientos, 200);

    }

    public function change(Request $request){

        $request->validate([
            'categoria_anterior'  => 'required|numeric',
            'categoria_nueva'  => 'required|numeric',
        ]);

        $categoria = Categoria::findOrFail($request->categoria_anterior);

        foreach ($categoria->productos as $producto) {
            $producto->categoria_id = $request->categoria_nueva;
            $producto->save();
        }

        return Response()->json($categoria, 200);


    }


}
