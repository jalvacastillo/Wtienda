<?php

namespace App\Http\Controllers\Api\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Empresa;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Traslados\Traslado;
use App\Models\Inventario\Ajuste;
use App\Models\Compras\Compra;
use App\Models\Compras\Detalle as DetalleCompra;
use App\Models\Compras\DevolucionCompra;
use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle as DetalleVenta;
use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\DetalleCombo;

use App\Imports\Productos;
use Maatwebsite\Excel\Facades\Excel;

class ProductosController extends Controller
{
    

    public function index() {
       
        $productos = Producto::where('tipo', 'Producto')->with('bodegas')
                                // ->whereNull('codigo')
                                ->orderBy('id','desc')->paginate(10);

        return Response()->json($productos, 200);

    }

    public function gasolina() {
       
        $productos = Producto::where('categoria_id', '=', 1)
                    ->with('tanques')->get();

        return Response()->json($productos, 200);

    }

    public function telefonia() {
       
        $productos = Producto::whereHas('categoria', function($q){
                                    $q->where('nombre', 'TELEFONIA');
                                })
                                ->with('bodegas')
                                ->orderBy('id','asc')
                                ->paginate(1000);

        return Response()->json($productos, 200);

    }

    public function porCodigo($codigo) {
       
        // $producto = Producto::where('codigo', $codigo )->with('bodegas')->first();
        $producto = Producto::where('codigo', $codigo )->with('bodegas')->get();

        return Response()->json($producto, 200);

    }

    public function read($id) {

        $producto = Producto::where('id', $id)->with('bodegas', 'tanques', 'composiciones', 'promociones')->first();
        return Response()->json($producto, 200);

    }

    public function search($txt) {

        $productos = Producto::where('categoria_id', '!=', 1)
                                ->where('tipo', 'Producto')
                                ->with('bodegas')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function searchAll($txt) {

        $productos = Producto::where('categoria_id', '!=', 1)
                                ->with('bodegas')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function filter(Request $request) {

            $productos = Producto::where('tipo', 'Producto')->with('bodegas')
                                ->when($request->categoria_id, function($query) use ($request){
                                    return $query->where('categoria_id', $request->categoria_id);
                                })
                                ->when($request->stock_bodega, function($query) use ($request){
                                    return $query->whereHas('bodegas', function($query){
                                        return $query->where('bodega_id', 1)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->stock_venta, function($query) use ($request){
                                    return $query->whereHas('bodegas', function($query){
                                        return $query->where('bodega_id', 2)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->sin_control_inventario, function($query) use ($request){
                                    return $query->whereDoesntHave('bodegas')->orwhere('inventario', false);
                                })
                                ->when($request->sin_condigo, function($query) use ($request){
                                    return $query->whereNull('codigo');
                                })
                                ->orderBy('id','desc')->paginate(100000);

            return Response()->json($productos, 200);
    }

    public function store(Request $request)
    {
        if(empty($request->codigo)){
            $request['codigo'] = NULL;
        }
        
        $request->validate([
            'nombre'    => 'required|max:255',
            // 'codigo'    => 'nullable|unique:productos,codigo,'. $request->id . ',id,deleted_at,NULL' ,
            'precio'    => 'required|numeric',
            'costo'     => 'required|numeric',
            'categoria_id'    => 'required',
        ]);

        if($request->id)
            $producto = Producto::findOrFail($request->id);
        else
            $producto = new Producto;
        
        $producto->fill($request->all());
        $producto->save();

        return Response()->json($producto, 200);

    }

    public function delete($id)
    {
        $producto = Producto::findOrFail($id);
        foreach ($producto->bodegas as $bodega) {
            $bodega->delete();
        }
        $producto->delete();

        return Response()->json($producto, 201);

    }

    public function precios($id)
    {
        $producto = Producto::findOrFail($id);
        
        
        $ventas = DetalleVenta::where('producto_id', $producto->id)->get();

        $ventas_precios =  collect();
        $ventas_fechas =  collect();

        foreach ($ventas->unique('precio') as $venta) {
            $ventas_precios->push($venta->precio);
            $ventas_fechas->push($venta->created_at->format('d/m/Y'));
        }
        $producto->ventas_precios = $ventas_precios;
        $producto->ventas_fechas = $ventas_fechas;
        $producto->ventas = count($ventas);

        return Response()->json($producto, 201);

    }


    public function cardex(Request $request){

        $id = $request->producto_id;
        $star = $request->inicio;
        $end = $request->fin;

        $movimientos = collect();

        $producto = Producto::findOrFail($id);

        if ($producto->composicion) {

            foreach ($producto->composicion as $com) {
                $ventas = Venta::with('detalles')
                ->whereBetween('fecha', array($star, $end))
                ->whereHas('detalles', function($query) use ($com)
                {
                    $query->where('producto_id', $com->producto_id);
                })
                ->get();
                foreach ($ventas as $venta) {
                    foreach ($venta->detalles as $detalle) {
                        if($detalle->producto_id == $com->producto_id)
                        {
                            if ($venta->estado == 'Anulada') {
                                $tipo = 'Venta: Anulada';
                            }else{
                                $tipo = 'Venta: Grupo';
                            }
                            $movimientos->push([
                                'fecha'         => $venta->fecha,
                                'referencia'    => $venta->correlativo ? $venta->correlativo : $venta->id,
                                'entrada'       => null,
                                'salida'        => $detalle->cantidad * $com->cantidad,
                                'precio'        => $detalle->precio,
                                'costo'        => $detalle->costo,
                                'usuario'       => $venta->usuario,
                                'tipo'          => $tipo,
                                'total'         => null

                            ]);
                        }
                    }
                }

                $traslados = Traslado::with('detalles')
                ->whereBetween('fecha', array($star, $end))
                ->whereHas('detalles', function($query) use ($com)
                {
                    $query->where('producto_id', $com->producto_id);
                })
                ->get();
                foreach ($traslados as $traslado) {
                    foreach ($traslado->detalles as $detalle) {
                        if($detalle->producto_id == $com->producto_id)
                        {
    
                            $tipo = 'Traslado: ' . $traslado->origen->nombre . ' a ' . $traslado->destino->nombre;

                            $movimientos->push([
                                'fecha'         => $traslado->fecha,
                                'referencia'    => $traslado->id,
                                'entrada'       => null,
                                'salida'        => $detalle->cantidad * $com->cantidad,
                                'precio'        => $detalle->precio,
                                'costo'         => $detalle->costo,
                                'usuario'       => $traslado->usuario,
                                'tipo'          => $tipo,
                                'total'         => null

                            ]);
                        }
                    }
                }
            }

        }

        // return $producto;


        $compras = Compra::with('detalles')
        ->whereBetween('fecha', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();


        foreach ($compras as $compra) {
            foreach ($compra->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'         => $compra->fecha,
                        'referencia'    => $compra->referencia,
                        'categoria'     => $detalle->producto()->first()->categoria,
                        'entrada'       => $detalle->cantidad,
                        'precio'        => $detalle->costo,
                        'costo'        => $detalle->costo,
                        'salida'        => null,
                        'usuario'       =>  $compra->usuario,
                        'tipo'          => 'Compra',
                        'total'         => null
                    ]);
                }
            }
        }

        $devolucioncompras = DevolucionCompra::with('detalles')
        ->whereBetween('fecha', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();


        foreach ($devolucioncompras as $compra) {
            foreach ($compra->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $compra->fecha,
                        'referencia'  => $compra->referencia,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'       => null,
                        'salida'        => $detalle->cantidad,
                        'precio'        => $detalle->precio,
                        'costo'        => $detalle->costo,
                        'usuario'  =>  $compra->usuario,
                        'tipo'      => 'Devolución de Compra',
                        'total'         => null
                    ]);
                }
            }
        }


        $requisiciones = Traslado::with('detalles')
        ->whereBetween('fecha', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();

        foreach ($requisiciones as $requisicion) {
            foreach ($requisicion->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $requisicion->fecha,
                        'referencia'  => $requisicion->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => $detalle->cantidad,
                        'precio'        => $detalle->precio,
                        'costo'        => $detalle->costo,
                        'salida'  => $detalle->cantidad,
                        'usuario'  =>  $requisicion->usuario,
                        'tipo'      => 'Traslado de ' . $requisicion->origen->nombre . ' a ' . $requisicion->destino->nombre,
                        'total'         => null
                    ]);
                }
            }
        }

        $ajustes = Ajuste::where('producto_id', $id)
        ->whereBetween('created_at', array($star, $end))
        ->get();

        foreach ($ajustes as $ajuste) {
            if($ajuste->producto_id == $id)
            {
                $movimientos->push([
                    'fecha'         => $ajuste->created_at->toDateTimeString(),
                    'referencia'    => $ajuste->id,
                    'categoria'     => $ajuste->producto()->first()->categoria,
                    'entrada'       => $ajuste->ajuste > 0 ? abs($ajuste->ajuste) : null,
                    'salida'        => $ajuste->ajuste < 0 ? abs($ajuste->ajuste) : null,
                    'usuario'       =>  $ajuste->usuario,
                    'precio'        => $ajuste->producto()->first()->precio,
                    'costo'         => $ajuste->producto()->first()->costo,
                    'tipo'          => 'Ajuste: ' . $ajuste->ubicacion,
                    'total'         => null
                ]);
            }
        }


        $ventas = Venta::with('detalles')
        ->whereBetween('fecha', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();
        foreach ($ventas as $venta) {
            foreach ($venta->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    if ($venta->estado == 'Anulada') {
                        $tipo = 'Venta: Anulada';
                    }else{
                        $tipo = 'Venta';
                    }
                    $movimientos->push([
                        'fecha'     => $venta->fecha,
                        'referencia'  => $venta->correlativo ? $venta->correlativo : $venta->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => null,
                        'salida'  => $detalle->cantidad,
                        'precio'        => $detalle->precio,
                        'costo'        => $detalle->costo,
                        'usuario'  =>  $venta->usuario,
                        'tipo'      => $tipo,
                        'total'         => null
                    ]);
                }
            }
        }

        $devolucionventas = DevolucionVenta::with('detalles')
        ->whereBetween('fecha', array($star, $end))
        ->whereHas('detalles', function($query) use ($id)
        {
            $query->where('producto_id', $id);
        })
        ->get();
        foreach ($devolucionventas as $venta) {
            foreach ($venta->detalles as $detalle) {
                if($detalle->producto_id == $id)
                {
                    $movimientos->push([
                        'fecha'     => $venta->fecha,
                        'referencia'  => $venta->correlativo ? $venta->correlativo : $venta->id,
                        'categoria' => $detalle->producto()->first()->categoria,
                        'entrada'  => $detalle->cantidad,
                        'salida'  => null,
                        'precio'        => $detalle->precio,
                        'costo'        => $detalle->costo,
                        'usuario'  =>  $venta->usuario,
                        'tipo'      => 'Devolución de Venta',
                        'total'         => null
                    ]);
                }
            }
        }

        // Combos
        $combos = DetalleCombo::where('producto_id', $id)
        ->whereHas('detalle.venta', function($query) use ($star, $end)
        {
            $query->whereBetween('fecha', array($star, $end));
        })
        ->get();

        foreach ($combos as $detalle) {
            $movimientos->push([
                'fecha'         => $detalle->created_at->toDateTimeString(),
                'referencia'    => $detalle->detalle()->first()->venta()->first()->correlativo,
                'entrada'       => null,
                'salida'        => $detalle->cantidad,
                'precio'        => $detalle->precio,
                'costo'        => $detalle->costo,
                'usuario'       => $detalle->detalle()->first()->venta()->first()->usuario,
                'tipo'          => 'Venta:Combo',
                'total'         => null

            ]);
        }

        
        $total = $producto->stock_venta - ($movimientos->sum('entrada') - $movimientos->sum('salida'));
        
        if ($star < '2021-05-01') {
            if (count($movimientos) > 0) {
                $movimientos->push([
                    'fecha'         => '2021-05-01',
                    'referencia'    => null,
                    'entrada'       => $total,
                    'salida'        => null,
                    'precio'        => $producto->precio,
                    'costo'         => $producto->costo,
                    'usuario'       => 'Usuario Admin',
                    'tipo'          => 'Inventario Inicial',
                    'total'         => $total,
                    'entradaValor'  => $total * $producto->precio,
                    'salidaValor'   => null,
                    'totalValor'    => $total * $producto->precio,

                ]);
            }
        }

        $data = $movimientos->sortByDesc('fecha')->values()->all();

        
        for ($i=count($data)-1; $i >= 0; $i--) { 
            if ($i != count($data)-1) {
                $total = $total + $data[$i]['entrada'] - $data[$i]['salida'];
                $data[$i]['total'] = $total;
                $data[$i]['entradaValor'] = $data[$i]['entrada'] ? $data[$i]['entrada'] * $data[$i]['precio'] : null;
                $data[$i]['salidaValor'] = $data[$i]['salida'] ? $data[$i]['salida'] * $data[$i]['precio'] : null;
                $data[$i]['totalValor'] = $total * $data[$i]['precio'];
            }
        }

        $producto->movimientos = $data;

        return Response()->json($producto, 200);
    }

    public function analisis(Request $request) {


            $productos = Producto::when($request->nombre, function($query) use ($request){
                                        return $query->where('nombre', 'like' ,'%' . $request->nombre . '%');
                                    })
                                    ->when($request->categoria_id, function($query) use ($request){
                                        return $query->where('categoria_id', $request->categoria_id);
                                    })

                                    ->get();

            $movimientos = collect();

            $empresa = Empresa::find(1);

            foreach ($productos as $producto) {
                if ($empresa->valor_inventario == 'Promedio') {
                    $producto->costo = $producto->costo_promedio;
                }
                $utilidad = $producto->precio - $producto->costo;
                // $margen = $producto->precio > 0 ? (round($utilidad / $producto->costo, 2) * 100) : null;
                $movimientos->push([
                    'nombre'        => $producto->nombre,
                    'proveedor'     => $producto->proveedor,
                    'precio'        => $producto->precio,
                    'costo'         => $producto->costo,
                    'costo_anterior' => $producto->costo_anterior,
                    'utilidad'      => 0,
                    'margen'        =>  0
                ]);
            }

            return Response()->json($movimientos, 200);
    }

    public function compras(Request $request, $id) {

        $compras = Compra::whereHas('detalles', function($q) use ($id) {
                                    $q->where('producto_id', $id);
                                })
                                ->orderBy('id','desc')->paginate(10);
        

        return Response()->json($compras, 200);

    }

    public function import(Request $request){
        
        $request->validate([
            'file'          => 'required',
        ]);

        $import = new Productos();
        Excel::import($import, $request->file);
        
        return Response()->json($import->getRowCount(), 200);

    }

    public function export(Request $request){

      $productos = new ProductosExport();
      $productos->filter($request);

      return Excel::download($productos, 'productos.xlsx');
    }


}
