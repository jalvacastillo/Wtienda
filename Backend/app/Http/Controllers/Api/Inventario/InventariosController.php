<?php

namespace App\Http\Controllers\Api\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Bodega;
use App\Models\Inventario\Inventario;

class InventariosController extends Controller
{
    

    public function index($bodega) {
       
        $inventarios = Inventario::where('bodega_id', $bodega)->with('producto')->orderBy('created_at','desc')->paginate(10);

        return Response()->json($inventarios, 200);

    }

    public function filterBodega(Request $request) {

            $productos = Inventario::where('bodega_id', 1)->with('producto')
                                ->when($request->categorias_id, function($query) use ($request){
                                    $query->whereHas('producto', function($query) use ($request){
                                        return $query->whereIn('categoria_id', $request->categorias_id);
                                    });
                                })
                                ->when($request->stock_bodega, function($query) use ($request){
                                    return $query->whereRaw('stock <= stock_min');
                                })->paginate(100000);

            return Response()->json($productos, 200);
    }

    public function filterVenta(Request $request) {

            $productos = Inventario::where('bodega_id', 2)->with('producto')
                                ->when($request->categorias_id, function($query) use ($request){
                                    $query->whereHas('producto', function($query) use ($request){
                                        return $query->whereIn('categoria_id', $request->categorias_id);
                                    });
                                })
                                ->when($request->stock_venta, function($query) use ($request){
                                    return $query->whereRaw('stock <= stock_min');
                                })->paginate(100000);

            return Response()->json($productos, 200);
    }


    public function read($id) {
        
        $bodega = Inventario::findOrFail($id);
        return Response()->json($bodega, 200);

    }

    public function store(Request $request) {
    	
        $request->validate([
            'bodega_id'      => 'required|numeric',
            'producto_id'    => 'required|numeric',
            'stock'          => 'required|numeric',
            'stock_min'      => 'required|numeric',
            'stock_max'      => 'required|numeric',
        ]);

        if($request->id)
            $bodegaProducto = Inventario::findOrFail($request->id);
        else
            $bodegaProducto = new Inventario;
        
        $bodegaProducto->fill($request->all());
        $bodegaProducto->save();

        return Response()->json($bodegaProducto, 200);


    }

    public function delete($id) {
        
        $bodega = Inventario::findOrFail($id);
        $bodega->delete();

        return Response()->json($bodega, 201);

    }


    public function bodegaSearch($txt) {
        $productoBodega = Inventario::where('bodega_id', 1)->whereHas('producto', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%')
                        ->orWhere('codigo', 'like' ,'%' . $txt . '%');

                    })->with('producto')->orderBy('stock', 'desc')->paginate(10);


    	return Response()->json($productoBodega, 200);

    }

    public function ventaSearch($txt) {

    	$productoVenta = Inventario::where('bodega_id', 2)->whereHas('producto', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%')
                        ->orWhere('codigo', 'like' ,'%' . $txt . '%');

                    })->with('producto')->orderBy('stock', 'desc')->paginate(10);


        return Response()->json($productoVenta, 200);

    }


}
