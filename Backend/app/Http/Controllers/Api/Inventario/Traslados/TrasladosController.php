<?php

namespace App\Http\Controllers\Api\Inventario\Traslados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Traslados\Traslado;
use App\Models\Inventario\Traslados\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Inventario;
use App\Models\Admin\Empresa;
use App\Models\Inventario\Kardex;
use Illuminate\Support\Facades\DB;

class TrasladosController extends Controller
{
    

    public function index() {
       
        $traslados = Traslado::orderBy('id','desc')->with('origen', 'destino')->paginate(7);

        return Response()->json($traslados, 200);

    }


    public function read($id) {

        $traslado = Traslado::where('id', $id)->with('detalles')->firstOrFail();
        return Response()->json($traslado, 200);

    }
    
    public function search($txt) {

        $traslados = Traslado::whereHas('cliente', function($query) use ($txt) {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');
                    })->paginate(7);

        return Response()->json($traslados, 200);

    }

    public function filter(Request $request) {


        $traslados = Traslado:://whereBetween('created_at', [$star, $end])
                            when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->usuario_id, function($query) use ($request){
                                return $query->where('usuario_id', $request->usuario_id);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->when($request->tipo, function($query) use ($request){
                                return $query->where('origen_id', $request->tipo);
                            })
                            ->with('origen', 'destino')
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($traslados, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'fecha'         => 'required',
            'estado'        => 'required',
            'origen_id'     => 'required|numeric',
            'destino_id'    => 'required|numeric',
            'detalles'     => 'required',
            'usuario_id'    => 'required|numeric'
        ]);

        DB::beginTransaction();
         
        try {

            if($request->id)
                $traslado = Traslado::findOrFail($request->id);
            else
                $traslado = new Traslado;

            $traslado->fill($request->all());
            $traslado->save();

            // Detalles
            foreach ($request->detalles as $i => $value) {
                if (!isset($value['id'])) {
                    $detalle = new Detalle;
                    $value['traslado_id'] = $traslado->id;
                    $detalle->fill($value);
                    $detalle->save();
                }
            }

            // Afectar Inventario
            if ($request->estado == 'Aprobado') {
                foreach ($request->detalles as $i => $det) {
                    // Actualizar inventario
                        $producto = Producto::findOrFail($det['producto_id']);
                        
                        if ($producto->tipo == 'Compuesto') {
                            
                            foreach ($producto->composiciones as $comp) {
                                // Disminuir origen
                                $origen = Inventario::where('bodega_id', $traslado->origen_id)->where('producto_id',$comp->compuesto_id)->first();
                                // Aumentar destino
                                $destino = Inventario::where('bodega_id', $traslado->destino_id)->where('producto_id',$comp->compuesto_id)->first();
                                    // Kardex
                                    if ($origen) {
                                        $origen->stock -= ($det['cantidad'] * $comp->cantidad);
                                        $origen->save();
                                        $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                                        $entradaCantidad =  null;
                                        $salidaCantidad = $det['cantidad'];
                                        Kardex::create([
                                            'fecha'             => date('Y-m-d'),
                                            'producto_id'       => $producto->id,
                                            'bodega_id'         => $origen->bodega_id,
                                            'detalle'           => 'Traslado a ' . $destino->bodega,
                                            'referencia'        => $traslado->id,
                                            'valor_unitario'    => $valor,
                                            'entrada_cantidad'  => $entradaCantidad,
                                            'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                            'salida_cantidad'   => $salidaCantidad,
                                            'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                            'total_cantidad'    => $origen->stock,
                                            'total_valor'       => $origen->stock * $valor,
                                            'usuario_id'        => $request->usuario_id,
                                        ]);
                                    }
                                    // Kardex
                                    if ($destino) {
                                        $destino->stock += ($det['cantidad'] * $comp->cantidad);
                                        $destino->save();
                                        $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                                        $entradaCantidad =  $det['cantidad'];
                                        $salidaCantidad = null;
                                        Kardex::create([
                                            'fecha'             => date('Y-m-d'),
                                            'producto_id'       => $producto->id,
                                            'bodega_id'         => $destino->bodega_id,
                                            'detalle'           => 'Traslado de ' . $origen->bodega,
                                            'referencia'        => $traslado->id,
                                            'valor_unitario'    => $valor,
                                            'entrada_cantidad'  => $entradaCantidad,
                                            'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                            'salida_cantidad'   => $salidaCantidad,
                                            'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                            'total_cantidad'    => $destino->stock,
                                            'total_valor'       => $destino->stock * $valor,
                                            'usuario_id'        => $request->usuario_id,
                                        ]);
                                    }
                            }
                            
                        }

                        // Disminuir origen
                        $origen = Inventario::where('bodega_id', $traslado->origen_id)->where('producto_id',$producto->id)->first();
                        // Aumentar destino
                        $destino = Inventario::where('bodega_id', $traslado->destino_id)->where('producto_id',$producto->id)->first();
                        
                        // Kardex
                        if ($origen) {
                            $origen->stock -= $det['cantidad'];
                            $origen->save();
                            $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                            $entradaCantidad =  null;
                            $salidaCantidad = $det['cantidad'];
                            Kardex::create([
                                'fecha'             => date('Y-m-d'),
                                'producto_id'       => $producto->id,
                                'bodega_id'         => $origen->bodega_id,
                                'detalle'           => 'Traslado a ' . $destino->bodega,
                                'referencia'        => $traslado->id,
                                'valor_unitario'    => $valor,
                                'entrada_cantidad'  => $entradaCantidad,
                                'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                'salida_cantidad'   => $salidaCantidad,
                                'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                'total_cantidad'    => $origen->stock,
                                'total_valor'       => $origen->stock * $valor,
                                'usuario_id'        => $request->usuario_id,
                            ]);
                        }


                        // Kardex
                        if ($destino) {
                            $destino->stock += $det['cantidad'];
                            $destino->save();
                            $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                            $entradaCantidad =  $det['cantidad'];
                            $salidaCantidad = null;
                            Kardex::create([
                                'fecha'             => date('Y-m-d'),
                                'producto_id'       => $producto->id,
                                'bodega_id'         => $destino->bodega_id,
                                'detalle'           => 'Traslado de ' . $origen->bodega,
                                'referencia'        => $traslado->id,
                                'valor_unitario'    => $valor,
                                'entrada_cantidad'  => $entradaCantidad,
                                'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                'salida_cantidad'   => $salidaCantidad,
                                'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                'total_cantidad'    => $destino->stock,
                                'total_valor'       => $destino->stock * $valor,
                                'usuario_id'        => $request->usuario_id,
                            ]);
                        }
                
                }
            }

        DB::commit();
        return Response()->json($traslado, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json($e->getMessage(), 422);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json($e->getMessage(), 422);
        }



        return Response()->json($traslado, 200);

    }

    public function delete($id)
    {
        $traslado = Traslado::findOrFail($id);
        $traslado->delete();

        return Response()->json($traslado, 201);

    }

    public function generarDoc($id) {

        $traslado = Traslado::where('id', $id)->with('detalles', 'origen', 'destino')->firstOrFail();
        $empresa = Empresa::find(1);

        $reportes = \PDF::loadView('reportes.traslado', compact('traslado', 'empresa'));
        return $reportes->stream();

    }


    public function venta(){

        $productos = Producto::where('categoria_id', '!=', 1)->with('bodegas')->whereHas('bodegas', function($q){
                                    $q->where('bodega_id', 2)->whereRaw('stock < stock_max');
                                })->get();
        
        $traslados = collect();

        foreach ($productos as $producto) {

            $Bventa = $producto->bodegas()->where('bodega_id', 2)->first();
            $bodega = $producto->bodegas()->where('bodega_id', 1)->first();
            $cantidad = $Bventa->stock_max - $Bventa->stock;

            if ($cantidad > $bodega->stock) {
                $cantidad = $bodega->stock;
                $disponible = false;
            }else{
                $disponible = true;
            }

            $traslados->push([
                'producto_id'       => $producto->id,
                'disponible'        => $disponible,
                'existencia'        => $bodega->stock,
                'stock'             => $Bventa->stock,
                'stock_min'         => $Bventa->stock_min,
                'stock_max'         => $Bventa->stock_max,
                'cantidad'          => $cantidad,
                'producto'          => $producto->nombre,
                'medida'            => $producto->medida,
                'categoria'         => $producto->categoria()->first()->nombre,
            ]);
        }


        return Response()->json($traslados, 201);

    }

    public function bodega(){

        $productos = Producto::where('categoria_id', '!=', 1)->with('bodegas')->whereHas('bodegas', function($q){
                                    $q->where('bodega_id', 1)->whereRaw('stock < stock_max');
                                })->get();
        
        $traslados = collect();

        foreach ($productos as $producto) {

            $bodega = $producto->bodegas()->where('bodega_id', 1)->first();
            $Bventa = $producto->bodegas()->where('bodega_id', 2)->first();

            $cantidad = $bodega->stock_max - $bodega->stock;
            if ($cantidad > $Bventa->stock) {
                $cantidad = $Bventa->stock;
                $disponible = false;
            }else{
                $disponible = true;
            }


            $traslados->push([
                'producto_id'       => $producto->id,
                'disponible'        => $disponible,
                'proveedor'         => $producto->proveedor_id,
                'existencia'        => $Bventa->stock,
                'stock'             => $bodega->stock,
                'stock_min'         => $bodega->stock_min,
                'stock_max'         => $bodega->stock_max,
                'cantidad'          => $cantidad,
                'producto'          => $producto->nombre,
                'medida'            => $producto->medida,
                'categoria'         => $producto->categoria()->first()->nombre,
            ]);
        }


        return Response()->json($traslados, 201);

    }

    public function bodegaFiltrar(Request $request){

        $productos = Producto::where('categoria_id', '!=', 1)->with('bodegas', 'categoria')
                                ->when($request->categoria_id, function($query) use ($request){
                                    return $query->whereHas('categoria', function($query) use ($request){
                                        return $query->where('categoria_id', $request->categoria_id);
                                    });
                                })
                                ->whereHas('bodegas', function($q){
                                    $q->where('bodega_id', 1)->whereRaw('stock < stock_max');
                                })->get();
        
        $traslados = collect();

        foreach ($productos as $producto) {

            $bodega = $producto->bodegas()->where('bodega_id', 1)->first();

            if ($request->proveedor_id) {

                if ($request->proveedor_id == $producto->proveedor_id) {
                    $traslados->push([
                        'producto_id'       => $producto->id,
                        'stock'             => $bodega->stock,
                        'stock_min'         => $bodega->stock_min,
                        'stock_max'         => $bodega->stock_max,
                        'cantidad'          => $bodega->stock_max - $bodega->stock,
                        'producto'          => $producto->nombre,
                        'medida'            => $producto->medida,
                        'categoria'         => $producto->categoria()->first()->nombre,
                    ]);
                }
                
            }else{
                $traslados->push([
                    'producto_id'       => $producto->id,
                    'stock'             => $bodega->stock,
                    'stock_min'         => $bodega->stock_min,
                    'stock_max'         => $bodega->stock_max,
                    'cantidad'          => $bodega->stock_max - $bodega->stock,
                    'producto'          => $producto->nombre,
                    'medida'            => $producto->medida,
                    'categoria'         => $producto->categoria()->first()->nombre,
                ]);
            }
        }


        return Response()->json($traslados, 201);

    }



}
