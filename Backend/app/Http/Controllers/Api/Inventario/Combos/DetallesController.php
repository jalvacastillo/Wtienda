<?php

namespace App\Http\Controllers\Api\Inventario\Combos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventario\Combos\Detalle;

class DetallesController extends Controller
{
    

    public function index($id) {
       
        $detalles = Detalle::where('combo_id', $id)->orderBy('id','desc')->paginate(7);

        return Response()->json($detalles, 200);

    }

    public function opciones($id) {
       
        $opciones = Detalle::where('id', $id)->with('opciones')->first();

        return Response()->json($opciones, 200);

    }


    public function read($id) {

        $detalle = Detalle::findOrFail($id);
        return Response()->json($detalle, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'combo_id'    => 'required|numeric',
            'cantidad'    => 'required|numeric',
            'precio'    => 'required|numeric',
            'producto_id'    => 'required|numeric'
        ]);

        if($request->id)
            $detalle = Detalle::findOrFail($request->id);
        else
            $detalle = new Detalle;

        $detalle->fill($request->all());
        $detalle->save();        

        return Response()->json($detalle, 200);

    }

    public function delete($id)
    {
        $detalle = Detalle::findOrFail($id);
        $detalle->delete();

        return Response()->json($detalle, 201);

    }


    public function search($txt) {

        $detalles = Detalle::whereHas('cliente', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');

                    })->paginate(7);

        return Response()->json($detalles, 200);

    }


}
