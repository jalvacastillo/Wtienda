<?php

namespace App\Http\Controllers\Api\Inventario\Combos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventario\Combos\Combo;
use App\Models\Ventas\Detalle;

class CombosController extends Controller
{
    

    public function index() {
       
        $combos = Combo::orderBy('inicio','desc')->paginate(7);

        return Response()->json($combos, 200);

    }
    
    public function all() {
       
        $combos = Combo::with('detalles', 'detalles.opciones')->orderBy('nombre','asc')->get();
        $combos = $combos->where('activo', true)->values();

        return Response()->json($combos, 200);

    }


    public function read($id) {

        $combo = Combo::findOrFail($id);
        return Response()->json($combo, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre'    => 'required|max:255',
            // 'inicio'    => 'required|numeric',
            // 'fin'    => 'required|numeric'
        ]);

        if($request->id)
            $combo = Combo::findOrFail($request->id);
        else
            $combo = new Combo;

        $combo->fill($request->all());
        $combo->save();        

        return Response()->json($combo, 200);

    }

    public function delete($id)
    {
        $combo = Combo::findOrFail($id);
        $combo->delete();

        return Response()->json($combo, 201);

    }


    public function search($txt) {

        $combos = Combo::where('nombre', 'like' ,'%' . $txt . '%')->paginate(7);

        return Response()->json($combos, 200);

    }

    public function detalles($id) {

        $combo = Combo::where('id', $id)->with('detalles')->first();

        return Response()->json($combo, 200);


    }

    public function historial(Request $request) {

            $request->inicio = $request->inicio . ' ' . $request->inicio_hora . ':00';
            $request->fin = $request->fin . ' ' . $request->fin_hora . ':59';


            $ventas = Detalle::where('escombo', true)
                            ->whereHas('venta', function($query) use ($request){
                                $query->where('estado', 'Cobrada')
                                ->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->nombre, function($query) use ($request){
                                return $query->whereHas('combo', function($q) use ($request){
                                    $q->where('nombre', 'like' ,'%' . $request->nombre . '%');
                                });
                            })
                            // ->when($request->categoria_id, function($query) use ($request){
                            //     return $query->whereHas('producto', function($q) use ($request){
                            //         $q->where('categoria_id', $request->categoria_id );
                            //     });
                            // })
                            ->get()
                            ->groupBy('producto_id');

            
            $movimientos = collect();

            foreach ($ventas as $venta) {
                $ids = '';
                foreach ($venta as $item) {
                    $ids .= $item->venta->id . ' '; 
                }
                $movimientos->push([
                    'ids'           => $ids,
                    'fecha'         => $venta[0]->venta->fecha,
                    'producto'      => $venta[0]->producto_nombre,
                    'medida'        => $venta[0]->medida,
                    'cantidad'      => $venta->sum('cantidad'),
                    'total'         => $venta->sum('total'),
                    'costo'         => $venta->sum('subcosto'),
                    'utilidad'      => $venta->sum('total') - $venta->sum('subcosto'),
                    'margen'        => $venta->sum('total') > 0 ? round((($venta->sum('total') - ($venta->sum('subcosto'))) / $venta->sum('total') * 100), 2) : null
                ]);
            }

            return Response()->json($movimientos, 200);
        }



}
