<?php

namespace App\Http\Controllers\Api\Inventario\Combos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Inventario\Combos\Opcion;

class OpcionesController extends Controller
{
    

    public function index($id) {
       
        $opciones = Opcion::where('combo_id', $id)->orderBy('id','desc')->paginate(7);

        return Response()->json($opciones, 200);

    }


    public function read($id) {

        $opcion = Opcion::findOrFail($id);
        return Response()->json($opcion, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'combo_detalle_id'    => 'required|numeric',
            'producto_id'    => 'required|numeric'
        ]);

        if($request->id)
            $opcion = Opcion::findOrFail($request->id);
        else
            $opcion = new Opcion;       


        $opcion->fill($request->all());
        $opcion->save();        

        return Response()->json($opcion, 200);

    }

    public function delete($id)
    {
        $opcion = Opcion::findOrFail($id);
        $opcion->delete();

        return Response()->json($opcion, 201);

    }


    public function search($txt) {

        $opciones = Opcion::whereHas('cliente', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');

                    })->paginate(7);

        return Response()->json($opciones, 200);

    }


}
