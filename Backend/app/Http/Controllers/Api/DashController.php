<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use stdClass;

use App\Models\Admin\Empresa;
use App\Models\Admin\Caja;
use App\Models\Ventas\Venta;
use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\Entrada;
use App\Models\Ventas\Detalle as VentaDetalle;
use App\Models\Compras\Compra;
use App\Models\Compras\Salida;
use App\Models\Registros\Anticipo;
use App\Models\Admin\Corte;

use App\Models\Inventario\Producto;
use App\Models\Registros\Cliente;

use App\Models\User;

class DashController extends Controller
{

    public function admin(Request $request) {
        $datos = new stdClass();

        $datos->inicio = $request->inicio;
        $datos->fin = $request->fin;

        // Ventas
        $ventas                     = Venta::where('estado', '!=', 'Anulada')->whereBetween('fecha', [$datos->inicio, $datos->fin])->get();
        $datos->ventas              = $ventas->where('estado','Cobrada')->sum('total');
        $datos->cuentas_cobrar      = $ventas->where('estado', 'Pendiente')->sum('total');

        // Cajas
        $cajas = Caja::with('ventasDia')->get();
        
        foreach ($cajas as $caja) {
            $caja->estado = $caja->corte ? $caja->corte->estado : 'Sin Cortes';
            $caja->ventas_suma = $caja->ventasDia->sum('total');
            $caja->ventas_cantidad = $caja->ventasDia->count();
        }

        $datos->cajas = $cajas;

        // Compras
        $compras                    = Compra::whereBetween('fecha', [$datos->inicio, $datos->fin])->get();
        $datos->compras             = $compras->where('estado','Pagada')->sum('total');
        $datos->cuentas_pagar       = $compras->where('estado', 'Pendiente')->sum('total');

        
        $datos->pago_anticipado     = Anticipo::whereBetween('fecha', [$datos->inicio, $datos->fin])->where('tipo', 'Abono')->sum('total');
        
        // Pagos y cobros pendientes
        $datos->pagos               = Compra::where('estado', 'Pendiente')->orderBy('fecha_pago','desc')->take(3)->get();
        $datos->cobros              = Venta::where('estado', 'Pendiente')->orderBy('id','asc')->take(3)->get();

        return Response()->json($datos, 200);

    }

    public function galonaje(Request $request) {
        $datos = new stdClass();

        $datos->inicio = $request->inicio;
        $datos->fin = $request->fin;

        // Galones del mes
        $galones_mes                = VentaDetalle::whereIn('producto_id', [1,2,3])->where('escombo', false)
                                        ->whereHas('venta', function($q){
                                            $q->where('estado', '!=', 'Anulada')
                                            ->whereYear('fecha', '=', date('Y'))
                                            ->whereMonth('fecha', date('m'));
                                        })
                                        ->get();

        $datos->galones_mes_super   = $galones_mes->where('producto_id', 1)->sum('cantidad');
        $datos->galones_mes_regular = $galones_mes->where('producto_id', 2)->sum('cantidad');
        $datos->galones_mes_diesel  = $galones_mes->where('producto_id', 3)->sum('cantidad');
        $datos->galones_mes         = $galones_mes->sum('cantidad');

        // Galones del día
        $galones_dia                = VentaDetalle::whereIn('producto_id', [1,2,3])->where('escombo', false)
                                            ->whereHas('venta', function($q) use ($datos){
                                                $q->where('estado', '!=', 'Anulada')
                                                ->whereBetween('fecha', [$datos->inicio, $datos->fin]);
                                            })
                                            ->get();

        $datos->galones_dia_super   = $galones_dia->where('producto_id', 1)->sum('cantidad');
        $datos->galones_dia_regular = $galones_dia->where('producto_id', 2)->sum('cantidad');
        $datos->galones_dia_diesel  = $galones_dia->where('producto_id', 3)->sum('cantidad');
        $datos->galones_dia         = $galones_dia->sum('cantidad');

        $datos->meta_galones = 140000;

        return Response()->json($datos, 200);

    }

    public function caja(){

        $datos = new stdClass();
        $usuario = User::findOrFail($id);

        $datos->caja    = Caja::findOrFail($usuario->caja_id);
        
        // Sino hay corte se muestran los datos del dia.
        if ($datos->caja->corte) {
            $ventas     = Venta::where('usuario_id', $id)->where('estado', '!=', 'Anulada')->where('created_at', '>=', $datos->caja->corte->inicio)->get();
            $devoluciones     = DevolucionVenta::where('usuario_id', $id)->where('created_at', '>=', $datos->caja->corte->inicio)->get();
            $datos->enCaja              = $ventas->sum('total') + $datos->caja->corte->total;
        } else {
            $ventas     = Venta::where('usuario_id', $id)->where('estado', '!=', 'Anulada')->where('created_at', '>=', Carbon::today())->get();
            $devoluciones = DevolucionVenta::where('usuario_id', $id)->where('created_at', '>=', Carbon::today())->get();
            $datos->enCaja              = $ventas->sum('total');
        }
        
        // Se saca el total de venta
        $datos->ventas              = $ventas->sum('total');
        $datos->devoluciones        = $devoluciones->sum('total');
        // Por Forma de Pago
        $datos->ventas_efectivo     = $ventas->where('forma_de_pago','Efectivo')->sum('total');
        $datos->ventas_tarjeta      = $ventas->where('forma_de_pago','Tarjeta')->sum('total');
        $datos->ventas_vale         = $ventas->where('forma_de_pago','Vale')->sum('total');
        $datos->ventas_cheque       = $ventas->where('forma_de_pago','Cheque')->sum('total');
        $datos->ventas_versatec     = $ventas->where('forma_de_pago','Versatec')->sum('total');

        return Response()->json($datos, 200);
    }

    public function estadistica(Request $request){
        $datos = new stdClass();

        $star = $request->fecha_ini . ' ' . $request->hora_ini;
        $end = $request->fecha_fin . ' ' . $request->hora_fin;
        
        $datos->totales         = VentaDetalle::whereBetween('created_at', [$star, $end])
                                                    ->where('escombo', false)
                                                    ->whereNotIn('producto_id', [1,2,3])
                                                    ->groupBy('producto_id')
                                                    ->selectRaw('sum(cantidad) AS vt, producto_id')
                                                    ->orderBy('vt', 'desc')
                                                    ->pluck('vt')
                                                    ->take(5)
                                                    ->toArray();
        $datos->productos       = VentaDetalle::whereBetween('created_at', [$star, $end])
                                                    ->where('escombo', false)
                                                    ->whereNotIn('producto_id', [1,2,3])
                                                    ->groupBy('producto_id')
                                                    ->with('producto')
                                                    ->selectRaw('sum(cantidad) AS vt, producto_id, (select nombre from productos where producto_id = id) as nombre')
                                                    ->orderBy('vt', 'desc')
                                                    ->pluck('nombre')
                                                    ->take(5)
                                                    ->toArray();

        $datos->ctotales         = VentaDetalle::whereBetween('created_at', [$star, $end])
                                                    ->where('escombo', true)
                                                    ->groupBy('producto_id')
                                                    ->selectRaw('sum(cantidad) AS vt, producto_id')
                                                    ->orderBy('vt', 'desc')
                                                    ->pluck('vt')
                                                    ->take(5)
                                                    ->toArray();
        $datos->cproductos       = VentaDetalle::whereBetween('created_at', [$star, $end])
                                                    ->where('escombo', true)
                                                    ->groupBy('producto_id')
                                                    ->with('producto')
                                                    ->selectRaw('sum(cantidad) AS vt, producto_id, (select nombre from producto_combos where producto_id = id) as nombre')
                                                    ->orderBy('vt', 'desc')
                                                    ->pluck('nombre')
                                                    ->take(5)
                                                    ->toArray();
        return Response()->json($datos, 200);
    }

    public function telefoniaDatos(Request $request) {
       
       $datos = new stdClass();

       $usuario = User::findOrFail($request->usuario_id);

       if ($usuario->tipo == 'Empleado') {

           $datos->caja    = Caja::findOrFail($usuario->caja_id);
           
           // Sino hay corte se muestran los datos del dia.
           if ($datos->caja->corte) {
                $telefonias     = VentaDetalle::whereIn('producto_id', [1,2,3, 4])
                                        ->whereHas('venta', function($q) use ($usuario, $datos) { 
                                            $q->where('usuario_id', $usuario->id)
                                                ->where('fecha', '>=', $datos->caja->corte->inicio);
                                            })->get();
            }else{

                $telefonias     = VentaDetalle::whereIn('producto_id', [1,2,3, 4])
                                    ->whereHas('venta', function($q) use ($usuario, $datos) {
                                        $q->where('usuario_id', $usuario->id)
                                            ->where('fecha', '>=', Carbon::today());
                                        })->get();
            }

        }else{

            if ($request->empleado_id > 0) {
                $telefonias     = VentaDetalle::whereIn('producto_id', [1,2,3,4])
                                            ->whereHas('venta', function($q) use ($request) { 
                                                $q->where('usuario_id', $request->empleado_id)
                                                    ->whereBetween('fecha', [$request->inicio, $request->fin]);
                                                })->get();
            }else{
                $telefonias     = VentaDetalle::whereIn('producto_id', [1,2,3, 4])
                                            ->whereHas('venta', function($q) use ($request) {
                                                $q->whereBetween('fecha', [$request->inicio, $request->fin]);
                                            })->get();
            }

        }

        $datos->tigo           = $telefonias->where('producto_id', 1)->sum('total');
        $datos->tigo_recarga   = $telefonias->where('producto_id', 1)->where('detalle', 'Recarga')->sum('total');
        $datos->tigo_paquete   = $telefonias->where('producto_id', 1)->where('detalle', 'Paquete')->sum('total');
        $datos->tigo_recarga_tipos     = $telefonias->where('producto_id', 1)->where('detalle', 'Recarga')->groupBy('precio')->values()->all();
        $datos->tigo_paquete_tipos     = $telefonias->where('producto_id', 1)->where('detalle', 'Paquete')->groupBy('precio')->values()->all();
        
        $datos->claro           = $telefonias->where('producto_id', 2)->sum('total');
        $datos->claro_recarga   = $telefonias->where('producto_id', 2)->where('detalle', 'Recarga')->sum('total');
        $datos->claro_paquete   = $telefonias->where('producto_id', 2)->where('detalle', 'Paquete')->sum('total');
        $datos->claro_recarga_tipos     = $telefonias->where('producto_id', 2)->where('detalle', 'Recarga')->groupBy('precio')->values()->all();
        $datos->claro_paquete_tipos     = $telefonias->where('producto_id', 2)->where('detalle', 'Paquete')->groupBy('precio')->values()->all();
        
        $datos->movistar           = $telefonias->where('producto_id', 3)->sum('total');
        $datos->movistar_recarga   = $telefonias->where('producto_id', 3)->where('detalle', 'Recarga')->sum('total');
        $datos->movistar_paquete   = $telefonias->where('producto_id', 3)->where('detalle', 'Paquete')->sum('total');
        $datos->movistar_recarga_tipos   = $telefonias->where('producto_id', 3)->where('detalle', 'Recarga')->groupBy('precio')->values()->all();
        $datos->movistar_paquete_tipos   = $telefonias->where('producto_id', 3)->where('detalle', 'Paquete')->groupBy('precio')->values()->all();

        $datos->digicel           = $telefonias->where('producto_id', 4)->sum('total');
        $datos->digicel_recarga   = $telefonias->where('producto_id', 4)->where('detalle', 'Recarga')->sum('total');
        $datos->digicel_paquete   = $telefonias->where('producto_id', 4)->where('detalle', 'Paquete')->sum('total');
        $datos->digicel_recarga_tipos     = $telefonias->where('producto_id', 4)->where('detalle', 'Recarga')->groupBy('precio')->values()->all();
        $datos->digicel_paquete_tipos     = $telefonias->where('producto_id', 4)->where('detalle', 'Paquete')->groupBy('precio')->values()->all();

        return Response()->json($datos, 200);

    }

}
