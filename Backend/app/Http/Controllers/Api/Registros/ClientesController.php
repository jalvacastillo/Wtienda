<?php

namespace App\Http\Controllers\Api\Registros;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Registros\Cliente;
use App\Models\Registros\Anticipo;
use App\Models\Ventas\Venta;

class ClientesController extends Controller
{
    

    public function index() {
       
        $clientes = Cliente::where('id','!=', 1)
                    ->orderBy('created_at','desc')
                    ->paginate(10);

        foreach ($clientes as $cliente) {
            $anticipos = $cliente->anticipos();
            $cliente->num_ventas = $cliente->ventas->count();            
            $cliente->num_ventas_pendientes = $cliente->ventasPendientes->count();
            $cliente->pago_pendiente = $cliente->ventasPendientes->sum('total');

            $abonos = $anticipos->where('tipo', 'Abono')->sum('total');
            $cargos = $anticipos->where('tipo', 'Cargo')->sum('total');
            $cliente->pago_anticipado = $abonos - $cargos;
        }

        return Response()->json($clientes, 200);

    }

    public function list($txt) {

        $clientes = Cliente::where('nombre', 'like' ,'%' . $txt . '%')
                            ->orWhere('registro', 'like' , $txt . '%')
                            ->orWhereRaw('REPLACE(registro, "-", "") like "'.$txt.'"')
                            ->orderBy('registro','asc')->paginate(10);
        foreach ($clientes as $cliente) {
            $anticipos = $cliente->anticipos();
            $cliente->num_ventas = $cliente->ventas->count();
            $cliente->num_ventas_pendientes = $cliente->ventasPendientes->count();
            
            $cliente->pago_pendiente = $cliente->ventasPendientes->sum('total');
            $abonos = $anticipos->where('tipo', 'Abono')->sum('total');
            $cargos = $anticipos->where('tipo', 'Cargo')->sum('total');
            $cliente->pago_anticipado = $abonos - $cargos;
        }
        
        return Response()->json($clientes, 200);

    }

    public function search($txt) {

        $clientes = Cliente::where('nombre', 'like' ,'%' . $txt . '%')
                            ->orWhere('registro', 'like' , $txt . '%')
                            ->orWhereRaw('REPLACE(registro, "-", "") like "'.$txt.'"')
                            ->orderBy('registro','asc')->paginate(10);
        return Response()->json($clientes, 200);

    }

    public function filter(Request $request) {

        if ($request->pagos != '') {
            if ($request->pagos == 1) {
                $clientes = Cliente::where('id','!=', 1)
                                    ->wherehas('ventas', function($q){
                                        $q->where('estado', 'Pendiente');
                                    })->orderBy('id','desc')->paginate(1000);
            }else{
                $clientes = Cliente::where('id','!=', 1)
                                    ->whereDoesntHave('ventas', function($q){
                                        $q->where('estado', 'Pendiente');
                                    })->orderBy('id','desc')->paginate(1000);
            }
        }

        foreach ($clientes as $cliente) {
            $anticipos = $cliente->anticipos();
            $cliente->num_ventas = $cliente->ventas->count();
            $cliente->num_ventas_pendientes = $cliente->ventasPendientes->count();
            
            $cliente->pago_pendiente = $cliente->ventasPendientes->sum('total');
            $abonos = $anticipos->where('tipo', 'Abono')->sum('total');
            $cargos = $anticipos->where('tipo', 'Cargo')->sum('total');
            $cliente->pago_anticipado = $abonos - $cargos;
        }

        return Response()->json($clientes, 200);

    }

    public function read($id) {

        $cliente = Cliente::findOrFail($id);
        return Response()->json($cliente, 200);

    }

    public function store(Request $request)
    {

        $request->validate([
            'nombre'    => 'required|max:255',
            'registro'  => 'sometimes|unique:clientes,registro,'. $request->id,
        ]);

        if($request->id)
            $cliente = Cliente::findOrFail($request->id);
        else
            $cliente = new Cliente;
        
        $cliente->fill($request->all());
        $cliente->save();

        return Response()->json($cliente, 200);

    }

    public function delete($id)
    {
        $cliente = Cliente::findOrFail($id);
        if ($cliente->ventas()->count() == 0) {
            $cliente->delete();
        }

        return Response()->json($cliente, 201);

    }

    public function ventas($id) {

        $ventas = Venta::where('cliente_id', $id)
                        ->where('estado', '!=', 'Anulada')
                        ->orderBy('id', 'desc')
                        ->paginate(10);
        return Response()->json($ventas, 200);

    }

    public function ventasFilter(Request $request) {

        if ($request->estado == 'Anulada') {
            $ventas = Venta::where('cliente_id', $request->id)
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->forma_de_pago, function($query) use ($request){
                            return $query->where('forma_de_pago', $request->forma_de_pago);
                        })
                        ->orderBy('id','desc')->paginate(100000);
        }else{

            $ventas = Venta::where('cliente_id', $request->id)
                        ->where('estado', '!=', 'Anulada')
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->forma_de_pago, function($query) use ($request){
                            return $query->where('forma_de_pago', $request->forma_de_pago);
                        })
                        ->orderBy('id','desc')->paginate(100000);
        }

        return Response()->json($ventas, 200);

    }

    public function valesPendientes($id) {

        $ventas = Venta::where('cliente_id', $id)->where('estado', 'Pendiente')
                        ->where('forma_de_pago', 'Vale')
                        ->orderBy('id', 'desc')->get();
        return Response()->json($ventas, 200);

    }

    public function anticipos($id){

        $anticipos = Anticipo::where('cliente_id', $id)
                    ->orderBy('id','desc')->paginate(10);
        return Response()->json($anticipos, 200);

    }


    public function cxc() {
       
        $clientes = Cliente::where('id','!=', 1)
                        ->whereRaw('clientes.id in (select cliente_id from ventas where estado = ?)', ['Pendiente'])
                        ->paginate(10);

        foreach ($clientes as $cliente) {
            $cliente->num_ventas_pendientes = $cliente->ventasPendientes->count();
            $cliente->pago_pendiente = $cliente->ventasPendientes->sum('total');
        }

        return Response()->json($clientes, 200);

    }

    public function cxcBuscar($txt) {
       
        $clientes = Cliente::where('id','!=', 1)->where('nombre', 'like' ,'%' . $txt . '%')
                        ->orWhere('registro', 'like' , $txt . '%')
                        ->orWhereRaw('REPLACE(registro, "-", "") like "'.$txt.'"')
                        ->whereRaw('clientes.id in (select cliente_id from ventas where estado = ?)', ['Pendiente'])
                        ->paginate(10);

        return Response()->json($clientes, 200);

    }




}