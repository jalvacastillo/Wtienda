<?php

namespace App\Http\Controllers\Api\Registros;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Registros\Anticipo;

class AnticiposController extends Controller
{
    

    public function index() {
       
        $anticipos = Anticipo::orderBy('id','desc')->paginate(10);

        return Response()->json($anticipos, 200);

    }

    public function search($txt) {

        $anticipos = Anticipo::whereHas('cliente', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');

                    })->paginate(10);
        return Response()->json($anticipos, 200);

    }

    public function filter(Request $request) {

        $anticipos = Anticipo::
                            when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->tipo, function($query) use ($request){
                                return $query->where('tipo', $request->tipo);
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($anticipos, 200);

    }

    public function read($id) {

        $anticipo = Anticipo::where('id', $id)->with('cliente')->first();
        return Response()->json($anticipo, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'fecha'    => 'required',
            'total'    => 'required',
            'tipo'    => 'required',
            'cliente_id'    => 'required',
            'usuario_id'    => 'required'
        ]);
        
        if($request->id)
            $anticipo = Anticipo::findOrFail($request->id);
        else
            $anticipo = new Anticipo;
        
        $anticipo->fill($request->all());
        $anticipo->save();

        return Response()->json($anticipo, 200);

    }

    public function delete($id)
    {
        $anticipo = Anticipo::findOrFail($id);
        $anticipo->delete();

        return Response()->json($anticipo, 201);

    }


}
