<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User as Usuario;

class UsuariosController extends Controller
{
    

    public function index() {
       
        $usuarios = Usuario::orderBy('id','desc')->paginate(15);

        return Response()->json($usuarios, 200);

    }


    public function read($id) {
        
        // $usuario = Usuario::where('id', $id)->with('metas')->firstOrFail();
        $usuario = Usuario::where('id', $id)->firstOrFail();

        $ventas = $usuario->ventas()->selectRaw('*, MONTH(fecha) as mes')->orderBy('mes')->get();

        $data = $ventas->groupby('mes')->map(function ($row) {
                            return $row->sum('total');
                        });

        $mes = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','diciembre'];

        $items = collect();

        foreach ($data as $key => $value) {
            $items->push([
                'label' => $mes[$key - 1 ],
                'data' => $value,
            ]);
        }

        $usuario->labels = $items->pluck('label');
        $usuario->data = [['label' => 'ventas', 'data' => $items->pluck('data')]];

        $usuario->cortes = $ventas = $usuario->cortes()->take(5)->latest()->get();

        return Response()->json($usuario, 200);
    }

    public function filter($campo, $valor) {
        
        $usuario = Usuario::where($campo, $valor)->paginate(15);

        return Response()->json($usuario, 200);
    }

    public function search($txt) {

        $usuarios = Usuario::where('name', 'like' ,'%' . $txt . '%')->paginate(15);
        return Response()->json($usuarios, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required|max:255',
            'username'  => 'required|unique:users,username,'.$request->id
        ]);

        if ($request->password) {
            $request->validate([
                'password' => 'required|string|min:3|confirmed'
            ]);
        }

        if($request->id)
            $usuario = Usuario::findOrFail($request->id);
        else
            $usuario = new Usuario;


        if ($request->password) {
            $request['password'] = \Hash::make($request->password);
        }
       
        if (!$request->id) {
            $request['password'] = \Hash::make('emple');
        }
        
        $usuario->fill($request->all());
        $usuario->save();

        return Response()->json($usuario, 200);


    }

    public function delete($id)
    {
       
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();

        return Response()->json($usuario, 201);

    }

    public function caja($id)
    {
       
        $usuarios = Usuario::where('caja_id', $id)->get();

        return Response()->json($usuarios, 200);

    }

    public function validar(Request $request)
    {

       
        $supervisor = Usuario::where('codigo', $request->codigo)->first();
        
        if (!$supervisor)
            return Response()->json(['error' => ['Datos incorrectos'], 'code' => 422], 422);

        return Response()->json($supervisor, 200); 

    }



}
