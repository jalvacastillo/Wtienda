<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Mensaje;

class MensajesController extends Controller
{
    

    public function index() {

        $mensajes = Mensaje::orderBy('id','desc')->paginate(7);

        return Response()->json($mensajes, 200);            

    }

    public function search($txt) {

        $mensajes = Mensaje::where('descripcion', 'like' ,'%' . $txt . '%')->paginate(7);

        return Response()->json($mensajes, 200);

    }

    public function filter($filtro, $valor) {
       
        $mensajes = Mensaje::where($filtro, $valor)->orderBy('id','desc')->get();

        return Response()->json($mensajes, 200);

    }


    public function read($id) {

        $mensaje = Mensaje::find($id);
        return Response()->json($mensaje, 200);

    }


    public function store(Request $request)
    {

        $request->validate([
            'descripcion'    => 'required',
            'estado'        => 'required',
            'usuario_id'    => 'required|numeric',
        ]);

        if($request->id)
            $mensaje = Mensaje::findOrFail($request->id);
        else
            $mensaje = new Mensaje;
        
        $mensaje->fill($request->all());
        $mensaje->save();

        return Response()->json($mensaje, 200);

    }

    public function delete($id)
    {
        
        $mensaje = Mensaje::find($id);
        $mensaje->delete();

        return Response()->json($mensaje, 201);


    }

}
