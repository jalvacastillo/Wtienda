<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Tanque;

class TanquesController extends Controller
{
    

    public function index() {
       
        $tanques = Tanque::all();

        return Response()->json($tanques, 200);

    }


    public function read($id) {
        
        $tanque = Tanque::findOrFail($id);
        return Response()->json($tanque, 200);

    }


    public function store(Request $request) {
    	
        $request->validate([
            'nombre'        => 'required|max:255',
            'producto_id'   => 'required',
            'stock'         => 'required',
            'stock_min'     => 'required',
            'stock_max'     => 'required'
        ]);

        if($request->id)
            $tanque = Tanque::findOrFail($request->id);
        else
            $tanque = new Tanque;
        
        $tanque->fill($request->all());
        $tanque->save();

        return Response()->json($tanque, 200);


    }

    public function ajustes($id) {
        
        $tanque = Tanque::where('id', $id)->with('ajustes')->firstOrFail();

        $data = collect();
        $fechas = collect();

        foreach ($tanque->ajustes->take(12) as $ajuste) {
            $data->push(round($ajuste->ajuste,2));
        }

        foreach ($tanque->ajustes as $ajuste) {
            $fechas->push($ajuste->created_at->format('d/m/Y'));
        }

        $tanque->ajustes_data = $data;
        $tanque->ajustes_fechas = $fechas;

        return Response()->json($tanque, 200);

    }

}
