<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use JWTAuth;
use NumeroALetras;
use App\Models\Admin\Empresa;
use App\Models\Admin\Bodega;
use App\Models\Admin\Corte;
use App\Models\Ventas\Venta;
use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\Detalle;
use App\Models\Inventario\Requisicion;

class ReportesController extends Controller
{
    
    public function devolucion($id) {
       
        $venta = DevolucionVenta::where('id', $id)->with('detalles', 'cliente')->first();

        if ($venta) {
            if ($venta->tipo_transaccion == 'Factura') {

                return view('reportes.factura', compact('venta'));
            }
            elseif ($venta->tipo_transaccion == 'Credito Fiscal') {

                return view('reportes.credito', compact('venta'));
            }
            else{
                return "Devolucion";
            }
        }else{
            return "Devolucion";       
        }

    }


    public function requisicionCompra(Request $request) {

        $requisicion = $request;
        $requisicion = Requisicion::where('id', 1)->with('detalles')->first();
        $empresa = Empresa::find(1);

        // return $requisicion;

        if ($requisicion) {
            $reportes = \PDF::loadView('reportes.requisicion', compact('requisicion', 'empresa'));
            return $reportes->download();
        }else{
            return "No entontrado";
        }

    }

    




}
