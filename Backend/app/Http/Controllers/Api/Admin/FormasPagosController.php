<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use App\Models\Admin\FormaPago;

class FormasPagosController extends Controller
{
    

    public function index() {
       
        $caja_id = JWTAuth::parseToken()->authenticate()->caja_id;
        $formapagos = FormaPago::where('caja_id', $caja_id)->get();

        return Response()->json($formapagos, 200);

    }


    public function read($id) {

        $formapago = FormaPago::findOrFail($id);
        return Response()->json($formapago, 200);

    }

    public function store(Request $request)
    {

        $request->validate([
            'nombre'        => 'required|max:255',
            'caja_id'       => 'required|numeric'
        ]);

        if($request->id)
            $formapago = FormaPago::findOrFail($request->id);
        else
            $formapago = new FormaPago;

        
        $formapago->fill($request->all());
        $formapago->save();

        return Response()->json($formapago, 200);

    }


}
