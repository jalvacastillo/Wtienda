<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Promocion extends Model {

    protected $table = 'producto_promociones';
    protected $fillable = array(
        'producto_id',
        'precio',
        'inicio',
        'fin'
    );

    // protected $appends = ['categoria', 'proveedor_id', 'costo_promedio', 'stock_bodega', 'stock_venta'];

   
    public function producto(){
        return $this->hasMany('App\Models\Inventario\Producto','producto_id');
    }

}



