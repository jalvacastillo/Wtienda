<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model {

    protected $table = 'producto_inventarios';
    protected $fillable = array(
        'producto_id',
        'stock',
        'stock_min',
        'stock_max',
        'bodega_id',
        'ubicacion'
    );

    protected $appends = ['producto', 'bodega', 'costo', 'precio', 'inventario'];

    public function getProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function getCostoAttribute(){
        return $this->producto()->pluck('costo')->first();
    }

    public function getPrecioAttribute(){
        return $this->producto()->pluck('precio')->first();
    }
    public function getInventarioAttribute(){
        return $this->producto()->pluck('inventario')->first();
    }
    
    public function getBodegaAttribute(){
        return $this->bodega()->pluck('nombre')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function bodega(){
    	return $this->belongsTo('App\Models\Admin\Bodega', 'bodega_id');
    }


}



