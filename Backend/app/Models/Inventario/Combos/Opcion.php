<?php

namespace App\Models\Inventario\Combos;

use Illuminate\Database\Eloquent\Model;

class Opcion extends Model {

    protected $table = 'producto_combo_detalle_opciones';
    protected $fillable = array(
        'combo_detalle_id',
        'producto_id'
    );

    protected $appends = ['producto'];

    public function getProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function detalle(){
        return $this->belongsTo('App\Models\Inventario\Combos\Detalle', 'combo_detalle_id');
    }


}



