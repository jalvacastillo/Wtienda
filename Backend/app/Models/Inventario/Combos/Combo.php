<?php

namespace App\Models\Inventario\Combos;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Combo extends Model {

    protected $table = 'producto_combos';
    protected $fillable = array(
        'nombre',
        'medida',
        'inicio',
        'fin',
    );

    protected $appends = ['precio', 'categoria', 'activo', 'costo'];

    public function getPrecioAttribute(){
        $detalles = $this->detalles()->get();
        return $detalles->sum('total');
    }

    public function getCostoAttribute(){
        $detalles = $this->detalles()->get();
        return $detalles->sum('costo_total');
    }

    public function getCategoriaAttribute(){
        if ($this->detalles()->count() > 0) {
            return $this->detalles()->first()->producto()->first()->categoria()->first()->nombre;
        } else {
            return 'Ninguna';
        }
        
    }

    public function getActivoAttribute(){

        $fechahoy = strtotime(Carbon::now()->toDateTimeString());
        $inicio = strtotime($this->inicio);
        $fin = strtotime($this->fin);

        if (($fin && $inicio) && $fin >= $fechahoy ) {
            return true;
        }else {
            return false;
        }
                
    }

    public function detalles(){
        return $this->hasMany('App\Models\Inventario\Combos\Detalle', 'combo_id');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Inventario\Categoria', 'categoria_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Detalle', 'producto_id');
    }


}



