<?php

namespace App\Models\Inventario\Combos;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'producto_combo_detalles';
    protected $fillable = array(
        'producto_id',
        'cantidad',
        'costo',
        'precio',
        'combo_id'
    );

    protected $appends = ['producto', 'costo_total', 'total'];

    public function getProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function getCostoTotalAttribute(){
        return $this->costo * $this->cantidad;
    }

    public function getTotalAttribute(){
        return $this->precio * $this->cantidad;
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function combo(){
        return $this->belongsTo('App\Models\Inventario\Combos\Combo', 'combo_id');
    }

    public function opciones(){
        return $this->hasMany('App\Models\Inventario\Combos\Opcion', 'combo_detalle_id');
    }


}



