<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model {

    protected $table = 'producto_kardex';
    protected $fillable = array(
        'fecha',
        'producto_id',
        'bodega_id',
        'detalle',
        'referencia',
        'valor_unitario',
        'entrada_cantidad',
        'entrada_valor',
        'salida_cantidad',
        'salida_valor',
        'total_cantidad',
        'total_valor',
        'usuario_id',
    );

    protected $appends = ['nombre_usuario', 'nombre_producto', 'nombre_inventario', 'ajuste'];

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->first() ? $this->usuario()->pluck('name')->first() : '';
    }

    public function getNombreProductoAttribute()
    {
        return  $this->producto()->first() ? $this->producto()->pluck('nombre')->first() : '';
    }

    public function getNombreInventarioAttribute()
    {
        return  $this->inventario()->first() ? $this->inventario()->first()->bodega()->pluck('nombre')->first() : '';
    }

    public function getAjusteAttribute()
    {
        return $this->stock_final - $this->stock_inicial;
    }

    public function inventario(){
        return $this->belongsTo('App\Models\Inventario\Inventario','inventario_id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id')->withoutGlobalScopes();
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

}



