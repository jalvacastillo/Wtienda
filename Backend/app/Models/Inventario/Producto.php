<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model {

    use SoftDeletes;
    protected $table = 'productos';
    protected $fillable = array(
        'nombre',
        'descripcion',
        'codigo',
        'medida',
        'precio',
        'costo',
        'costo_anterior',
        'categoria_id',
        'tipo',
        'tipo_impuesto',
        'inventario',
        'compuesto',
        'bodega_venta_id',
        'activo',
    );

    protected $appends = ['categoria', 'proveedor_id', 'proveedor', 'costo_promedio', 'stock_bodega', 'stock_venta', 'promocion'];

    protected $casts = [
        'activo' => 'boolean',
        'inventario' => 'boolean',
        'compuesto' => 'boolean',
    ];

    public function getNombreAttribute($value)
    {
        return strtoupper($value);
    }

    public function setCodigoAttribute($value) {
        if ( empty($value) ) {
            $this->attributes['codigo'] = NULL;
        } else {
            $this->attributes['codigo'] = $value;
        }
    }

    public function getCategoriaAttribute()
    {
        return $this->categoria()->pluck('nombre')->first();
    }

    public function getPromocionAttribute()
    {
        return $this->promociones()->where('inicio', '<', \Carbon\Carbon::now())
                                ->where('fin', '>', \Carbon\Carbon::now())
                                ->latest()
                                ->first();
    }

    public function getCostoPromedioAttribute()
    {
        return number_format(($this->costo + $this->costo_anterior) / 2, 4);
    }

    public function getStockVentaAttribute()
    {
        if ($this->categoria_id == 1) {
            return $this->tanques()->pluck('stock')->first();
        } else {
            return $this->bodegas()->where('bodega_id', 2)->pluck('stock')->first();
        }        
    }

    public function getStockBodegaAttribute()
    {
        if ($this->categoria_id == 1) {
            return $this->tanques()->pluck('stock')->first();
        } else {
            return $this->bodegas()->where('bodega_id', 1)->pluck('stock')->first();
        }        
    }

    public function getProveedorIdAttribute(){
        $compra = $this->compras()->orderBy('id', 'desc')->first();
        if ($compra && $compra->compra)
            return $compra->compra->proveedor_id;
        else
            return null;
    }

    public function getProveedorAttribute(){
        $compra = $this->compras()->orderBy('id', 'desc')->first();
        if ($compra && $compra->compra)
            return $compra->compra->proveedor;
        else
            return null;
    }


    public function categoria(){
        return $this->belongsTo('App\Models\Inventario\Categoria','categoria_id');
    }

    public function compras(){
        return $this->hasMany('App\Models\Compras\Detalle','producto_id');
    }

    // Gasolinas

    public function tanques(){
        return $this->hasMany('App\Models\Admin\Tanque','producto_id');
    }

    public function bombas(){
        return $this->hasMany('App\Models\Admin\Bomba','producto_id');
    }

    // Productos
    public function bodegas(){
        return $this->hasMany('App\Models\Inventario\Inventario','producto_id')->orderBy('bodega_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Detalle','producto_id');
    }

    public function traslados(){
        return $this->hasMany('App\Models\Inventario\TrasladoDetalle','producto_id');
    }

    public function promociones(){
        return $this->hasMany('App\Models\Inventario\Promocion','producto_id');
    }

    public function composiciones(){
        return $this->hasMany('App\Models\Inventario\Composicion','producto_id');
    }

    public function composicion(){
        return $this->hasMany('App\Models\Inventario\Composicion','compuesto_id');
    }

    public function ajustes(){
        return $this->hasMany('App\Models\Inventario\Ajuste','producto_id');
    }

}



