<?php

namespace App\Models\Inventario\Traslados;

use Illuminate\Database\Eloquent\Model;

class Traslado extends Model {

    protected $table = 'producto_traslados';
    protected $fillable = array(
       'fecha',
       'nota',
       'estado',
       'origen_id',
       'destino_id',
       'autorizo_id',
       'usuario_id'
    );

    protected $appends = ['usuario_nombre','autorizo_nombre', 'total'];

    public function getNotaAttribute($value)
    {
        return ucwords(mb_strtolower($value));
    }

    public function getUsuarioNombreAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getAutorizoNombreAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getTotalAttribute()
    {
        return  $this->detalles()->count();
    }

    public function detalles(){
        return $this->hasMany('App\Models\Inventario\Traslados\Detalle','traslado_id');
    }

    public function origen(){
        return $this->belongsTo('App\Models\Admin\Bodega','origen_id');
    }

    public function destino(){
        return $this->belongsTo('App\Models\Admin\Bodega','destino_id');
    }

    public function autorizo(){
        return $this->belongsTo('App\Models\User','autorizo_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

}



