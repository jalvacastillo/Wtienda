<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Ajuste extends Model {

    protected $table = 'producto_ajustes';
    protected $fillable = array(
        'nota',
        'producto_id',
        'bodega_id',
        'stock_inicial',
        'stock_final',
        'usuario_id'
    );

    protected $appends = ['usuario', 'producto', 'ajuste', 'ubicacion'];

    public function getUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getProductoAttribute()
    {
        return  $this->producto()->pluck('nombre')->first();
    }

    public function getAjusteAttribute()
    {
        return $this->stock_final - $this->stock_inicial;
    }

    public function getUbicacionAttribute()
    {
        if ($this->producto()->pluck('categoria_id')->first() == 1) {
            return $this->tanque()->pluck('nombre')->first();
        } else {
            return $this->bodega()->pluck('nombre')->first();
        }
        
    }

    public function bodega(){
        return $this->belongsTo('App\Models\Admin\Bodega','bodega_id');
    }

    public function tanque(){
        return $this->belongsTo('App\Models\Admin\Tanque','bodega_id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

}



