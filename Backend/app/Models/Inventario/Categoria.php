<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model {

    use SoftDeletes;
    protected $table = 'producto_categorias';
    protected $fillable = array(
        'nombre',
        'descripcion'
    );


    public function productos(){
        return $this->hasMany('App\Models\Inventario\Producto', 'categoria_id');
    }


}



