<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'codigo',
        'tipo',
        'avatar',
        'activo',
        'caja_id',
        'ultimo_login',
        'ultimo_logout',
    ];

    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['caja', 'ultimo_login_human', 'ultimo_logout_human'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUltimoLoginHumanAttribute(){
        if ($this->ultimo_login) {
            return Carbon::parse($this->ultimo_login)->diffForhumans();
        }
    }

    public function getUltimoLogoutHumanAttribute(){
        if ($this->ultimo_logout) {
                return Carbon::parse($this->ultimo_logout)->diffForhumans();
        }
    }

    public function getCajaAttribute(){
        return $this->caja()->pluck('tipo')->first();
    }

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }

    public function cortes(){
        return $this->hasMany('App\Models\Admin\Corte', 'usuario_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta', 'usuario_id')->where('estado', 'Cobrada');
    }

    public function compras(){
        return $this->hasMany('App\Models\Compras\Compra', 'usuario_id');
    }

    public function anticipos(){
        return $this->hasMany('App\Models\Registros\Anticipo', 'usuario_id');
    }
    
    // public function metas(){
    //     return $this->hasMany('App\Models\Admin\EmpleadoMeta', 'usuario_id');
    // }

    public function getJWTIdentifier() {
      return $this->getKey();
    }

    public function getJWTCustomClaims() {
      return [];
    }

}