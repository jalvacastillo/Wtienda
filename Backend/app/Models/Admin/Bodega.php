<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model {

    protected $table = 'bodegas';
    protected $fillable = array(
        'nombre',
        'descripcion'
    );

    public function productos(){
        return $this->hasMany('App\Models\Inventario\Inventario', 'bodega_id');
    }


}



