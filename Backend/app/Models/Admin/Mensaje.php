<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model {

    protected $table = 'mensajes';
    protected $fillable = [
        'descripcion',
        'estado',
        'usuario_id'
    ];

    protected $appends = ['usuario'];

    public function getUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User');
    }

}
