<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FormaPago extends Model {

    protected $table = 'caja_formas_pago';
    protected $fillable = array(
        'nombre',
        'caja_id'

    );

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }


}



