<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model {

    protected $table = 'cajas';
    protected $fillable = array(
        'nombre',
        'tipo',
        'descripcion'
    );

    public function corte(){
        return $this->hasOne('App\Models\Admin\Corte')->latest();
    }

    public function cortesDia(){
        return $this->hasMany('App\Models\Admin\Corte')->whereDate('fecha', date('Y-m-d'));
    }

    public function cortes(){
        return $this->hasMany('App\Models\Admin\Corte');
    }

    public function ventasDia(){
        return $this->hasMany('App\Models\Ventas\Venta', 'caja_id')->whereDate('fecha', date('Y-m-d'))->where('estado', '!=', 'Anulada');
    }

    public function devolucionesDia(){
        return $this->hasMany('App\Models\Ventas\DevolucionVenta', 'caja_id')->whereDate('fecha', date('Y-m-d'));
    }

    public function documentos(){
        return $this->hasMany('App\Models\Admin\Documento');
    }

    public function usuarios(){
    	return $this->hasMany('App\User', 'usuario_id');
    }


}



