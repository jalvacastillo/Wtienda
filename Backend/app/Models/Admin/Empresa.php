<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model {

    use SoftDeletes;
    protected $table = 'empresa';
    protected $fillable = [
        'nombre',
        'direccion',
        'telefono',
        'correo',
        'municipio',
        'departamento',
        'logo',
        'meta_galones',
        'valor_inventario'
    ];


}
