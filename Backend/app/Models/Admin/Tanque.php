<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tanque extends Model {

    use SoftDeletes;
    protected $table = 'tanques';
    protected $fillable = array(
        'nombre',
        'producto_id',
        'stock',
        'stock_min',
        'stock_max'
    );

    protected $appends = ['producto'];

    public function getProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }


    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function ajustes(){
    	return $this->hasMany('App\Models\Inventario\Ajuste', 'producto_id')->orderBy('id', 'DESC');
    }

    public function bombas(){
        return $this->hasMany('App\Models\Admin\Bomba', 'tanque_id');
    }


}



