<?php

namespace App\Models\Registros;

use Illuminate\Database\Eloquent\Model;

class Anticipo extends Model {

    protected $table = 'cliente_anticipos';
    protected $fillable = array(
        'fecha',
        'tipo',
        'total',
        'referencia',
        'cliente_id',
        'usuario_id'
    );

    protected $appends = ['cliente', 'usuario'];


    public function getClienteAttribute(){

        return $this->cliente()->pluck('nombre')->first();
    }

    public function getUsuarioAttribute(){

        return $this->usuario()->pluck('name')->first();
    }
   
    public function cliente(){
        return $this->belongsTo('App\Models\Registros\Cliente', 'cliente_id');
    }

    public function usuario(){
        return $this->belongsTo('App\User', 'usuario_id');
    }

}

