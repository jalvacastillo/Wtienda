<?php

namespace App\Models\Registros;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

    protected $table = 'clientes';
    protected $fillable = array(
        'nombre',
        'dui',
        'nit',
        'registro',
        'giro',
        'direccion',
        'municipio',
        'departamento',
        'telefono',
        'correo',
        'sexo',
        'etiquetas',
        'nota',
        'tipo_contribuyente',
        'descuento'

    );

    public function getNombreAttribute($value)
    {
        return strtoupper($value);
    }
    
    public function getGiroAttribute($value)
    {
        return strtoupper($value);
    }

    public function getEtiquetasAttribute($value) 
    {
        return is_string($value) ? json_decode($value) : $value;
    }
    

    public function anticipos(){
        return $this->hasMany('App\Models\Registros\Anticipo', 'cliente_id');
    }

    public function ventasPendientes(){
        return $this->hasMany('App\Models\Ventas\Venta', 'cliente_id')->where('estado', 'Pendiente');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta', 'cliente_id');
    }

}

