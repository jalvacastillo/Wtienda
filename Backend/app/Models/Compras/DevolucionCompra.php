<?php

namespace App\Models\Compras;

use Illuminate\Database\Eloquent\Model;

class DevolucionCompra extends Model {

    protected $table = 'compras_devoluciones';
    protected $fillable = array(
        'fecha',
        'estado',
        'nota',
        'recibio',
        'referencia',
        'proveedor_id',
        'iva',
        'fovial',
        'cotrans',
        'subtotal',
        'total',
        'usuario_id',
    );

    protected $appends = ['proveedor', 'usuario', 'exenta', 'gravada', 'no_sujeta'];


    public function getExentaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('excenta');
        return $interno;
    }

    public function getGravadaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('gravada');
        return $interno;
    }

    public function getNoSujetaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('no_sujeta');
        return $interno;
    }

    public function getProveedorAttribute()
    {
        return ucwords(mb_strtolower($this->proveedor()->pluck('nombre')->first()));
    }

    public function getUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function proveedor(){
        return $this->belongsTo('App\Models\Registros\Proveedor','proveedor_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Compras\DevolucionDetalle','compra_id');
    }


}
