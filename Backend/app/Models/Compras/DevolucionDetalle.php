<?php

namespace App\Models\Compras;

use Illuminate\Database\Eloquent\Model;

class DevolucionDetalle extends Model {

    protected $table = 'compra_devolucion_detalles';
    protected $fillable = array(
        'producto_id',
        'cantidad',
        'costo',
        'tipo',
        'iva',
        'cotrans',
        'fovial',
        'subtotal',
        'total',
        'compra_id'

    );

    protected $appends = ['producto_nombre', 'medida', 'exenta', 'gravada', 'no_sujeta'];

    public function getProductoNombreAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function getExentaAttribute(){
        if ($this->tipo == 'Exenta')
            return $this->subtotal;
        else
            return 0;
    }

    public function getGravadaAttribute(){
        if ($this->tipo == 'Gravada')
            return $this->subtotal;
        else
            return 0;
    }

    public function getNoSujetaAttribute(){
        if ($this->tipo == 'No Sujeta')
            return $this->subtotal;
        else
            return 0;
    }

    public function getMedidaAttribute(){
        return $this->producto()->pluck('medida')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

    public function compra(){
        return $this->belongsTo('App\Models\Compras\DevolucionCompra','compra_id');
    }


}
