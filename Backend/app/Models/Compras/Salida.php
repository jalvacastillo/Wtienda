<?php

namespace App\Models\Compras;

use Illuminate\Database\Eloquent\Model;

class Salida extends Model {

    protected $table = 'salidas';
    protected $fillable = array(
        'fecha',
        'total',
        'descripcion',
        'usuario_id'
    );

    protected $appends = ['usuario'];

    public function getUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }


}



