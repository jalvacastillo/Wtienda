<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class DevolucionVenta extends Model {

    protected $table = 'ventas_devoluciones';
    protected $fillable = array(
        'fecha',
        'correlativo',
        'estado',
        'tipo',
        'metodo_pago',
        'tipo_documento',
        'referencia',
        'nombre',
        'placa',
        'kilometraje',
        'observacion',
        'recibido',
        'iva_retenido',
        'iva',
        'fovial',
        'cotrans',
        'subcosto',
        'subtotal',
        'total',
        'cliente_id',
        'caja_id',
        'corte_id',
        'usuario_id'
    );

    protected $appends = ['cliente', 'registro', 'usuario', 'exenta', 'gravada', 'no_sujeta'];

    public function getClienteAttribute()
    {
        return $this->cliente()->pluck('nombre')->first() ? $this->cliente()->pluck('nombre')->first() : $this->nombre;
    }

    public function getNombreAttribute($name)
    {
        return strtoupper($name);
    }

    public function getRegistroAttribute()
    {
        return $this->cliente()->pluck('registro')->first();
    }

    public function getUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getExentaAttribute(){
        if ($this->tipo == 'Exenta')
            return $this->subtotal;
        else
            return 0;
    }

    public function getGravadaAttribute(){
        if ($this->tipo == 'Gravada')
            return $this->subtotal;
        else
            return 0;
    }

    public function getNoSujetaAttribute(){
        if ($this->tipo == 'No Sujeta')
            return $this->subtotal;
        else
            return 0;
    }

    public function documento(){
        return $this->belongsTo('App\Models\Admin\Documento', 'tipo_documento', 'nombre')->where('caja_id', 1);
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Admin\Empresa', 1);
    }

    public function cliente(){
        return $this->belongsTo('App\Models\Registros\Cliente','cliente_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Ventas\DevolucionDetalle','venta_id');
    }


}
