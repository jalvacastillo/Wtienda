<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class DetalleCombo extends Model {

    protected $table = 'venta_detalles_combo';
    protected $fillable = array(
        'producto_id',
        'cantidad',
        'precio',
        'costo',
        'detalle_id'
    );

    protected $appends = ['producto', 'detalle_cantidad'];

    public function getProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function getDetalleCantidadAttribute(){
        return $this->detalle()->pluck('cantidad')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

    public function detalle(){
        return $this->belongsTo('App\Models\Ventas\Detalle','detalle_id');
    }


}
