<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'venta_detalles';
    protected $fillable = array(
        'escombo',
        'producto_id',
        'cantidad',
        'precio',
        'costo',
        'descuento',
        'tipo_impuesto',
        'iva',
        'fovial',
        'cotrans',
        'subcosto',
        'subtotal',
        'total',
        'venta_id'
    );

    protected $appends = ['producto_nombre', 'medida', 'exenta', 'gravada', 'no_sujeta'];

    public function getProductoNombreAttribute(){
        if ($this->escombo)
            return $this->combo()->pluck('nombre')->first();
        else
            return $this->producto()->pluck('nombre')->first();
    }

    public function getMedidaAttribute(){
        if ($this->combo)
            return 'Combo';
        else
            return $this->producto()->pluck('medida')->first();
    }

    public function getExentaAttribute(){
        if ($this->tipo_impuesto == 'Exenta')
            return $this->subtotal;
        else
            return 0;
    }

    public function getGravadaAttribute(){
        if ($this->tipo_impuesto == 'Gravada')
            return $this->subtotal;
        else
            return 0;
    }

    public function getNoSujetaAttribute(){
        if ($this->tipo_impuesto == 'No Sujeta')
            return $this->subtotal;
        else
            return 0;
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto','producto_id');
    }

    public function combo(){
        return $this->belongsTo('App\Models\Inventario\Combos\Combo','producto_id');
    }

    public function bomba(){
        return $this->belongsTo('App\Models\Admin\Bomba','bomba_id');
    }

    public function venta(){
        return $this->belongsTo('App\Models\Ventas\Venta','venta_id');
    }


}
