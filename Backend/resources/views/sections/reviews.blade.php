<div class="section text-center">
    <h2 class="title">Clientes</h2>
    <p>
        La opinión de nuestros clientes es muy importante. <br>
        Recogemos todas sus sugerencias para mejorar nuestro producto. <br>
    </p>

    <div class="team">
      <div class="row justify-content-center">
        <div class="col-5 col-md-4" itemprop="review" itemscope itemtype="http://schema.org/Review">
          <div class="team-player">
            <div class="card card-plain">
              <div class="col-md-6 ml-auto mr-auto">
                <img src="{{ asset('img/clientes/1.png') }}" alt="Servicentro Rivera" class="img-raised rounded-circle img-fluid">
              </div>
              <h4 class="card-title" itemprop="author"> Servicentro Rivera</h4>
                <p class="d-none" itemprop="datePublished" content="2020-12-03">
                    03-12-2020
                </p>
                <p itemprop="reviewBody">
                    Gasolinera y estación de servicio en Cojutepeque, El Salvador.
                </p>
                <p>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star-half-alt text-warning"></i>
                </p>
              <div class="d-none" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                  <meta itemprop="worstRating" content="0.5">
                  <span itemprop="ratingValue">5.0</span>/
                  <span itemprop="bestRating">5</span>
              </div>
            </div>
          </div>
        </div>

        <div class="col-5 col-md-4" itemprop="review" itemscope itemtype="http://schema.org/Review">
          <div class="team-player">
            <div class="card card-plain">
              <div class="col-md-6 ml-auto mr-auto">
                <img src="{{ asset('img/clientes/2.png') }}" alt="Logo gosen" class="img-raised rounded-circle img-fluid">
              </div>
              <h4 class="card-title" itemprop="author"> Gosen</h4>
                <p class="d-none" itemprop="datePublished" content="2020-12-03">
                    07-06-2021
                </p>
                <p itemprop="reviewBody">
                    Gasolinera y estación de servicio en Suchitoto, El Salvador.
                </p>
                <p>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star text-warning"></i>
                  <i class="fa fa-star-half-alt text-warning"></i>
                </p>
              <div class="d-none" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                  <meta itemprop="worstRating" content="0.5">
                  <span itemprop="ratingValue">5.0</span>/
                  <span itemprop="bestRating">5</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>