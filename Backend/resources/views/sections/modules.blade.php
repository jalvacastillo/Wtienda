<div class="section">
    <div class="container">
        
    <div class="row justify-content-center px-0 px-lg-5 mx-0 mx-lg-5">
        <div class="col-md-12 mb-5 text-center">
            <h2 class="title">¿Qué incluye?</h2>
            <h4>Wgas es un sistema integrado por 8 módulos:</h4>
        </div>

        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-primary">dashboard</i>
                <h3>Dashboad</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Indicadores principales</li>
                <li><i class="fa fa-check text-info"></i> Cuentas pendientes</li>
                <li><i class="fa fa-check text-info"></i> Información de galonaje</li>
                <li><i class="fa fa-check text-info"></i> Estadística de productos</li>
                <li><i class="fa fa-check text-info"></i> Resumen de cajas</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-gray">build</i>
                <h3>Administrador</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Control de tanques</li>
                <li><i class="fa fa-check text-info"></i> Gestión de bodegas</li>
                <li><i class="fa fa-check text-info"></i> Gestión de empleados</li>
                <li><i class="fa fa-check text-info"></i> Gestión de cajas y turnos</li>
                <li><i class="fa fa-check text-info"></i> Configuraciones generales</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-rose">insert_chart</i>
                <h3>Reportes</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Ventas por fecha</li>
                <li><i class="fa fa-check text-info"></i> Ventas por producto</li>
                <li><i class="fa fa-check text-info"></i> Ventas por categorías</li>
                <li><i class="fa fa-check text-info"></i> Análisis de productos</li>
                <li><i class="fa fa-check text-info"></i> Reporte de ventas y compras</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-success">receipt</i>
                <h3>Facturación</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Facturación de gasolina</li>
                <li><i class="fa fa-check text-info"></i> Facturación de productos</li>
                <li><i class="fa fa-check text-info"></i> Control de vales</li>
                <li><i class="fa fa-check text-info"></i> Gestión de facturas</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-warning">shopping_cart</i>
                <h3>Compras</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Registro de facturas</li>
                <li><i class="fa fa-check text-info"></i> Cargo a inventario</li>
                <li><i class="fa fa-check text-info"></i> Devoluciones</li>
                <li><i class="fa fa-check text-info"></i> Reportes</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-rose">archive</i>
                <h3>Inventario</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de productos</li>
                <li><i class="fa fa-check text-info"></i> Kardex y control de stock</li>
                <li><i class="fa fa-check text-info"></i> Promociones y combos</li>
                <li><i class="fa fa-check text-info"></i> Ajustes y traslados</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-info">account_circle</i>
                <h3>Registros</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Gestión de clientes</li>
                <li><i class="fa fa-check text-info"></i> Cuentas por Cobrar</li>
                <li><i class="fa fa-check text-info"></i> Gestión de proveedores</li>
                <li><i class="fa fa-check text-info"></i> Cuentas por Pagar</li>
            </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4 mb-4">
            <div class="text-center">
                <i class="large material-icons text-secondary">account_balance</i>
                <h3>Contabilidad</h3>
            </div>
            <ul class="list-unstyled ml-5">
                <li><i class="fa fa-check text-info"></i> Libro de IVA</li>
                <li><i class="fa fa-check text-info"></i> Libro de compras</li>
                <li><i class="fa fa-check text-info"></i> Galonaje de combustibles</li>
            </ul>
        </div>
    </div>

    </div>
</div>