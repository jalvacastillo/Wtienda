<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  {{-- <script language="javascript">setTimeout("self.close();",500)</script> --}}
  <title>Ticket</title>
  <style>
    h1, h2, h3{
        margin: 3pt;
    }
    html, body {
        font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
    "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans",
    "Droid Sans", "Helvetica Neue", sans-serif;
        margin: 0pt;
        padding: 0pt;
        font-size: 9pt;
    }
    hr { border: none; height: 2px; /* Set the hr color */ color: #000; /* old IE */ background-color: #333; /* Modern Browsers */ }

    p{ margin: 0px; text-align:center};
    table{width: 100%; margin: auto; text-align: left; border-collapse: collapse;}
    table td{height: 12pt;}
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    .no-print{position: absolute;}
  </style>
  
  <style media="print"> .no-print{display: none; } </style>

</head>
<body onload="javascript:print();">
  
    <h3 class="text-center">{{ $venta->empresa->nombre}}</h3>
    <p>{{ $venta->empresa->direccion}}</p>
    <p>{{ $venta->empresa->giro}}</p>
    <p><b>NIT:</b> {{ $venta->empresa->nit}}</p> 
    <p><b>NCR:</b> {{ $venta->empresa->ncr}}</p>
    <p><b>Teléfono:</b> {{ $venta->empresa->telefono}}</p>
    <p><b>Caja #:</b> 0001 </p>
    <p><b>Fecha:</b> {{ $venta->created_at->format('d-m-Y h:i:s a') }}</p>
    <p><b>CORTE N°:</b> {{ $venta->corte_id }}</p>
    <p><b>VENTA N°:</b> {{ $venta->id }}</p>
    <p><b>TICKET:</b> T{{ $venta->correlativo}} &nbsp;&nbsp;&nbsp; CONTADO</p>
    <p><b>CLIENTE:</b> {{ $venta->cliente_nombre ? $venta->cliente_nombre : 'CLIENTE GENERAL'}}</p>
    <p><b>CAJERO:</b> {{ $venta->usuario }}</p>
    
    <hr>

    <table style="margin: auto;">
        <thead>
            <tr>
                {{-- <th>Productos</th> --}}
                <th>Cantidad</th>
                <th class="text-center">Precio</th>
                <th class="text-right">Total</th>
            </tr>
        </thead>
        <tbody>
        @foreach($venta->detalles as $detalle)
            <tr>
                <td colspan="3"> {{ $detalle->producto_nombre  }}</td>
            </tr>
            <tr>
                <td> {{ number_format($detalle->cantidad, 2) }}</td>
                <td class="text-center">   ${{ number_format($detalle->precio, 2 ) }}</td>
                <td class="text-right"> ${{ number_format($detalle->total, 2) }}G </th>
            </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
    <table style="margin: auto;">
        <tbody>
            <tr>
                <td class="text-center">Descuento: <b>${{ number_format($venta->detalles->sum('descuento'), 2) }}</b></td>
            </tr>
            <tr>
                <td class="text-center">Exenta: <b>${{ number_format($venta->exenta, 2) }}</b></td>
            </tr>
            <tr>
                <td class="text-center">No sujeta: <b>${{ number_format($venta->no_sujeta, 2) }}</b></td>
            </tr>
        </tbody>
    </table>
    <hr>

    <h2 class="text-center">
        <b>TOTAL: 
        <span style="margin-left: 20px;">${{ number_format($venta->total, 2) }}</span></b>
    </h2>
    <hr>
    <table style="margin: auto;">
        <tbody>
            <tr>
                <td class="text-center">Efectivo: ${{ number_format($venta->recibido, 2) }}</td>
            </tr>
            <tr>
                <td class="text-center">Importe: ${{ number_format($venta->total, 2) }}</td>
            </tr>
            <tr>
                <td class="text-center">Su Cambio: <b>${{ number_format($venta->recibido - $venta->total, 2) }}</b></td>
            </tr>
        </tbody>
    </table>
    <hr>

    <p class="text-center"><small>G = GRAVADO &nbsp;&nbsp; E = EXENTO &nbsp;&nbsp; N = NO SUJETO</small></p>

    @if($venta->total > 200)
    <br>
    {{-- <p>LLENAR SI LA VENTA ES MAYOR/IGUAL A $200.00</p> --}}
    <table style="margin: auto;">
        <tbody>
            <tr>
                <td width="100px">NOMBRE:</td>
                <td>________________________</td>
            </tr>
            <tr>
                <td width="100px">NIT:</td>
                <td>________________________</td>
            </tr>
            <tr>
                <td width="100px">DUI:</td>
                <td>________________________</td>
            </tr>
            <tr>
                <td width="100px">FIRMA:</td>
                <td>________________________</td>
            </tr>
        </tbody>
    </table>
    @endif
    <br>
    
    <p class="text-center"> Total de articulos: {{ $venta->detalles->sum('cantidad') }} </p>
    <br><br>
    <div class="footer">
        @if ($venta->documento->rangos)
            <p>SERIE AUTORIZADA: <br> {{ $venta->documento->rangos }}</p>
        @endif
        @if ($venta->documento->resolucion)
            <p>RESOLUCIÓN: <br> {{ $venta->documento->resolucion }}</p>
        @endif
        @if ($venta->documento->fecha)
            <p>DE FECHA: <br>
                <?php
                 
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                 
                echo \Carbon\Carbon::parse($venta->documento->fecha)->format('d')." de ".$meses[\Carbon\Carbon::parse($venta->documento->fecha)->format('m') - 1]. " del " . \Carbon\Carbon::parse($venta->documento->fecha)->format('Y') ;
                //Salida: Miercoles 05 de Septiembre del 2016
                 
                ?>
            </p>
        @endif
    </div>
    <br><br>
    @if ($venta->documento->nota)
        <p class="text-center">{!! str_replace(chr(10),"<br>",$venta->documento->nota) !!}</p>
    @endif
    <br><br><br><br>
    <p>.</p>
    <button class="no-print" onClick="window.close();" autofocus>Cerrar</button>


</body>
</html>
