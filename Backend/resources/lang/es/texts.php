<?php

return [

    'about_title' => "Hola, soy Diana Ramírez",
    'about_content' => "Bienvenida a mi tienda, soy madre soltera de tres hermosas niñas y compartiré mi historia contigo.
                <br/><br/>
            Me convertí en madre soltera a los 34 años y no tenía trabajo ni experiencia en ningún área profesional. Esto me llevó a estudiar y realizar cursos de contabilidad, administración y enfermería.
            <br/><br/>
            Después de estudiar un tiempo, me dieron la oportunidad de trabajar como asistente de enfermería, lo cual fue muy agotador y me pasó factura en la salud, así que tuve que irme para priorizar mi salud. Tuve más oportunidades pero debido a circunstancias imprevistas, me di cuenta que tenía que haber un cambio radical en mi vida.
            <br/><br/>
            Llegó un día que me marcó de por vida y desde entonces decidí montar mi propio negocio. No sabía cómo abrir un negocio, así que investigué mucho. También solicité ayuda al municipio de Los Ángeles, quienes me apoyaron respondiendo a todas mis dudas, inquietudes y pudieron orientarme para emprender mi sueño.
            <br/><br/>
            Ahora estoy comenzando la mayor aventura de mi vida. Este proyecto me llena de incertidumbre pero soy muy optimista, ¡será mi mayor éxito!
            <br/><br/>
            Nada ha sido fácil pero creo que con trabajo duro todo es posible. Tengo la suerte de poder contar con mi familia y amigos, quienes me apoyan plenamente y creen que este será mi mayor logro. Mi sueño es crecer tanto para poder dar trabajo a otras madres solteras que, como yo, han tenido que salir adelante en la vida solas.",

    'politic_shipping_title' => 'Shipping Policies',
    'politic_shipping_content' => 'Shipping Policies',

];
