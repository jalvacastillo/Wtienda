<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/',       			[HomeController::class, 'index'])->name('home');
Route::get('/registro',       	[HomeController::class, 'registro'])->name('registro');
Route::get('/demo',       		[HomeController::class, 'demo']);
Route::post('/demo',       		[HomeController::class, 'demoPost'])->name('demo');

Route::prefix('demo-gratis')->group(function () {

	Route::get('/{url1?}/{url2?}/{url3?}', function () { 
		return redirect('/demo-gratis');	
	});

});

Auth::routes();
