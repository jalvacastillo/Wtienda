<?php 

use App\Http\Controllers\Api\Registros\ClientesController;

    Route::get('/clientes',         				[ClientesController::class, 'index']);
	Route::get('/clientes/buscar/{txt}',  			[ClientesController::class, 'search']);
    Route::get('/clientes/list/{txt}',              [ClientesController::class, 'list']);
    Route::post('/clientes/filtrar',                [ClientesController::class, 'filter']);
	Route::get('/cliente/{id}',     				[ClientesController::class, 'read']);
	Route::post('/cliente',         				[ClientesController::class, 'store']);
	Route::delete('/cliente/{id}',  				[ClientesController::class, 'delete']);
    
// Otros
    Route::get('/cliente/ventas/{id}',              [ClientesController::class, 'ventas']);
    Route::post('/cliente/ventas/filtrar',          [ClientesController::class, 'ventasFilter']);
    Route::get('/cliente/vales-pendientes/{id}',  	[ClientesController::class, 'valesPendientes']);
    Route::get('/cliente/anticipos/{id}',  			[ClientesController::class, 'anticipos']);

    Route::get('/cuentas-cobrar',                   [ClientesController::class, 'cxc']);
    Route::get('/cuentas-cobrar/buscar/{text}',     [ClientesController::class, 'cxcBuscar']);

?>
