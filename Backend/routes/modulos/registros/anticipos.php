<?php 
use App\Http\Controllers\Api\Registros\AnticiposController;

	Route::get('/anticipos',         		[AnticiposController::class, 'index']);
	Route::get('/anticipos/buscar/{txt}',   [AnticiposController::class, 'search']);
	Route::post('/anticipos/filtrar',      	[AnticiposController::class, 'filter']);
	Route::get('/anticipo/{id}',     		[AnticiposController::class, 'read']);
	Route::post('/anticipo',         		[AnticiposController::class, 'store']);
	Route::delete('/anticipo/{id}',  		[AnticiposController::class, 'delete']);
	
?>
