<?php 

use App\Http\Controllers\Api\Ventas\VentasController;
use App\Http\Controllers\Api\Ventas\EntradasController;
use App\Http\Controllers\Api\Ventas\ValesController;

    Route::get('/ventas',               [VentasController::class, 'index']);
    Route::get('/ventas/buscar/{txt}',  [VentasController::class, 'search']);
    Route::post('/ventas/filtrar',      [VentasController::class, 'filter']);
    Route::get('/venta/{id}',           [VentasController::class, 'read']);
    Route::post('/venta',               [VentasController::class, 'store']);
    Route::delete('/venta/{id}',        [VentasController::class, 'delete']);

    Route::get('/ventas/ordenes',       [VentasController::class, 'ordenes']);

    Route::post('/libro-iva',           [VentasController::class, 'libroIva']);
    Route::post('/galonaje',            [VentasController::class, 'galonaje']);

    Route::post('/ventas/historial',    [VentasController::class, 'historial']);

    // Entradas
    Route::get('/entradas',             [EntradasController::class, 'index']);
    Route::post('/entrada',             [EntradasController::class, 'store']);
    Route::get('/entrada/{id}',         [EntradasController::class, 'read']);

    // Otros
       
    Route::get('/vales',                    [ValesController::class, 'vales']);
    Route::get('/vales/corte',              [ValesController::class, 'corte']);
    Route::get('/vales/buscar/{txt}',       [ValesController::class, 'valesSearch']);
    Route::post('/vales/filtrar',           [ValesController::class, 'valesFilter']);

?>
