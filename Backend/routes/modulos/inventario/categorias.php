<?php 

use App\Http\Controllers\Api\Inventario\CategoriasController;

    Route::get('/categorias',         		[CategoriasController::class, 'index']);
    Route::get('/categorias/buscar/{text}',	[CategoriasController::class, 'search']);
    Route::post('/categoria',         		[CategoriasController::class, 'store']);
    Route::get('/categoria/{id}',     		[CategoriasController::class, 'read']);
    Route::delete('/categoria/{id}',  		[CategoriasController::class, 'delete']);

    Route::post('/categorias/ventas',       [CategoriasController::class, 'historialVentas']);
    Route::post('/categorias/compras',      [CategoriasController::class, 'historialCompras']);

    Route::post('/categoria/cambio',        [CategoriasController::class, 'change']);


?>