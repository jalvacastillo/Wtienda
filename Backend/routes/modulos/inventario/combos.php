<?php 

use App\Http\Controllers\Api\Inventario\Combos\CombosController;
use App\Http\Controllers\Api\Inventario\Combos\DetallesController;
use App\Http\Controllers\Api\Inventario\Combos\OpcionesController;

    Route::get('/combos',                       [CombosController::class, 'index']);
    Route::get('/combos-all',                   [CombosController::class, 'all']);
    Route::post('/combo',                       [CombosController::class, 'store']);
    Route::get('/combos/buscar/{txt}',          [CombosController::class, 'search']);
    Route::get('/combo/{id}',                   [CombosController::class, 'read']);
    Route::delete('/combo/{id}',                [CombosController::class, 'delete']);
    Route::get('/combos-detalle/{id}',          [CombosController::class, 'detalles']);

    Route::post('/combo-detalle',                [DetallesController::class, 'store']);
    Route::get('/combo-detalle/{id}',            [DetallesController::class, 'read']);
    Route::get('/combos-detalle-opciones/{id}',  [DetallesController::class, 'opciones']);
    Route::delete('/combo-detalle/{id}',         [DetallesController::class, 'delete']);

    Route::post('/combo-detalle-opcion',                [OpcionesController::class, 'store']);
    Route::get('/combo-detalle-opcion/{id}',            [OpcionesController::class, 'read']);
    Route::delete('/combo-detalle-opcion/{id}',         [OpcionesController::class, 'delete']);

    Route::post('/combos/historial',            [CombosController::class, 'historial']);

?>