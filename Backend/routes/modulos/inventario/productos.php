<?php 

use App\Http\Controllers\Api\Inventario\ProductosController;
use App\Http\Controllers\Api\Inventario\ComposicionesController;
use App\Http\Controllers\Api\Inventario\PromocionesController;
use App\Http\Controllers\Api\Inventario\KardexController;

    Route::get('/productos',         		    [ProductosController::class, 'index']);
    Route::get('/producto/{id}',     		    [ProductosController::class, 'read']);
    Route::get('/productos/buscar/{text}',      [ProductosController::class, 'search']);
    Route::get('/productos/buscar-all/{text}',      [ProductosController::class, 'searchAll']);
    Route::post('/productos/filtrar',			[ProductosController::class, 'filter']);
    Route::post('/producto',                    [ProductosController::class, 'store']);
    Route::delete('/producto/{id}',  		    [ProductosController::class, 'delete']);

    Route::get('/productos/buscar-codigo/{codigo}', [ProductosController::class, 'porCodigo']);

    Route::post('/producto/kardex',  	        [KardexController::class, 'index']);
    Route::get('/productos/gasolina',  	        [ProductosController::class, 'gasolina']);
    Route::get('/producto/precios/historicos/{id}', [ProductosController::class, 'precios']);

    Route::post('/productos/analisis',          [ProductosController::class, 'analisis']);

    Route::post('/producto-composicion',        [ComposicionesController::class, 'store']);
    Route::delete('/producto-composicion/{id}', [ComposicionesController::class, 'delete']);

    Route::post('/producto-promocion',          [PromocionesController::class, 'store']);
    Route::delete('/producto-promocion/{id}',   [PromocionesController::class, 'delete']);

    Route::get('/productos/telefonia',          [ProductosController::class, 'telefonia']);

    Route::get('/producto/compras/{id}',          [ProductosController::class, 'compras']);

    Route::post('/producto/import',          [ProductosController::class, 'import']);

?>