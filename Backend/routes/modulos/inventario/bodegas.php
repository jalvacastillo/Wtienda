<?php
	
use App\Http\Controllers\Api\Inventario\InventariosController;

    Route::get('/bodega-productos/{id}',                 	[InventariosController::class, 'index']);
    Route::get('/bodega-producto/{id}',                     [InventariosController::class, 'read']);
	Route::post('/bodega-productos/filtrar-bodega',			[InventariosController::class, 'filterBodega']);
	Route::post('/bodega-productos/filtrar-venta',			[InventariosController::class, 'filterVenta']);
    Route::post('/bodega-producto',                 		[InventariosController::class, 'store']);
    Route::delete('/bodega-producto/{id}',                  [InventariosController::class, 'delete']);
    
    Route::get('/bodega-productos/bodega/buscar/{txt}',     [InventariosController::class, 'bodegaSearch']);
    Route::get('/bodega-productos/sala-venta/buscar/{txt}', [InventariosController::class, 'ventaSearch']);