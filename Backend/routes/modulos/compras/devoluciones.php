<?php

use App\Http\Controllers\Api\Compras\DevolucionComprasController;
use App\Http\Controllers\Api\Compras\DevolucionDetallesController;


    Route::get('/devoluciones-compras',              [DevolucionComprasController::class, 'index']);
    Route::post('/devolucion-compra',              [DevolucionComprasController::class, 'store']);
    Route::get('/devolucion-compra/{id}',          [DevolucionComprasController::class, 'read']);
    Route::delete('/devolucion-compra/{id}',       [DevolucionComprasController::class, 'delete']);
    
    Route::post('/devolucion/compra',              [DevolucionComprasController::class, 'facturacion']);

    Route::get('/devolucion-compra/detalle/{id}',          [DevolucionDetallesController::class, 'index']);
    Route::post('/devolucion-compra/detalle',              [DevolucionDetallesController::class, 'store']);
    Route::delete('/devolucion-compra/detalle/{id}',       [DevolucionDetallesController::class, 'delete']);