<?php
use App\Http\Controllers\Api\Admin\TanquesController;

    Route::get('/tanques',              [TanquesController::class, 'index']);
    Route::post('/tanque',              [TanquesController::class, 'store']);
    Route::get('/tanque/ajustes/{id}',  [TanquesController::class, 'ajustes']);

?> 