<?php 

use App\Http\Controllers\Api\Admin\EmpresaController;
use App\Http\Controllers\Api\Admin\BodegasController;
use App\Http\Controllers\Api\Admin\ReportesController;

    Route::post('/empresa',        	        [EmpresaController::class, 'store']);
    Route::get('/empresa/{id}',             [EmpresaController::class, 'read']);

    Route::get('/bodegas',                  [BodegasController::class, 'index']);
    Route::get('/bodega/{id}',              [BodegasController::class, 'read']);
    Route::post('/bodega',                  [BodegasController::class, 'store']);
    Route::get('/reporte/bodegas/{id}/{cat}', [BodegasController::class, 'reporte']);

    Route::post('/reporte/requisicion-compras',    [ReportesController::class, 'requisicionCompra']);
    Route::get('/reporte/corte/{id}',              [ReportesController::class, 'corte']);


?>
