<?php 

use App\Http\Controllers\Api\DashController;

    Route::post('/admin',       [DashController::class, 'admin']);
    Route::post('/galonajes',       [DashController::class, 'galonaje']);
    Route::post('/estadistica', [DashController::class, 'estadistica']);
    Route::post('/telefonia-datos',  [DashController::class, 'telefoniaDatos']);