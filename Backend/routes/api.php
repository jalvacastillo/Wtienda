<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/prueba', function(){ return Response()->json(['message' => 'Success'], 200); });

require base_path('routes/modulos/auth.php');
		
// Route::group(['middleware' => ['jwt.auth']], function () {


		require base_path('routes/modulos/dash.php');

		require base_path('routes/modulos/facturacion.php');
	
		require base_path('routes/modulos/ventas/ventas.php');
		require base_path('routes/modulos/ventas/detalles.php');
		require base_path('routes/modulos/ventas/devoluciones.php');

		require base_path('routes/modulos/compras/compras.php');
		require base_path('routes/modulos/compras/detalles.php');
		require base_path('routes/modulos/compras/devoluciones.php');

		require base_path('routes/modulos/registros/clientes.php');
		require base_path('routes/modulos/registros/anticipos.php');
		require base_path('routes/modulos/registros/proveedores.php');

		require base_path('routes/modulos/inventario/productos.php');
		require base_path('routes/modulos/inventario/materias-primas.php');
		require base_path('routes/modulos/inventario/bodegas.php');
		require base_path('routes/modulos/inventario/categorias.php');
		require base_path('routes/modulos/inventario/traslados.php');
		require base_path('routes/modulos/inventario/ajustes.php');
		require base_path('routes/modulos/inventario/combos.php');
		
		
		require base_path('routes/modulos/admin/bodegas.php');
		require base_path('routes/modulos/admin/cajas.php');
		require base_path('routes/modulos/admin/usuarios.php');
		require base_path('routes/modulos/admin/mensajes.php');
		require base_path('routes/modulos/admin/tanques.php');
		
// });